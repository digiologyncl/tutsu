<?php
/*
Plugin Name: TUTSU
Plugin URI: http://fungtutsu.co.uk/
Description: Boom.
Author: Tom Ungerer
Author URI: http://tomungerer.com/
Text Domain: tutsu
Version: 2.1.9.6
Bitbucket Plugin URI: Digiology/tutsu
Bitbucket Plugin URI: https://bitbucket.org/Digiology/tutsu
*/

define( 'TUTSU_VERSION', '2.1.9.6' );

define( 'TUTSU_REQUIRED_WP_VERSION', '4.7' );

define( 'TUTSU_PLUGIN', __FILE__ );

define( 'TUTSU_PLUGIN_BASENAME', plugin_basename( TUTSU_PLUGIN ) );

define( 'TUTSU_PLUGIN_NAME', trim( dirname( TUTSU_PLUGIN_BASENAME ), '/' ) );

define( 'TUTSU_PLUGIN_DIR', untrailingslashit( dirname( TUTSU_PLUGIN ) ) );

define( 'TUTSU_PLUGIN_WHICH_COMPANY', get_field('admin_setup_company', 'option') );

require_once ABSPATH . '/wp-content/plugins/advanced-custom-fields-pro/acf.php';

// FUNCTIONS
require_once TUTSU_PLUGIN_DIR . '/functions.php';

// ADMIN
require_once TUTSU_PLUGIN_DIR . '/admin/admin.php';

// PUBLIC
require_once TUTSU_PLUGIN_DIR . '/public/public.php';

// // CHECK FOR ERRORS
// add_action('activated_plugin','tutsu_save_error');
// function tutsu_save_error(){
//     update_option('plugin_error',  ob_get_contents());
// }

// add_action('deactivated_plugin','tutsu_unsave_error');
// function tutsu_unsave_error(){
//     update_option('plugin_error',  '');
// }

// echo get_option('plugin_error');


// add_filter( 'template_include', 'contact_page_template', 99 );

// function contact_page_template( $template ) {
//     $file_name = 'page_contact.php';

//     if ( is_page( 'contact' ) ) {
//         if ( locate_template( $file_name ) ) {
//             $template = locate_template( $file_name );
//         } else {
//             // Template not found in theme's folder, use plugin's template as a fallback
//             $template = dirname( __FILE__ ) . '/templates/' . $file_name;
//         }
//     }

//     return $template;
// }