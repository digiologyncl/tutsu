<?php

if( function_exists('acf_add_options_page') ) {
  acf_add_options_page(
    array(
      'page_title'  => 'Theme Settings',
      'menu_title'  => 'Theme Settings',
      'menu_slug'   => 'theme-settings',
      'capability'  => 'edit_posts',
      // 'redirect'    => false
    )
  );
}

require_once TUTSU_PLUGIN_DIR . '/admin/partials/setup.php';
require_once TUTSU_PLUGIN_DIR . '/admin/partials/company_info.php';
require_once TUTSU_PLUGIN_DIR . '/admin/partials/design.php';
require_once TUTSU_PLUGIN_DIR . '/admin/partials/widgets.php';
require_once TUTSU_PLUGIN_DIR . '/admin/partials/help.php';
require_once TUTSU_PLUGIN_DIR . '/admin/partials/page_options.php';
require_once TUTSU_PLUGIN_DIR . '/admin/partials/apis.php';

// Automatically fetch files within the /admin/components folder
$optional_settings = get_field('admin_setup_optional_settings', 'option');
$component_files=glob( TUTSU_PLUGIN_DIR . '/admin/components/*.php');
foreach( $component_files as $component_file ) {
  $component_slug = basename($component_file, ".php");
  if($optional_settings && in_array( $component_slug, $optional_settings)){
    require_once TUTSU_PLUGIN_DIR . '/admin/components/' . $component_slug . '.php';
  }
}


function tutsu_add_plugin_action_links( $links ) {
	return array_merge(
		array(
			'settings' => '<a href="' . get_bloginfo( 'wpurl' ) . '/wp-admin/admin.php?page=theme-settings-admin-setup">Setup</a> | <a href="' . get_bloginfo( 'wpurl' ) . '/wp-admin/admin.php?page=theme-settings-help">Help</a>'
		),
		$links
	);
}
add_filter( 'plugin_action_links_' . TUTSU_PLUGIN_BASENAME, 'tutsu_add_plugin_action_links' );