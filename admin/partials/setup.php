<?php

if( function_exists('acf_add_options_page')) {
  acf_add_options_sub_page(
    array(
      'page_title'  => 'Setup',
      'menu_title'  => 'Setup',
      'parent_slug' => 'theme-settings',
      'menu_slug'   => 'theme-settings-admin-setup',
      'capability'  => 'administrator',
    )
  );
}

if( function_exists('acf_add_local_field_group') ){
  acf_add_local_field_group(
    array (
      'key' => 'group_admin_theme_setup',
      'title' => 'Theme Setup',
      'fields' => array (
        array (
          'key' => 'field_admin_setup_company',
          'label' => 'Agency',
          'name' => 'admin_setup_company',
          'type' => 'select',
          'choices' => array(
            'default' => 'Default',
            'appymonkey' => 'Appy Monkey',
            'blueshark' => 'Blue Shark',
            'creativedental' => 'Creative Dental',
            'digiology' => 'Digiology',
            'dentistid' => 'Dentist ID',
            'hpd' => 'Health Practice Digital',
            'itc' => 'ITC Service',
            'robsonbrown' => 'Robson Brown',
            'thinkmediahub' => 'Think Media Hub',
            'personal' => 'Tom Ungerer',
          ),
          'allow_null' => 1,
          'multiple' => 0,
          'ui' => 0,
          'ajax' => 0,
          'placeholder' => '',
        ),
        array (
          'key' => 'field_admin_setup_external_scripts',
          'label' => 'External Scripts',
          'name' => 'admin_setup_external_scripts',
          'type' => 'checkbox',
          'choices' => array(
            'lightbox' => 'Lightbox',
            'slick_slider' => 'Slick Slider',
            'waypoints' => 'Waypoints',
            'material_icons' => 'Material Icons',
            'font_awesome' => 'Font Awesome',
          ),
          'allow_custom' => 0,
          'save_custom' => 0,
          'layout' => 0,
          'toggle' => 0
        ),
        array (
          'key' => 'field_admin_setup_custom_posts',
          'label' => 'Custom Posts',
          'name' => 'admin_setup_custom_posts',
          'type' => 'checkbox',
          'choices' => array(
            'artists' => 'Artists',
            'case_studies' => 'Case Studies',
            'events' => 'Events',
            'offers' => 'Offers',
            'projects' => 'Projects',
            'products' => 'Products',
            'venues' => 'Venues',
          ),
          'allow_custom' => 0,
          'save_custom' => 0,
          'layout' => 0,
          'toggle' => 0
        ),
        array (
          'key' => 'field_admin_setup_optional_settings',
          'label' => 'Optional Settings',
          'name' => 'admin_setup_optional_settings',
          'type' => 'checkbox',
          'choices' => array(
            'affiliations' => 'Affiliations',
            'testimonials' => 'Testimonials',
            'team_members' => 'Team Members',
            'banners' => 'Banners',
            'sections' => 'Sections',
            'featured_icon' => 'Featured Icons',
            'maps' => 'Maps',
            'forms' => 'Forms',
            'hello_bar' => 'Hello Bar',
          ),
          'allow_custom' => 0,
          'save_custom' => 0,
          'layout' => 0,
          'toggle' => 0
        ),
      ),
      'location' => array (
        array (
          array (
            'param' => 'options_page',
            'operator' => '==',
            'value' => 'theme-settings-admin-setup',
          ),
        ),
      ),
      'menu_order' => 0,
      'position' => 'normal',
      'style' => 'default',
      'label_placement' => 'left',
      'instruction_placement' => 'label',
      'hide_on_screen' => '',
    )
  );

  acf_add_local_field_group(
    array (
      'key' => 'group_admin_header_settings',
      'title' => 'Header Settings',
      'fields' => array (
        array (
          'key' => 'field_activate_top_bar',
          'label' => 'Activate Top Bar',
          'name' => 'activate_top_bar',
          'type' => 'true_false',
          'default_value' => 1,
        ),
        array (
          'key' => 'field_top_left_items',
          'label' => 'Top Left Items',
          'name' => 'top_left_items',
          'button_label' => 'Add Item',
          'layout' => 'block',
          'type' => 'repeater',
          'conditional_logic' => array (
            array (
              array (
                'field' => 'field_activate_top_bar',
                'operator' => '==',
                'value' => '1',
              ),
            ),
          ),
          'wrapper' => array (
            'width' => '50',
            'class' => '',
            'id' => '',
          ),
          'sub_fields' => array (
            array (
              'key' => 'field_top_left_items_item',
              'label' => 'Item',
              'name' => 'item',
              'type' => 'text',
              'conditional_logic' => 0,
              'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
              ),
            ),
          ),
        ),
        array (
          'key' => 'field_top_right_items',
          'label' => 'Top Right Items',
          'name' => 'top_right_items',
          'layout' => 'block',
          'button_label' => 'Add Item',
          'type' => 'repeater',
          'conditional_logic' => array (
            array (
              array (
                'field' => 'field_activate_top_bar',
                'operator' => '==',
                'value' => '1',
              ),
            ),
          ),
          'wrapper' => array (
            'width' => '50',
            'class' => '',
            'id' => '',
          ),
          'sub_fields' => array (
            array (
              'key' => 'field_top_right_items_item',
              'label' => 'Item',
              'name' => 'item',
              'type' => 'text',
            ),
          ),
        ),
        array (
          'key' => 'field_activate_header',
          'label' => 'Activate Header',
          'name' => 'activate_header',
          'type' => 'true_false',
          'default_value' => 1,
        ),
      ),
      'location' => array (
        array (
          array (
            'param' => 'options_page',
            'operator' => '==',
            'value' => 'theme-settings-admin-setup',
          ),
        ),
      ),
      'menu_order' => 2,
      'position' => 'normal',
      'style' => 'default',
      'label_placement' => 'top',
      'instruction_placement' => 'label',
      'hide_on_screen' => '',
    )
  );
}
