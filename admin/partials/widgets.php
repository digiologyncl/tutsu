<?php

function tutsu_widgets_init() {

  // Sidebars Widgets
  register_sidebar( array(
    'name'          => esc_html__( 'Post Sidebar', 'tutsu' ),
    'id'            => 'sidebar-1',
    'description'   => esc_html__( 'Add widgets to appear in your post sidebar.', 'tutsu' ),
    'before_widget' => '<div class="widget">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
  ) );
  register_sidebar( array(
    'name'          => esc_html__( 'Page Sidebar 1', 'tutsu' ),
    'id'            => 'sidebar-2',
    'description'   => esc_html__( 'Add widgets to appear in your page sidebar.', 'tutsu' ),
    'before_widget' => '<div class="widget">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
  ) );
  register_sidebar( array(
    'name'          => esc_html__( 'Page Sidebar 2', 'tutsu' ),
    'id'            => 'sidebar-3',
    'description'   => esc_html__( 'Add widgets to appear in your page sidebar.', 'tutsu' ),
    'before_widget' => '<div class="widget">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
  ) );
  register_sidebar( array(
    'name'          => esc_html__( 'Page Sidebar 3', 'tutsu' ),
    'id'            => 'sidebar-4',
    'description'   => esc_html__( 'Add widgets to appear in your page sidebar.', 'tutsu' ),
    'before_widget' => '<div class="widget">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
  ) );

  // Footer Widgets
  register_sidebar( array(
    'name'          => esc_html__( 'Footer 1', 'tutsu' ),
    'id'            => 'footer-1',
    'description'   => esc_html__( 'Add widgets to appear in your footer.', 'tutsu' ),
    'before_widget' => '<div class="footer-area">',
    'after_widget'  => '</div>',
    'before_title'  => '<h4 class="footer-title">',
    'after_title'   => '</h4><hr>',
  ) );
  register_sidebar( array(
    'name'          => esc_html__( 'Footer 2', 'tutsu' ),
    'id'            => 'footer-2',
    'description'   => esc_html__( 'Add widgets to appear in your footer.', 'tutsu' ),
    'before_widget' => '<div class="footer-area">',
    'after_widget'  => '</div>',
    'before_title'  => '<h4 class="footer-title">',
    'after_title'   => '</h4><hr>',
  ) );
  register_sidebar( array(
    'name'          => esc_html__( 'Footer 3', 'tutsu' ),
    'id'            => 'footer-3',
    'description'   => esc_html__( 'Add widgets to appear in your footer.', 'tutsu' ),
    'before_widget' => '<div class="footer-area">',
    'after_widget'  => '</div>',
    'before_title'  => '<h4 class="footer-title">',
    'after_title'   => '</h4><hr>',
  ) );
  register_sidebar( array(
    'name'          => esc_html__( 'Footer 4', 'tutsu' ),
    'id'            => 'footer-4',
    'description'   => esc_html__( 'Add widgets to appear in your footer.', 'tutsu' ),
    'before_widget' => '<div class="footer-area">',
    'after_widget'  => '</div>',
    'before_title'  => '<h4 class="footer-title">',
    'after_title'   => '</h4><hr>',
  ) );
}
add_action( 'widgets_init', 'tutsu_widgets_init', 11 );

$widgets_files=glob( TUTSU_PLUGIN_DIR . '/admin/widgets/*.php');
foreach( $widgets_files as $widgets_file ) {
  $widgets_slug = basename($widgets_file, ".php");
  include( $widgets_file );
}

if( function_exists('acf_add_local_field_group') ) {
  acf_add_local_field_group(
    array (
      'key' => 'group_widget_settings',
      'title' => 'Widget Settings',
      'fields' => array (
        array (
          'key' => 'field_widget_class',
          'label' => 'Custom Class',
          'name' => 'widget_class',
          'type' => 'text',
          'instructions' => 'Add a custom class to the widget.',
        ),
        array (
          'key' => 'field_widget_all',
          'label' => 'All',
          'name' => 'widget_all',
          'type' => 'true_false',
          'default_value' => 1,
          'instructions' => 'Display on the all pages/posts.',
        ),
        array (
          'return_format' => 'id',
          'key' => 'field_widget_include',
          'label' => 'Include',
          'name' => 'widget_include',
          'type' => 'relationship',
          'instructions' => 'Display on the following pages.',
          'post_type' => array (
            0 => 'page',
          ),
          'filters' => array (
            0 => 'search',
          ),
          'conditional_logic' => array (
            array (
              array (
                'field' => 'field_widget_all',
                'operator' => '!=',
                'value' => 1,
              ),
            ),
          ),
        ),
        array (
          'return_format' => 'id',
          'key' => 'field_widget_exceptions',
          'label' => 'Exceptions',
          'name' => 'widget_exceptions',
          'type' => 'relationship',
          'instructions' => 'Do not display on the following pages.',
          'post_type' => array (
            0 => 'page',
          ),
          'filters' => array (
            0 => 'search',
          ),
          'conditional_logic' => array (
            array (
              array (
                'field' => 'field_widget_all',
                'operator' => '==',
                'value' => 1,
              ),
            ),
          ),
        ),
      ),
      'location' => array (
        array (
          array (
            'param' => 'widget',
            'operator' => '==',
            'value' => 'all',
          ),
        ),
      ),
      'menu_order' => 0,
      'position' => 'normal',
      'style' => 'default',
      'label_placement' => 'top',
      'instruction_placement' => 'label',
      'hide_on_screen' => '',
    )
  );
}

add_filter('dynamic_sidebar_params', 'tutsu_dynamic_sidebar_params');
function tutsu_dynamic_sidebar_params( $params ) {

  // get widget vars
  $widget_name = $params[0]['widget_name'];
  $widget_id = $params[0]['widget_id'];

  if(is_page() || is_single() || is_archive()){
    global $post;
    $current_id = $post->ID;
  } else {
    $current_id = '';
  }

  $all = get_field('widget_all', 'widget_' . $widget_id);
  $includes = get_field('widget_include', 'widget_' . $widget_id);
  $exceptions = get_field('widget_exceptions', 'widget_' . $widget_id);
  $class = get_field('widget_class', 'widget_' . $widget_id);

  if( $class ) {
    $params[0]['before_widget'] = '<div class="' . $class . '">' . $params[0]['before_widget'];
    $params[0]['after_widget'] = '</div></div>';
  }

  if(empty($includes)){
    $includes = array();
  }

  if(empty($exceptions)){
    $exceptions = array();
  }

  $excluded_pages = array();
  $included_pages = array();


  if($all){
    foreach ($exceptions as $exception) {
      array_push($excluded_pages, $exception);
    }
  } else {
    foreach ($includes as $include) {
      array_push($included_pages, $include);
    }
  }

  if($all) {
    if(in_array($current_id, $excluded_pages)) {
      $params[0]['before_widget'] = '<div style="display:none">';
      $params[0]['after_widget'] = '</div>';
    }
  }

  if(!$all) {
    if(!in_array($current_id, $included_pages)) {
      $params[0]['before_widget'] = '<div style="display:none">';
      $params[0]['after_widget'] = '</div>';
    }
  }

  return $params;

}