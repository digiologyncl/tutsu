<?php

if( function_exists('acf_add_options_page') ) {
  acf_add_options_page(
    array(
      'page_title'  => 'Company Info',
      'menu_title'  => 'Company Info',
      'parent_slug' => 'theme-settings',
      'menu_slug'   => 'theme-settings-company-info',
      'capability'  => 'edit_posts',
    )
  );
}

if( function_exists('acf_add_local_field_group') ) {

  // Company Address
  acf_add_local_field_group(
    array (
      'key' => 'group_company_address',
      'title' => 'Company Address',
      'fields' => array (
        array (
          'key' => 'field_company_name',
          'label' => 'Company Name',
          'name' => 'company_name',
          'type' => 'text',
        ),
        array (
          'key' => 'field_street_line_1',
          'label' => 'Street Line 1',
          'name' => 'street_line_1',
          'type' => 'text',
          'wrapper' => array (
            'width' => '50%',
          ),
        ),
        array (
          'key' => 'field_street_line_2',
          'label' => 'Street Line 2',
          'name' => 'street_line_2',
          'type' => 'text',
          'wrapper' => array (
            'width' => '50%',
          ),
        ),
        array (
          'key' => 'field_city',
          'label' => 'City',
          'name' => 'city',
          'type' => 'text',
          'wrapper' => array (
            'width' => '33%',
          ),
        ),
        array (
          'key' => 'field_region',
          'label' => 'Region',
          'name' => 'region',
          'type' => 'text',
          'wrapper' => array (
            'width' => '33%',
          ),
        ),
        array (
          'key' => 'field_postcode',
          'label' => 'Postcode',
          'name' => 'postcode',
          'type' => 'text',
          'wrapper' => array (
            'width' => '34%',
          ),
        ),
        array (
          'key' => 'field_country',
          'label' => 'Country',
          'name' => 'country',
          'type' => 'text',
        ),
      ),
      'location' => array (
        array (
          array (
            'param' => 'options_page',
            'operator' => '==',
            'value' => 'theme-settings-company-info',
          ),
        ),
      ),
      'menu_order' => 0,
      'position' => 'normal',
      'style' => 'default',
      'label_placement' => 'left',
      'instruction_placement' => 'label',
      'hide_on_screen' => '',
    )
  );

  // Company Info
  acf_add_local_field_group(
    array (
      'key' => 'group_company_info',
      'title' => 'Contact Information',
      'fields' => array (
        array (
          'key' => 'field_email',
          'label' => 'Email',
          'name' => 'email',
          'type' => 'email'
        ),
        array (
          'key' => 'field_phone',
          'label' => 'Phone',
          'name' => 'phone',
          'type' => 'text'
        ),
        array (
          'key' => 'field_fax',
          'label' => 'Fax',
          'name' => 'fax',
          'type' => 'text'
        ),
        array (
          'key' => 'field_mobile',
          'label' => 'Mobile',
          'name' => 'mobile',
          'type' => 'text'
        ),
        array (
          'key' => 'field_google_analytics',
          'label' => 'Google Analytics',
          'name' => 'google_analytics',
          'type' => 'text',
          'placeholder' => 'UA-XXXXXXXX-X'
        ),
      ),
      'location' => array (
        array (
          array (
            'param' => 'options_page',
            'operator' => '==',
            'value' => 'theme-settings-company-info',
          ),
        ),
      ),
      'menu_order' => 1,
      'position' => 'normal',
      'style' => 'default',
      'label_placement' => 'left',
      'instruction_placement' => 'label',
      'hide_on_screen' => '',
    )
  );

  // Opening Hours
  acf_add_local_field_group(array (
    'key' => 'group_office_hours',
    'title' => 'Opening Hours',
    'fields' => array (
      array (
        'sub_fields' => array (
          array (
            'key' => 'field_office_days',
            'label' => 'Day(s)',
            'name' => 'days',
            'type' => 'text',
            'wrapper' => array (
              'width' => '50',
            ),
          ),
          array (
            'default_value' => '',
            'maxlength' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'key' => 'field_office_hours',
            'label' => 'Hours',
            'name' => 'hours',
            'type' => 'text',
            'wrapper' => array (
              'width' => '50',
            ),
          ),
        ),
        'layout' => 'table',
        'key' => 'field_all_office_hours',
        'label' => 'Office Hours',
        'name' => 'office_hours',
        'type' => 'repeater',
      ),
      array (
        'key' => 'field_office_hours_note',
        'label' => 'Note',
        'name' => 'office_hours_note',
        'type' => 'text',
      ),
    ),
    'location' => array (
      array (
        array (
        'param' => 'options_page',
        'operator' => '==',
        'value' => 'theme-settings-company-info',
        ),
      ),
    ),
    'menu_order' => 2,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'left',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
  ));

  // Social Media
  acf_add_local_field_group(
    array (
      'key' => 'group_social_media',
      'title' => 'Social Media',
      'fields' => array (
        array (
          'key' => 'field_facebook',
          'label' => 'Facebook',
          'name' => 'facebook',
          'type' => 'url'
        ),
        array (
          'key' => 'field_twitter',
          'label' => 'Twitter',
          'name' => 'twitter',
          'type' => 'url'
        ),
        array (
          'key' => 'field_google_plus',
          'label' => 'Google Plus',
          'name' => 'google_plus',
          'type' => 'url'
        ),
        array (
          'key' => 'field_instagram',
          'label' => 'Instagram',
          'name' => 'instagram',
          'type' => 'url'
        ),
        array (
          'key' => 'field_linkedin',
          'label' => 'Linkedin',
          'name' => 'linkedin',
          'type' => 'url'
        ),
        array (
          'key' => 'field_yelp',
          'label' => 'Yelp',
          'name' => 'yelp',
          'type' => 'url'
        ),
        array (
          'key' => 'field_pinterest',
          'label' => 'Pinterest',
          'name' => 'pinterest',
          'type' => 'url'
        ),
        array (
          'key' => 'field_youtube',
          'label' => 'Youtube',
          'name' => 'youtube',
          'type' => 'url'
        )
      ),
      'location' => array (
        array (
          array (
            'param' => 'options_page',
            'operator' => '==',
            'value' => 'theme-settings-company-info',
          ),
        ),
      ),
      'menu_order' => 3,
      'position' => 'normal',
      'style' => 'default',
      'label_placement' => 'left',
      'instruction_placement' => 'label',
      'hide_on_screen' => '',
    )
  );
}