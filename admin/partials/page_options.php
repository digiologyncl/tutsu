<?php

if( function_exists('acf_add_local_field_group') ) {
  acf_add_local_field_group(
    array (
      'key' => 'group_home_slider',
      'title' => 'Home Slider',
      'fields' => array (
        array (
          'sub_fields' => array (
            array (
              'key' => 'field_active',
              'label' => 'Active',
              'name' => 'active',
              'type' => 'true_false',
              'default_value' => 1,
              'wrapper' => array (
                'width' => '20%',
              ),
            ),
            array (
              'key' => 'field_class',
              'label' => 'Class',
              'name' => 'class',
              'type' => 'text',
              'default_value' => 'parallax',
              'wrapper' => array (
                'width' => '40%',
              ),
            ),
            array (
              'key' => 'field_id',
              'label' => 'ID',
              'name' => 'id',
              'type' => 'text',
              'wrapper' => array (
                'width' => '40%',
              ),
            ),
            array (
              'key' => 'field_content',
              'label' => 'Content',
              'name' => 'content',
              'type' => 'wysiwyg'
            ),
            array (
              'key' => 'field_image',
              'label' => 'Background Image:',
              'name' => 'image',
              'type' => 'image',
              'wrapper' => array (
                'width' => '100%',
              ),
            ),
          ),
          'key' => 'field_home_slides',
          'label' => 'Home Slides',
          'name' => 'home_slides',
          'type' => 'repeater',
          'instructions' => '',
          'layout' => 'block',
          'button_label' => 'Add Slide',
        ),
      ),
      'location' => array (
        array (
          array (
            'param' => 'page_type',
            'operator' => '==',
            'value' => 'front_page',
          ),
        ),
      ),
      'menu_order' => 1,
      'position' => 'acf_after_title',
      'style' => 'default',
      'label_placement' => 'top',
      'instruction_placement' => 'label',
      'hide_on_screen' => '',
      'active' => 1,
    )
  );

  acf_add_local_field_group(
    array (
      'key' => 'group_subtitle',
      'title' => 'Subtitle',
      'fields' => array (
        array (
          'key' => 'field_subtitle',
          'label' => '',
          'name' => 'subtitle',
          'type' => 'text',
        ),
      ),
      'location' => array (
        array (
          array (
            'param' => 'page_type',
            'operator' => '!=',
            'value' => 'front_page',
          ),
          array (
            'param' => 'post_type',
            'operator' => '!=',
            'value' => 'form-submission',
          ),
        ),
      ),
      'menu_order' => 1,
      'position' => 'acf_after_title',
      'style' => 'default',
      'label_placement' => 'top',
      'instruction_placement' => 'label',
      'hide_on_screen' => '',
      'active' => 1,
    )
  );

  acf_add_local_field_group(
    array (
      'key' => 'group_main_content_style',
      'title' => 'Main Content Style',
      'fields' => array (
        array (
          'key' => 'field_main_content_class',
          'label' => 'Class',
          'name' => 'main_content_class',
          'type' => 'text',
          'wrapper' => array (
            'width' => '50%',
          ),
        ),
        array (
          'key' => 'field_main_content_id',
          'label' => 'ID',
          'name' => 'main_content_id',
          'type' => 'text',
          'wrapper' => array (
            'width' => '50%',
          ),
        ),
        array (
          'key' => 'field_main_content_image',
          'label' => 'Background Image:',
          'name' => 'main_content_image',
          'type' => 'image',
          'wrapper' => array (
            'width' => '60%',
          ),
        ),
        array (
          'key' => 'field_main_content_container',
          'label' => 'Container Size',
          'name' => 'main_content_container',
          'type' => 'radio',
          'choices' => array(
            '' => 'Normal',
            '-xs' => 'Small',
            '-sm' => 'Medium',
            '-full' => 'Full'
          ),
          'wrapper' => array (
            'width' => '40%',
          ),
        ),
      ),
      'location' => array (
        array (
          array (
            'param' => 'post_type',
            'operator' => '==',
            'value' => 'page',
          ),
        ),
      ),
      'menu_order' => 0,
      'position' => 'normal',
      'style' => 'default',
      'label_placement' => 'top',
      'instruction_placement' => 'label',
      'hide_on_screen' => '',
    )
  );

  acf_add_local_field_group(
    array (
      'key' => 'group_posts_page_template',
      'title' => 'Template',
      'fields' => array (
        array (
          'key' => 'field_posts_page_template',
          'label' => 'Template',
          'name' => 'posts_page_template',
          'type' => 'select',
          'choices' => array(
            '' => 'Default',
            'left' => 'Sidebar Left',
            'right' => 'Sidebar Right',
          ),
        ),
      ),
      'location' => array (
        array (
          array (
            'param' => 'page_type',
            'operator' => '==',
            'value' => 'posts_page',
          ),
        ),
      ),
      'menu_order' => 0,
      'position' => 'side',
      'style' => 'default',
      'label_placement' => 'top',
      'instruction_placement' => 'label',
      'hide_on_screen' => '',
    )
  );

  acf_add_local_field_group(array (
    'key' => 'group_page_sidebar',
    'title' => 'Sidebar',
    'fields' => array (
      array (
        'key' => 'field_page_sidebar',
        'label' => 'Sidebar',
        'name' => 'page_sidebar',
        'type' => 'select',
        'choices' => array(
          'one' => 'Sidebar 1',
          'two' => 'Sidebar 2',
          'three' => 'Sidebar 3',
        ),
      )
    ),
    'location' => array (
      array (
        array (
          'param' => 'post_template',
          'operator' => '==',
          'value' => 'page_sidebar-left.php',
        ),
      ),
      array (
        array (
          'param' => 'post_template',
          'operator' => '==',
          'value' => 'page_sidebar-right.php',
        ),
      ),
    ),
    'menu_order' => 0,
    'position' => 'side',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
  ));
}