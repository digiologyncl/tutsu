<?php

if( function_exists('acf_add_options_page') ) {
  acf_add_options_sub_page(
    array(
      'page_title'  => 'Design',
      'menu_title'  => 'Design',
      'parent_slug' => 'theme-settings',
      'menu_slug'   => 'theme-settings-design',
    )
  );
}

if( function_exists('acf_add_local_field_group') ) {
  // COLORS
  acf_add_local_field_group(
    array (
      'key' => 'group_admin_colors',
      'title' => 'Colors',
      'fields' => array (
        array (
          'key' => 'field_color_black',
          'label' => 'Black',
          'name' => 'color_black',
          'type' => 'color_picker',
          'default_value' => '#060606',
        ),
        array (
          'key' => 'field_color_dark',
          'label' => 'Dark Grey',
          'name' => 'color_dark',
          'type' => 'color_picker',
          'default_value' => '#222222',
        ),
        array (
          'key' => 'field_color_grey',
          'label' => 'Grey',
          'name' => 'color_grey',
          'type' => 'color_picker',
          'default_value' => '#999999',
        ),
        array (
          'key' => 'field_color_faded',
          'label' => 'Light Grey',
          'name' => 'color_faded',
          'type' => 'color_picker',
          'default_value' => '#eeeeee',
        ),
        array (
          'key' => 'field_color_0',
          'label' => 'Color 0 (Body)',
          'name' => 'color_0',
          'type' => 'color_picker',
          'default_value' => '#373a3c',
        ),
        array (
          'key' => 'field_color_1',
          'label' => 'Color 1',
          'name' => 'color_1',
          'type' => 'color_picker',
          'default_value' => '#b22f4e',
        ),
        array (
          'key' => 'field_color_2',
          'label' => 'Color 2',
          'name' => 'color_2',
          'type' => 'color_picker',
          'default_value' => '#2b2e35',
        ),
      ),
      'location' => array (
        array (
          array (
            'param' => 'options_page',
            'operator' => '==',
            'value' => 'theme-settings-design',
          ),
        ),
      ),
      'menu_order' => 1,
      'position' => 'normal',
      'style' => 'default',
      'label_placement' => 'left',
      'instruction_placement' => 'label',
      'hide_on_screen' => '',
    )
  );

  // FONTS
  acf_add_local_field_group(
    array (
      'key' => 'group_admin_font',
      'title' => 'Fonts',
      'fields' => array (
        array (
          'key' => 'field_font_source',
          'label' => 'Font Source',
          'name' => 'font_source',
          'type' => 'select',
          'choices' => array(
            'google' => 'Google Fonts',
            'typekit' => 'Typekit',
          ),
          'allow_null' => 0,
          'multiple' => 0,
          'ui' => 0,
          'ajax' => 0,
          'placeholder' => '',
        ),
        array (
          'key' => 'field_typekit_id',
          'label' => 'Typekit ID',
          'name' => 'typekit_id',
          'type' => 'text',
          'placeholder' => 'xxxxxx',
          'conditional_logic' => array (
            array (
              array (
                'field' => 'field_font_source',
                'operator' => '==',
                'value' => 'typekit',
              ),
            ),
          ),
        ),
        array (
          'key' => 'field_font_1',
          'label' => 'Font 1',
          'name' => 'font_1',
          'type' => 'text',
          'default_value' => 'Roboto',
        ),
        array (
          'key' => 'field_font_1_styles',
          'label' => 'Font 1 Styles',
          'name' => 'font_1_styles',
          'type' => 'text',
          'default_value' => '400,400i,700,700i',
        ),
        array (
          'key' => 'field_font_1_typeface',
          'label' => 'Font 1 Typeface',
          'name' => 'font_1_typeface',
          'type' => 'text',
          'default_value' => 'sans-serif',
        ),
        array (
          'key' => 'field_font_2',
          'label' => 'Font 2',
          'name' => 'font_2',
          'type' => 'text',
        ),
        array (
          'key' => 'field_font_2_styles',
          'label' => 'Font 2 Styles',
          'name' => 'font_2_styles',
          'type' => 'text',
        ),
        array (
          'key' => 'field_font_2_typeface',
          'label' => 'Font 2 Typeface',
          'name' => 'font_2_typeface',
          'type' => 'text',
        ),
        array (
          'key' => 'field_font_3',
          'label' => 'Font 3',
          'name' => 'font_3',
          'type' => 'text',
        ),
        array (
          'key' => 'field_font_3_styles',
          'label' => 'Font 3 Styles',
          'name' => 'font_3_styles',
          'type' => 'text',
        ),
        array (
          'key' => 'field_font_3_typeface',
          'label' => 'Font 3 Typeface',
          'name' => 'font_3_typeface',
          'type' => 'text',
        ),
      ),
      'location' => array (
        array (
          array (
            'param' => 'options_page',
            'operator' => '==',
            'value' => 'theme-settings-design',
          ),
        ),
      ),
      'menu_order' => 2,
      'position' => 'normal',
      'style' => 'default',
      'label_placement' => 'left',
      'instruction_placement' => 'label',
      'hide_on_screen' => '',
    )
  );

  // LOGO
  acf_add_local_field_group(
    array (
      'key' => 'group_admin_logo',
      'title' => 'Logo',
      'fields' => array (
        array (
          'key' => 'field_site_logo',
          'label' => 'Logo',
          'name' => 'site_logo',
          'type' => 'image',
        ),
        array (
          'key' => 'field_site_logo_inv',
          'label' => 'Logo Inverted',
          'name' => 'site_logo_inv',
          'type' => 'image',
        ),
      ),
      'location' => array (
        array (
          array (
            'param' => 'options_page',
            'operator' => '==',
            'value' => 'theme-settings-design',
          ),
        ),
      ),
      'menu_order' => 5,
      'position' => 'normal',
      'style' => 'default',
      'label_placement' => 'left',
      'instruction_placement' => 'label',
      'hide_on_screen' => '',
    )
  );
}