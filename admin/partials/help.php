<?php

if( function_exists('acf_add_options_page') ) {
  acf_add_options_sub_page(
    array(
      'page_title'  => 'Help',
      'menu_title'  => 'Help',
      'parent_slug' => 'theme-settings',
      'menu_slug'   => 'theme-settings-help',
    )
  );
}

if( function_exists('acf_add_local_field_group') ) {

  $message  =  '';
  $message .=  '<p>For help got to <a href="https://tomungerer.gitbooks.io/fung-tutsu/content/" target="_blank">https://tomungerer.gitbooks.io/fung-tutsu/content/</a></p>';

  acf_add_local_field_group(
    array (
      'key' => 'group_admin_help',
      'title' => 'Help',
      'fields' => array (
        array (
          'key' => 'field_help',
          'label' => 'Help',
          'name' => 'help',
          'type' => 'message',
          'message' => $message
        ),
      ),
      'location' => array (
        array (
          array (
            'param' => 'options_page',
            'operator' => '==',
            'value' => 'theme-settings-help',
          ),
        ),
      ),
      'menu_order' => 1,
      'position' => 'normal',
      'style' => 'default',
      'label_placement' => 'left',
      'instruction_placement' => 'label',
      'hide_on_screen' => '',
    )
  );
}