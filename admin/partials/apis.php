<?php

if( function_exists('acf_add_options_page')) {
  acf_add_options_sub_page(
    array(
      'page_title'  => 'APIs',
      'menu_title'  => 'APIs',
      'parent_slug' => 'theme-settings',
      'menu_slug'   => 'theme-settings-apis',
      'capability'  => 'administrator',
    )
  );
}

if( function_exists('acf_add_local_field_group') ){
  acf_add_local_field_group(
    array (
      'key' => 'group_api_twitter',
      'title' => 'Twitter',
      'fields' => array (
        array (
          'key' => 'field_activate_twitter',
          'label' => 'Activate Twitter API',
          'name' => 'activate_twitter',
          'type' => 'true_false',
          'default_value' => false
        ),
        array (
          'key' => 'field_api_twitter_consumer_key',
          'label' => 'Consumer Key',
          'name' => 'api_twitter_consumer_key',
          'type' => 'text',
          'placeholder' => 'YOUR_CONSUMER_KEY',
          'instructions' => '<a href="https://apps.twitter.com/app/new" target="_blank">Get a Key</a>'
        ),
        array (
          'key' => 'field_api_twitter_consumer_secret',
          'label' => 'Consumer Secret',
          'name' => 'api_twitter_consumer_secret',
          'type' => 'text',
          'placeholder' => 'YOUR_CONSUMER_SECRET'
        ),
        array (
          'key' => 'field_api_twitter_access_token',
          'label' => 'Access Token',
          'name' => 'api_twitter_access_token',
          'type' => 'text',
          'placeholder' => 'YOUR_ACCESS_TOKEN'
        ),
        array (
          'key' => 'field_api_twitter_access_secret',
          'label' => 'Access Secret',
          'name' => 'api_twitter_access_secret',
          'type' => 'text',
          'placeholder' => 'YOUR_ACCESS_SECRET'
        ),
        array (
          'key' => 'field_api_twitter_username',
          'label' => 'Username',
          'name' => 'api_twitter_username',
          'type' => 'text',
        ),
        array (
          'key' => 'field_api_twitter_count',
          'label' => 'Count',
          'name' => 'api_twitter_count',
          'type' => 'number',
          'default_value' => 2,
        ),
      ),
      'location' => array (
        array (
          array (
            'param' => 'options_page',
            'operator' => '==',
            'value' => 'theme-settings-apis'
          ),
        ),
      ),
      'menu_order' => 1,
      'position' => 'normal',
      'style' => 'default',
      'label_placement' => 'left',
      'instruction_placement' => 'label',
      'hide_on_screen' => '',
    )
  );
}

if( function_exists('acf_add_local_field_group') ){
  acf_add_local_field_group(
    array (
      'key' => 'group_api_instagram',
      'title' => 'Instagram',
      'fields' => array (
        array (
          'key' => 'field_activate_instagram_api',
          'label' => 'Activate Instagram API',
          'name' => 'activate_instagram_api',
          'type' => 'true_false',
          'default_value' => false
        ),
        array (
          'key' => 'field_api_instagram_username',
          'label' => 'Username',
          'name' => 'api_instagram_username',
          'type' => 'text',
        ),
      ),
      'location' => array (
        array (
          array (
            'param' => 'options_page',
            'operator' => '==',
            'value' => 'theme-settings-apis'
          ),
        ),
      ),
      'menu_order' => 3,
      'position' => 'normal',
      'style' => 'default',
      'label_placement' => 'left',
      'instruction_placement' => 'label',
      'hide_on_screen' => '',
    )
  );
}

if( function_exists('acf_add_local_field_group') ){
  acf_add_local_field_group(
    array (
      'key' => 'group_api_bandsintown',
      'title' => 'Bandsintown',
      'fields' => array (
        array (
          'key' => 'field_activate_bandsintown_api',
          'label' => 'Activate Bandsintown API',
          'name' => 'activate_bandsintown_api',
          'type' => 'true_false',
          'default_value' => false
        ),
        array (
          'key' => 'field_api_bandsintown_artistname',
          'label' => 'Artist Name',
          'name' => 'api_bandsintown_artistname',
          'type' => 'text',
        ),
        array (
          'key' => 'field_api_bandsintown_app_id',
          'label' => 'App ID',
          'name' => 'api_bandsintown_app_id',
          'type' => 'text',
        ),
      ),
      'location' => array (
        array (
          array (
            'param' => 'options_page',
            'operator' => '==',
            'value' => 'theme-settings-apis'
          ),
        ),
      ),
      'menu_order' => 6,
      'position' => 'normal',
      'style' => 'default',
      'label_placement' => 'left',
      'instruction_placement' => 'label',
      'hide_on_screen' => '',
    )
  );
}