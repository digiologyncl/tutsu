<?php

class TUTSU_Widget_WYSIWYG extends WP_Widget {

  /**
   * Register widget with WordPress.
   */
  function __construct() {
    parent::__construct(
      'tutsu_widget_wysiwyg', // Base ID
      __('WYSIWYG', 'tutsu'), // Name
      array( 'description' => __( 'Display some content via a WYSIWYG. (TUTSU)', 'tutsu' ), ) // Args
    );
  }

  /**
   * Front-end display of widget.
   *
   * @see WP_Widget::widget()
   *
   * @param array $args     Widget arguments.
   * @param array $instance Saved values from database.
   */
  public function widget( $args, $instance ) {

    // OUTPUT - start
    $output  = '';
    $output .= $args['before_widget'];
    if ( !empty($instance['title']) ) {
      $output .= $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ). $args['after_title'];
    }

    $output .= apply_filters('the_content', get_field('widget_wysiwyg', 'widget_' . $args['widget_id']));

    $output .= $args['after_widget'];
    // OUTPUT - end

    echo $output;

  }

  /**
   * Back-end widget form.
   *
   * @see WP_Widget::form()
   *
   * @param array $instance Previously saved values from database.
   */
  public function form( $instance ) {
    if ( isset($instance['title']) ) {
      $title = $instance['title'];
    }
    else {
      $title = __( 'Widget Title', 'tutsu' );
    }
    ?>
    <p>
      <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
    </p>
    <?php
  }

  /**
   * Sanitize widget form values as they are saved.
   *
   * @see WP_Widget::update()
   *
   * @param array $new_instance Values just sent to be saved.
   * @param array $old_instance Previously saved values from database.
   *
   * @return array Updated safe values to be saved.
   */
  public function update( $new_instance, $old_instance ) {
    $instance = array();
    $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';

    return $instance;
  }

} // class TUTSU_Widget_WYSIWYG

// register TUTSU_Widget_WYSIWYG widget
add_action( 'widgets_init', function(){
  register_widget( 'TUTSU_Widget_WYSIWYG' );
});

if( function_exists('acf_add_local_field_group') ) {
  acf_add_local_field_group(
    array (
      'key' => 'group_widget_wysiwyg',
      'title' => 'WYSIWYG',
      'fields' => array (
        array (
          'key' => 'field_widget_wysiwyg',
          'label' => 'Content:',
          'name' => 'widget_wysiwyg',
          'type' => 'wysiwyg',
        ),
      ),
      'location' => array (
        array (
          array (
            'param' => 'widget',
            'operator' => '==',
            'value' => 'tutsu_widget_wysiwyg',
          ),
        ),
      ),
      'menu_order' => 0,
      'position' => 'normal',
      'style' => 'default',
      'label_placement' => 'top',
      'instruction_placement' => 'label',
      'hide_on_screen' => '',
    )
  );
}