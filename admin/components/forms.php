<?php

// function tutsu_forms_submissions() {
//   $labels = array(
//     'name'                       => _x( 'Categories', 'taxonomy general name', 'tutsu' ),
//     'singular_name'              => _x( 'Category', 'taxonomy singular name', 'tutsu' ),
//     'search_items'               => __( 'Search Categories', 'tutsu' ),
//     'popular_items'              => __( 'Popular Categories', 'tutsu' ),
//     'all_items'                  => __( 'All Categories', 'tutsu' ),
//     'parent_item'                => null,
//     'parent_item_colon'          => null,
//     'edit_item'                  => __( 'Edit Category', 'tutsu' ),
//     'update_item'                => __( 'Update Category', 'tutsu' ),
//     'add_new_item'               => __( 'Add New Category', 'tutsu' ),
//     'new_item_name'              => __( 'New Category Name', 'tutsu' ),
//     'separate_items_with_commas' => __( 'Separate categories with commas', 'tutsu' ),
//     'add_or_remove_items'        => __( 'Add or remove categories', 'tutsu' ),
//     'choose_from_most_used'      => __( 'Choose from the most used categories', 'tutsu' ),
//     'not_found'                  => __( 'No categories found.', 'tutsu' ),
//     'menu_name'                  => __( 'Categories', 'tutsu' ),
//   );
//   $args = array(
//     'hierarchical'          => false,
//     'labels'                => $labels,
//     'show_ui'               => true,
//     'show_admin_column'     => true,
//     'update_count_callback' => '_update_post_term_count',
//     'query_var'             => false,
//   );
//   register_taxonomy( 'form-submission-category', 'form-submission', $args );

//   $labels = array(
//     'name'               => _x( 'Submissions', 'post type general name', 'tutsu' ),
//     'singular_name'      => _x( 'Submission', 'post type singular name', 'tutsu' ),
//     'menu_name'          => _x( 'Submissions', 'admin menu', 'tutsu' ),
//     'name_admin_bar'     => _x( 'Submission', 'add new on admin bar', 'tutsu' ),
//     'add_new'            => _x( 'Add New', 'submission', 'tutsu' ),
//     'add_new_item'       => __( 'Add New Submission', 'tutsu' ),
//     'new_item'           => __( 'New Submission', 'tutsu' ),
//     'edit_item'          => __( 'Edit Submission', 'tutsu' ),
//     'view_item'          => __( 'View Submission', 'tutsu' ),
//     'all_items'          => __( 'All Submissions', 'tutsu' ),
//     'search_items'       => __( 'Search Submissions', 'tutsu' ),
//     'parent_item_colon'  => __( 'Parent Submissions:', 'tutsu' ),
//     'not_found'          => __( 'No submissions found.', 'tutsu' ),
//     'not_found_in_trash' => __( 'No submissions found in Trash.', 'tutsu' )
//   );

//   $args = array(
//     'labels'             => $labels,
//     'description'        => __( 'Form submissions.', 'tutsu' ),
//     'taxonomies'         => array( 'form-submission-category' ),
//     'public'             => false,
//     'publicly_queryable' => false,
//     'show_ui'            => true,
//     'show_in_menu'       => true,
//     'query_var'          => true,
//     'capability_type'    => 'post',
//     'has_archive'        => true,
//     'hierarchical'       => false,
//     'menu_position'      => null,
//     'menu_icon'          => 'dashicons-media-text',
//     'supports'           => array( 'title' ),
//   );

//   register_post_type( 'form-submission', $args );
// }
// add_action( 'init', 'tutsu_forms_submissions' );

if( function_exists('acf_add_options_page') ) {
  acf_add_options_page(
    array(
      'page_title'  => 'Forms',
      'menu_title'  => 'Forms',
      'menu_slug'   => 'theme-settings-forms',
      'icon_url'    => 'dashicons-list-view',
      'capability'  => 'edit_posts',
      'redirect'    => true
    )
  );

  acf_add_options_sub_page(
    array(
      'page_title'  => 'Settings',
      'menu_title'  => 'Settings',
      'parent_slug' => 'theme-settings-forms',
      'menu_slug'   => 'theme-settings-forms-settings',
      'capability'  => 'edit_posts',
    )
  );

  acf_add_options_sub_page(
    array(
      'page_title'  => 'Submissions',
      'menu_title'  => 'Submissions',
      'parent_slug' => 'theme-settings-forms',
      'menu_slug'   => 'theme-settings-forms-submissions',
      'capability'  => 'edit_posts',
    )
  );

  $activates = get_field('tutsu_forms_settings_activate_forms', 'options');

  if($activates){

    if(in_array('contact_us', $activates)){
      acf_add_options_sub_page(
        array(
          'page_title'  => 'Contact Us',
          'menu_title'  => 'Contact Us',
          'parent_slug' => 'theme-settings-forms',
          'menu_slug'   => 'theme-settings-forms-contact-us',
          'capability'  => 'edit_posts',
        )
      );
    }
    if(in_array('appointment', $activates)){
      acf_add_options_sub_page(
        array(
          'page_title'  => 'Appointment',
          'menu_title'  => 'Appointment',
          'parent_slug' => 'theme-settings-forms',
          'menu_slug'   => 'theme-settings-forms-appointment',
          'capability'  => 'edit_posts',
        )
      );
    }
    if(in_array('review_us', $activates)){
      acf_add_options_sub_page(
        array(
          'page_title'  => 'Review Us',
          'menu_title'  => 'Review Us',
          'parent_slug' => 'theme-settings-forms',
          'menu_slug'   => 'theme-settings-forms-review-us',
          'capability'  => 'edit_posts',
        )
      );
    }
    if(in_array('refer_us', $activates)){
      acf_add_options_sub_page(
        array(
          'page_title'  => 'Refer Us',
          'menu_title'  => 'Refer Us',
          'parent_slug' => 'theme-settings-forms',
          'menu_slug'   => 'theme-settings-forms-refer-us',
          'capability'  => 'edit_posts',
        )
      );
    }
    if(in_array('newsletter', $activates)){
      acf_add_options_sub_page(
        array(
          'page_title'  => 'Newsletter',
          'menu_title'  => 'Newsletter',
          'parent_slug' => 'theme-settings-forms',
          'menu_slug'   => 'theme-settings-forms-newsletter',
          'capability'  => 'edit_posts',
        )
      );
    }
  }
}



if( function_exists('acf_add_local_field_group') ){
  // SETTINGS
  acf_add_local_field_group(
    array (
      'key' => 'group_tutsu_forms_settings',
      'title' => 'Settings',
      'fields' => array (
        array (
          'key' => 'field_tutsu_forms_settings_activate_forms',
          'label' => 'Activate Forms',
          'name' => 'tutsu_forms_settings_activate_forms',
          'type' => 'checkbox',
          'choices' => array(
            'contact_us' => 'Contact Us',
            'appointment' => 'Appointment',
            'review_us' => 'Review Us',
            'refer_us' => 'Refer Us',
            'newsletter' => 'Newsletter',
          ),
          'layout' => 'horizontal',
        ),
        array (
          'key' => 'field_tutsu_forms_settings_recaptcha_activate',
          'label' => 'Activate Recaptcha',
          'name' => 'tutsu_forms_settings_recaptcha_activate',
          'type' => 'true_false',
        ),
        array (
          'key' => 'field_tutsu_forms_settings_recaptcha_site_key',
          'label' => 'Recaptcha Site Key',
          'name' => 'tutsu_forms_settings_recaptcha_site_key',
          'type' => 'text',
          'conditional_logic' => array (
            array (
              array (
                'field' => 'field_tutsu_forms_settings_recaptcha_activate',
                'operator' => '==',
                'value' => 1,
              ),
            ),
          ),
        ),
      ),
      'location' => array (
        array (
          array (
            'param' => 'options_page',
            'operator' => '==',
            'value' => 'theme-settings-forms-settings',
          ),
        ),
      ),
      'menu_order' => 0,
      'position' => 'normal',
      'style' => 'seamless',
      'label_placement' => 'left',
      'instruction_placement' => 'label',
      'active' => 1,
    )
  );

  $activates = get_field('tutsu_forms_settings_activate_forms', 'options');

  if($activates){

    // CONTACT US SETTINGS
    if(in_array('contact_us', $activates)){
      acf_add_local_field_group(
        array (
          'key' => 'group_tutsu_forms_settings_contact_us',
          'title' => 'Contact Us Form Settings <br><small>[form_contact_us]</small>',
          'fields' => array (
            array (
              'key' => 'field_tutsu_forms_settings_contact_us_email',
              'label' => 'Recipient Email',
              'name' => 'tutsu_forms_settings_contact_us_email',
              'type' => 'email',
            ),
            array (
              'key' => 'field_tutsu_forms_settings_contact_us_subject',
              'label' => 'Email Subject',
              'name' => 'tutsu_forms_settings_contact_us_subject',
              'type' => 'text',
            ),
            array (
              'key' => 'field_tutsu_forms_settings_contact_us_header_image',
              'label' => 'Header Image',
              'name' => 'tutsu_forms_settings_contact_us_header_image',
              'type' => 'image',
            ),
            array (
              'key' => 'field_tutsu_forms_settings_contact_us_body',
              'label' => 'Email Body',
              'name' => 'tutsu_forms_settings_contact_us_body',
              'type' => 'wysiwyg',
              'instructions' => 'Use {{name}} for Full Name, {{fname}} for First Name, {{lname}} for Last Name, {{email} for Email, {{phone}} for Phone and {{link}} for Verify Link.</a>',
            ),
          ),
          'location' => array (
            array (
              array (
                'param' => 'options_page',
                'operator' => '==',
                'value' => 'theme-settings-forms-settings',
              ),
            ),
          ),
          'menu_order' => 1,
          'position' => 'normal',
          'style' => 'default',
          'label_placement' => 'top',
          'instruction_placement' => 'label',
          'active' => 1,
        )
      );
    }

    // APPOINTMENT SETTINGS
    if(in_array('appointment', $activates)){
      acf_add_local_field_group(
        array (
          'key' => 'group_tutsu_forms_settings_appointment',
          'title' => 'Appointment Form Settings <br><small>[form_appointment]</small>',
          'fields' => array (
            array (
              'key' => 'field_tutsu_forms_settings_appointment_email',
              'label' => 'Recipient Email',
              'name' => 'tutsu_forms_settings_appointment_email',
              'type' => 'email',
            ),
            array (
              'key' => 'field_tutsu_forms_settings_appointment_subject',
              'label' => 'Email Subject',
              'name' => 'tutsu_forms_settings_appointment_subject',
              'type' => 'text',
            ),
            array (
              'key' => 'field_tutsu_forms_settings_appointment_header_image',
              'label' => 'Header Image',
              'name' => 'tutsu_forms_settings_appointment_header_image',
              'type' => 'image',
            ),
            array (
              'key' => 'field_tutsu_forms_settings_appointment_body',
              'label' => 'Email Body',
              'name' => 'tutsu_forms_settings_appointment_body',
              'type' => 'wysiwyg',
              'instructions' => 'Use {{name}} for Name and {{email} for Email.<br> Advised to use : <a href="http://emailframe.work/" target="_blank">http://emailframe.work/</a>',
            ),
          ),
          'location' => array (
            array (
              array (
                'param' => 'options_page',
                'operator' => '==',
                'value' => 'theme-settings-forms-settings',
              ),
            ),
          ),
          'menu_order' => 2,
          'position' => 'normal',
          'style' => 'default',
          'label_placement' => 'top',
          'instruction_placement' => 'label',
          'active' => 1,
        )
      );
    }

    // REVIEW US SETTINGS
    if(in_array('review_us', $activates)){
      acf_add_local_field_group(
        array (
          'key' => 'group_tutsu_forms_settings_review_us',
          'title' => 'Review Us Form Settings <br><small>[form_review_us]</small>',
          'fields' => array (
            array (
              'key' => 'field_tutsu_forms_settings_review_us_email',
              'label' => 'Recipient Email',
              'name' => 'tutsu_forms_settings_review_us_email',
              'type' => 'email',
            ),
            array (
              'key' => 'field_tutsu_forms_settings_review_us_subject',
              'label' => 'Email Subject',
              'name' => 'tutsu_forms_settings_review_us_subject',
              'type' => 'text',
            ),
            array (
              'key' => 'field_tutsu_forms_settings_review_us_header_image',
              'label' => 'Header Image',
              'name' => 'tutsu_forms_settings_review_us_header_image',
              'type' => 'image',
            ),
            array (
              'key' => 'field_tutsu_forms_settings_review_us_body',
              'label' => 'Email Body',
              'name' => 'tutsu_forms_settings_review_us_body',
              'type' => 'wysiwyg',
              'instructions' => 'Use {{name}} for Name and {{email} for Email.<br> Advised to use : <a href="http://emailframe.work/" target="_blank">http://emailframe.work/</a>',
            ),
          ),
          'location' => array (
            array (
              array (
                'param' => 'options_page',
                'operator' => '==',
                'value' => 'theme-settings-forms-settings',
              ),
            ),
          ),
          'menu_order' => 3,
          'position' => 'normal',
          'style' => 'default',
          'label_placement' => 'top',
          'instruction_placement' => 'label',
          'active' => 1,
        )
      );
    }

    // REVIEW US SETTINGS
    if(in_array('refer_us', $activates)){
      acf_add_local_field_group(
        array (
          'key' => 'group_tutsu_forms_settings_refer_us',
          'title' => 'Refer Us Form Settings <br><small>[form_refer_us]</small>',
          'fields' => array (
            array (
              'key' => 'field_tutsu_forms_settings_refer_us_email',
              'label' => 'Recipient Email',
              'name' => 'tutsu_forms_settings_refer_us_email',
              'type' => 'email',
            ),
            array (
              'key' => 'field_tutsu_forms_settings_refer_us_subject',
              'label' => 'Email Subject',
              'name' => 'tutsu_forms_settings_refer_us_subject',
              'type' => 'text',
            ),
            array (
              'key' => 'field_tutsu_forms_settings_refer_us_header_image',
              'label' => 'Header Image',
              'name' => 'tutsu_forms_settings_refer_us_header_image',
              'type' => 'image',
            ),
            array (
              'key' => 'field_tutsu_forms_settings_refer_us_body',
              'label' => 'Email Body',
              'name' => 'tutsu_forms_settings_refer_us_body',
              'type' => 'wysiwyg',
              'instructions' => 'Use {{name}} for Name and {{email} for Email.<br> Advised to use : <a href="http://emailframe.work/" target="_blank">http://emailframe.work/</a>',
            ),
          ),
          'location' => array (
            array (
              array (
                'param' => 'options_page',
                'operator' => '==',
                'value' => 'theme-settings-forms-settings',
              ),
            ),
          ),
          'menu_order' => 4,
          'position' => 'normal',
          'style' => 'default',
          'label_placement' => 'top',
          'instruction_placement' => 'label',
          'active' => 1,
        )
      );
    }

    // REVIEW US SETTINGS
    if(in_array('newsletter', $activates)){
      acf_add_local_field_group(
        array (
          'key' => 'group_tutsu_forms_settings_newsletter',
          'title' => 'Newsletter Form Settings <br><small>[form_newsletter]</small>',
          'fields' => array (
            array (
              'key' => 'field_tutsu_forms_settings_newsletter_email',
              'label' => 'Recipient Email',
              'name' => 'tutsu_forms_settings_newsletter_email',
              'type' => 'email',
            ),
            array (
              'key' => 'field_tutsu_forms_settings_newsletter_subject',
              'label' => 'Email Subject',
              'name' => 'tutsu_forms_settings_newsletter_subject',
              'type' => 'text',
            ),
            array (
              'key' => 'field_tutsu_forms_settings_newsletter_header_image',
              'label' => 'Header Image',
              'name' => 'tutsu_forms_settings_newsletter_header_image',
              'type' => 'image',
            ),
            array (
              'key' => 'field_tutsu_forms_settings_newsletter_body',
              'label' => 'Email Body',
              'name' => 'tutsu_forms_settings_newsletter_body',
              'type' => 'wysiwyg',
              'instructions' => 'Use {{name}} for Name and {{email} for Email.<br> Advised to use : <a href="http://emailframe.work/" target="_blank">http://emailframe.work/</a>',
            ),
          ),
          'location' => array (
            array (
              array (
                'param' => 'options_page',
                'operator' => '==',
                'value' => 'theme-settings-forms-settings',
              ),
            ),
          ),
          'menu_order' => 5,
          'position' => 'normal',
          'style' => 'default',
          'label_placement' => 'top',
          'instruction_placement' => 'label',
          'active' => 1,
        )
      );
    }

  }


  $activates = get_field('tutsu_forms_settings_activate_forms', 'options');

  if($activates){

  // CONTACT US SUBMISSIONS
    if(in_array('contact_us', $activates)){
      // acf_add_local_field_group(
      //   array (
      //     'key' => 'group_tutsu_forms_submissions_contact_us',
      //     'title' => 'Submissions',
      //     'fields' => array (
      //       array (
      //         'key' => 'field_tutsu_forms_submissions_contact_us_fname',
      //         'label' => 'First Name',
      //         'name' => 'fname',
      //         'type' => 'text',
      //         'wrapper' => array (
      //           'width' => '50',
      //         ),
      //       ),
      //       array (
      //         'key' => 'field_tutsu_forms_submissions_contact_us_lname',
      //         'label' => 'Last Name',
      //         'name' => 'lname',
      //         'type' => 'text',
      //         'wrapper' => array (
      //           'width' => '50',
      //         ),
      //       ),
      //       array (
      //         'key' => 'field_tutsu_forms_submissions_contact_us_email',
      //         'label' => 'Email',
      //         'name' => 'email',
      //         'type' => 'email',
      //         'wrapper' => array (
      //           'width' => '50',
      //         ),
      //       ),
      //       array (
      //         'key' => 'field_tutsu_forms_submissions_contact_us_phone',
      //         'label' => 'Phone',
      //         'name' => 'phone',
      //         'type' => 'text',
      //         'wrapper' => array (
      //           'width' => '50',
      //         ),
      //       ),
      //       array (
      //         'key' => 'field_tutsu_forms_submissions_contact_us_subject',
      //         'label' => 'Subject',
      //         'name' => 'subject',
      //         'type' => 'text',
      //         'wrapper' => array (
      //           'width' => '80',
      //         ),
      //       ),
      //       array (
      //         'key' => 'field_tutsu_forms_submissions_contact_us_verified',
      //         'label' => 'Verified',
      //         'name' => 'verified',
      //         'type' => 'true_false',
      //         'wrapper' => array (
      //           'width' => '20',
      //         ),
      //       ),
      //       array (
      //         'key' => 'field_tutsu_forms_submissions_contact_us_message',
      //         'label' => 'Message',
      //         'name' => 'message',
      //         'type' => 'textarea',
      //         'wrapper' => array (
      //           'width' => '100',
      //         ),
      //       ),
      //       array (
      //         'key' => 'field_tutsu_forms_submissions_contact_us_key',
      //         'label' => '',
      //         'name' => 'key',
      //         'class' => 'hidden',
      //         'type' => 'password',
      //         'wrapper' => array (
      //           'width' => '0',
      //         ),
      //       ),
      //       array (
      //         'key' => 'field_tutsu_forms_submissions_contact_us_page_id',
      //         'label' => '',
      //         'name' => 'page_id',
      //         'class' => 'hidden',
      //         'type' => 'text',
      //         'wrapper' => array (
      //           'width' => '0',
      //         ),
      //       ),
      //     ),
      //     'location' => array (
      //       array (
      //         array (
      //           'param' => 'post_type',
      //           'operator' => '==',
      //           'value' => 'form-submission',
      //         ),
      //         array (
      //           'param' => 'post_taxonomy',
      //           'operator' => '==',
      //           'value' => 'form-submission-category:contact-us',
      //         ),
      //       ),
      //     ),
      //     'menu_order' => 0,
      //     'position' => 'normal',
      //     'style' => 'default',
      //     'label_placement' => 'top',
      //     'instruction_placement' => 'label',
      //     'active' => 1,
      //     'hide_on_screen' => array (
      //       0 => 'field_subtitle',
      //     ),
      //   )
      // );
      acf_add_local_field_group(
        array (
          'key' => 'group_tutsu_forms_submissions_contact_us',
          'title' => 'Submissions',
          'fields' => array (
            array (
              'key' => 'field_tutsu_forms_submissions_contact_us',
              'label' => '',
              'name' => 'tutsu_forms_submissions_contact_us',
              'type' => 'repeater',
              'layout' => 'block',
              'sub_fields' => array (
                array (
                  'key' => 'field_tutsu_forms_submissions_contact_us_fname',
                  'label' => 'First Name',
                  'name' => 'fname',
                  'type' => 'text',
                  'wrapper' => array (
                    'width' => '50',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_contact_us_lname',
                  'label' => 'Last Name',
                  'name' => 'lname',
                  'type' => 'text',
                  'wrapper' => array (
                    'width' => '50',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_contact_us_email',
                  'label' => 'Email',
                  'name' => 'email',
                  'type' => 'email',
                  'wrapper' => array (
                    'width' => '50',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_contact_us_phone',
                  'label' => 'Phone',
                  'name' => 'phone',
                  'type' => 'text',
                  'wrapper' => array (
                    'width' => '50',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_contact_us_subject',
                  'label' => 'Subject',
                  'name' => 'subject',
                  'type' => 'text',
                  'wrapper' => array (
                    'width' => '80',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_contact_us_verified',
                  'label' => 'Verified',
                  'name' => 'verified',
                  'type' => 'true_false',
                  'wrapper' => array (
                    'width' => '20',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_contact_us_message',
                  'label' => 'Message',
                  'name' => 'message',
                  'type' => 'textarea',
                  'wrapper' => array (
                    'width' => '100',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_contact_us_key',
                  'label' => '',
                  'name' => 'key',
                  'class' => 'hidden',
                  'type' => 'password',
                  'wrapper' => array (
                    'width' => '0',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_contact_us_page_id',
                  'label' => '',
                  'name' => 'page_id',
                  'class' => 'hidden',
                  'type' => 'text',
                  'wrapper' => array (
                    'width' => '0',
                  ),
                ),
              ),
            ),
          ),
          'location' => array (
            array (
              array (
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'form-submission',
              ),
              array (
                'param' => 'post_taxonomy',
                'operator' => '==',
                'value' => 'form-submission-category:contact-us',
              ),
            ),
            array (
              array (
                'param' => 'options_page',
                'operator' => '==',
                'value' => 'theme-settings-forms-contact-us',
              ),
            ),
          ),
          'menu_order' => 0,
          'position' => 'normal',
          'style' => 'default',
          'label_placement' => 'top',
          'instruction_placement' => 'label',
          'active' => 1,
          'hide_on_screen' => array (
            0 => 'field_subtitle',
          ),
        )
      );
    }

  // APPOINTMENT SUBMISSIONS
    if(in_array('appointment', $activates)){
      acf_add_local_field_group(
        array (
          'key' => 'group_tutsu_forms_submissions_appointment',
          'title' => 'Submissions',
          'fields' => array (
            array (
              'key' => 'field_tutsu_forms_submissions_appointment',
              'label' => '',
              'name' => 'tutsu_forms_submissions_appointment',
              'type' => 'repeater',
              'layout' => 'block',
              'sub_fields' => array (
                array (
                  'key' => 'field_tutsu_forms_submissions_appointment_fname',
                  'label' => 'First Name',
                  'name' => 'fname',
                  'type' => 'text',
                  'wrapper' => array (
                    'width' => '50',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_appointment_lname',
                  'label' => 'Last Name',
                  'name' => 'lname',
                  'type' => 'text',
                  'wrapper' => array (
                    'width' => '50',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_appointment_parent_fname',
                  'label' => 'Parent First Name',
                  'name' => 'parent_fname',
                  'type' => 'text',
                  'wrapper' => array (
                    'width' => '50',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_appointment_parent_lname',
                  'label' => 'Parent Last Name',
                  'name' => 'parent_lname',
                  'type' => 'text',
                  'wrapper' => array (
                    'width' => '50',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_appointment_email',
                  'label' => 'Email',
                  'name' => 'email',
                  'type' => 'email',
                  'wrapper' => array (
                    'width' => '50',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_appointment_phone',
                  'label' => 'Phone',
                  'name' => 'phone',
                  'type' => 'text',
                  'wrapper' => array (
                    'width' => '50',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_appointment_prefered_time',
                  'label' => 'Preferred Time',
                  'name' => 'preferred_time',
                  'type' => 'text',
                  'wrapper' => array (
                    'width' => '30',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_appointment_source',
                  'label' => 'Source',
                  'name' => 'source',
                  'type' => 'text',
                  'wrapper' => array (
                    'width' => '50',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_appointment_verified',
                  'label' => 'Verified',
                  'name' => 'verified',
                  'type' => 'true_false',
                  'wrapper' => array (
                    'width' => '20',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_appointment_message',
                  'label' => 'Dental Needs',
                  'name' => 'message',
                  'type' => 'textarea',
                  'wrapper' => array (
                    'width' => '100',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_appointment_key',
                  'label' => '',
                  'name' => 'key',
                  'class' => 'hidden',
                  'type' => 'password',
                  'wrapper' => array (
                    'width' => '0',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_appointment_page_id',
                  'label' => '',
                  'name' => 'page_id',
                  'class' => 'hidden',
                  'type' => 'text',
                  'wrapper' => array (
                    'width' => '0',
                  ),
                ),
              ),
            ),
          ),
          'location' => array (
            array (
              array (
                'param' => 'options_page',
                'operator' => '==',
                'value' => 'theme-settings-forms-appointment',
              ),
            ),
          ),
          'menu_order' => 0,
          'position' => 'normal',
          'style' => 'default',
          'label_placement' => 'top',
          'instruction_placement' => 'label',
          'active' => 1,
        )
      );
    }

  // REVIEW US SUBMISSIONS
    if(in_array('review_us', $activates)){
      acf_add_local_field_group(
        array (
          'key' => 'group_tutsu_forms_submissions_review_us',
          'title' => 'Submissions',
          'fields' => array (
            array (
              'key' => 'field_tutsu_forms_submissions_review_us',
              'label' => '',
              'name' => 'tutsu_forms_submissions_review_us',
              'type' => 'repeater',
              'layout' => 'block',
              'sub_fields' => array (
                array (
                  'key' => 'field_tutsu_forms_submissions_review_us_rating',
                  'label' => 'Rating',
                  'name' => 'rating',
                  'type' => 'number',
                  'min' => '0',
                  'max' => '5',
                  'wrapper' => array (
                    'width' => '80',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_review_us_verified',
                  'label' => 'Verified',
                  'name' => 'verified',
                  'type' => 'true_false',
                  'wrapper' => array (
                    'width' => '20',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_review_us_fname',
                  'label' => 'First Name',
                  'name' => 'fname',
                  'type' => 'text',
                  'wrapper' => array (
                    'width' => '50',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_review_us_lname',
                  'label' => 'Last Name',
                  'name' => 'lname',
                  'type' => 'text',
                  'wrapper' => array (
                    'width' => '50',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_review_us_email',
                  'label' => 'Email',
                  'name' => 'email',
                  'type' => 'email',
                  'wrapper' => array (
                    'width' => '50',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_review_us_phone',
                  'label' => 'Phone',
                  'name' => 'phone',
                  'type' => 'text',
                  'wrapper' => array (
                    'width' => '50',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_review_us_subject',
                  'label' => 'Subject',
                  'name' => 'subject',
                  'type' => 'text',
                  'wrapper' => array (
                    'width' => '100',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_review_us_message',
                  'label' => 'Review',
                  'name' => 'message',
                  'type' => 'textarea',
                  'wrapper' => array (
                    'width' => '100',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_review_us_key',
                  'label' => '',
                  'name' => 'key',
                  'class' => 'hidden',
                  'type' => 'password',
                  'wrapper' => array (
                    'width' => '0',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_review_us_page_id',
                  'label' => '',
                  'name' => 'page_id',
                  'class' => 'hidden',
                  'type' => 'text',
                  'wrapper' => array (
                    'width' => '0',
                  ),
                ),
              ),
            ),
          ),
          'location' => array (
            array (
              array (
                'param' => 'options_page',
                'operator' => '==',
                'value' => 'theme-settings-forms-review-us',
              ),
            ),
          ),
          'menu_order' => 0,
          'position' => 'normal',
          'style' => 'default',
          'label_placement' => 'top',
          'instruction_placement' => 'label',
          'active' => 1,
        )
      );
    }

  // REFER US SUBMISSIONS
    if(in_array('refer_us', $activates)){
      acf_add_local_field_group(
        array (
          'key' => 'group_tutsu_forms_submissions_refer_us',
          'title' => 'Submissions',
          'fields' => array (
            array (
              'key' => 'field_tutsu_forms_submissions_refer_us',
              'label' => '',
              'name' => 'tutsu_forms_submissions_refer_us',
              'type' => 'repeater',
              'layout' => 'block',
              'sub_fields' => array (
                array (
                  'key' => 'field_tutsu_forms_submissions_refer_us_fname',
                  'label' => 'First Name',
                  'name' => 'fname',
                  'type' => 'text',
                  'wrapper' => array (
                    'width' => '50',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_refer_us_lname',
                  'label' => 'Last Name',
                  'name' => 'lname',
                  'type' => 'text',
                  'wrapper' => array (
                    'width' => '50',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_refer_us_friends_fname',
                  'label' => 'Friend\'s First Name',
                  'name' => 'fname',
                  'type' => 'text',
                  'wrapper' => array (
                    'width' => '50',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_refer_us_friends_lname',
                  'label' => 'Friend\'s Last Name',
                  'name' => 'lname',
                  'type' => 'text',
                  'wrapper' => array (
                    'width' => '50',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_refer_us_email',
                  'label' => 'Email',
                  'name' => 'email',
                  'type' => 'email',
                  'wrapper' => array (
                    'width' => '40',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_refer_us_friends_email',
                  'label' => 'Friend\'s Email',
                  'name' => 'friends_email',
                  'type' => 'email',
                  'wrapper' => array (
                    'width' => '40',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_refer_us_verified',
                  'label' => 'Verified',
                  'name' => 'verified',
                  'type' => 'true_false',
                  'wrapper' => array (
                    'width' => '20',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_refer_us_key',
                  'label' => '',
                  'name' => 'key',
                  'class' => 'hidden',
                  'type' => 'password',
                  'wrapper' => array (
                    'width' => '0',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_refer_us_page_id',
                  'label' => '',
                  'name' => 'page_id',
                  'class' => 'hidden',
                  'type' => 'text',
                  'wrapper' => array (
                    'width' => '0',
                  ),
                ),
              ),
            ),
          ),
          'location' => array (
            array (
              array (
                'param' => 'options_page',
                'operator' => '==',
                'value' => 'theme-settings-forms-refer-us',
              ),
            ),
          ),
          'menu_order' => 0,
          'position' => 'normal',
          'style' => 'default',
          'label_placement' => 'top',
          'instruction_placement' => 'label',
          'active' => 1,
        )
      );
    }

    // NEWSLETTER SUBMISSIONS
    if(in_array('newsletter', $activates)){
      acf_add_local_field_group(
        array (
          'key' => 'group_tutsu_forms_submissions_newsletter',
          'title' => 'Submissions',
          'fields' => array (
            array (
              'key' => 'field_tutsu_forms_submissions_newsletter',
              'label' => '',
              'name' => 'tutsu_forms_submissions_newsletter',
              'type' => 'repeater',
              'layout' => 'block',
              'sub_fields' => array (
                array (
                  'key' => 'field_tutsu_forms_submissions_newsletter_fname',
                  'label' => 'First Name',
                  'name' => 'fname',
                  'type' => 'text',
                  'wrapper' => array (
                    'width' => '50',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_newsletter_lname',
                  'label' => 'Last Name',
                  'name' => 'lname',
                  'type' => 'text',
                  'wrapper' => array (
                    'width' => '50',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_newsletter_email',
                  'label' => 'Email',
                  'name' => 'email',
                  'type' => 'email',
                  'wrapper' => array (
                    'width' => '80',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_newsletter_verified',
                  'label' => 'Verified',
                  'name' => 'verified',
                  'type' => 'true_false',
                  'wrapper' => array (
                    'width' => '20',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_newsletter_key',
                  'label' => '',
                  'name' => 'key',
                  'class' => 'hidden',
                  'type' => 'password',
                  'wrapper' => array (
                    'width' => '0',
                  ),
                ),
                array (
                  'key' => 'field_tutsu_forms_submissions_newsletter_page_id',
                  'label' => '',
                  'name' => 'page_id',
                  'class' => 'hidden',
                  'type' => 'text',
                  'wrapper' => array (
                    'width' => '0',
                  ),
                ),
              ),
            ),
          ),
          'location' => array (
            array (
              array (
                'param' => 'options_page',
                'operator' => '==',
                'value' => 'theme-settings-forms-newsletter',
              ),
            ),
          ),
          'menu_order' => 0,
          'position' => 'normal',
          'style' => 'default',
          'label_placement' => 'top',
          'instruction_placement' => 'label',
          'active' => 1,
        )
      );
    }

  }



  // SUBMISSIONS
  acf_add_local_field_group(
    array (
      'key' => 'group_tutsu_forms',
      'title' => 'Submissions',
      'fields' => array (
        array (
          'key' => 'field_tutsu_forms',
          'label' => '',
          'name' => 'tutsu_forms',
          'type' => 'repeater',
          'layout' => 'block',
          'sub_fields' => array (
            array (
              'key' => 'field_tutsu_forms_name',
              'label' => 'Name',
              'name' => 'name',
              'type' => 'text',
              'wrapper' => array (
                'width' => '30',
              ),
            ),
            array (
              'key' => 'field_tutsu_forms_email',
              'label' => 'Email',
              'name' => 'email',
              'type' => 'email',
              'wrapper' => array (
                'width' => '30',
              ),
            ),
            array (
              'key' => 'field_tutsu_forms_phone',
              'label' => 'Phone',
              'name' => 'phone',
              'type' => 'text',
              'wrapper' => array (
                'width' => '30',
              ),
            ),
            array (
              'key' => 'field_tutsu_forms_verified',
              'label' => 'Verified',
              'name' => 'verified',
              'type' => 'true_false',
              'wrapper' => array (
                'width' => '10',
              ),
            ),
            array (
              'key' => 'field_tutsu_forms_message',
              'label' => 'Message',
              'name' => 'message',
              'type' => 'textarea',
              'wrapper' => array (
                'width' => '100',
              ),
            ),
            array (
              'key' => 'field_tutsu_forms_id',
              'label' => '',
              'name' => 'id',
              'class' => 'hidden',
              'type' => 'password',
              'wrapper' => array (
                'width' => '0',
              ),
            ),
          ),
        ),
      ),
      'location' => array (
        array (
          array (
            'param' => 'options_page',
            'operator' => '==',
            'value' => 'theme-settings-forms-submissions',
          ),
        ),
      ),
      'menu_order' => 0,
      'position' => 'normal',
      'style' => 'default',
      'label_placement' => 'top',
      'instruction_placement' => 'label',
      'active' => 1,
    )
  );
}