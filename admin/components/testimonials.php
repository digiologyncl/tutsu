<?php

if( function_exists('acf_add_options_page') ) {
  acf_add_options_sub_page(
		array(
	    'page_title'  => 'Testimonials',
	    'menu_title'  => 'Testimonials',
	    'parent_slug' => 'theme-settings',
	    'menu_slug'   => 'theme-settings-testimonials',
	  )
  );
}

if( function_exists('acf_add_local_field_group') ) {
	acf_add_local_field_group(
	  array (
	    'key' => 'group_testimonials',
	    'title' => 'Testimonials',
	    'fields' => array (
	      array (
	        'key' => 'field_testimonials',
	        'label' => 'Testimonials',
	        'name' => 'testimonials',
	        'type' => 'repeater',
	        'sub_fields' => array(
			      array (
			        'key' => 'sub_field_name',
			        'label' => 'Name',
			        'name' => 'name',
			        'type' => 'text',
			        'wrapper' => array (
								'width' => '50%',
							),
			      ),
			      array(
			        'key' => 'sub_field_role',
			        'label' => 'Role',
			        'name' => 'role',
			        'type' => 'text',
			        'wrapper' => array (
								'width' => '50%',
							),
			      ),
			      array(
			        'key' => 'sub_field_location',
			        'label' => 'Location',
			        'name' => 'location',
			        'type' => 'text',
			        'wrapper' => array (
								'width' => '50%',
							),
			      ),
			      array(
			        'key' => 'sub_field_rating',
			        'label' => 'Rating',
			        'name' => 'rating',
			        'type' => 'number',
			        'wrapper' => array (
								'width' => '50%',
							),
							'placeholder' => '5',
							'min' => '0',
							'max' => '5',
							'step' => '0.5'
			      ),
			      array(
			        'key' => 'sub_field_photo',
			        'label' => 'Photo',
			        'name' => 'photo',
			        'type' => 'image'
			      ),
			      array (
			        'key' => 'sub_field_title',
			        'label' => 'Title',
			        'name' => 'title',
			        'type' => 'text',
			        'wrapper' => array (
								'width' => '50%',
							),
			      ),
			      array (
			        'key' => 'sub_field_date',
			        'label' => 'Date',
			        'name' => 'date',
			        'type' => 'date_picker',
			        'wrapper' => array (
								'width' => '50%',
							),
			      ),
			      array(
			        'key' => 'sub_field_review',
			        'label' => 'Review',
			        'name' => 'review',
			        'type' => 'wysiwyg'
			      ),
			      array(
			        'key' => 'sub_field_featured',
			        'label' => 'Featured',
			        'name' => 'featured',
			        'type' => 'true_false'
			      ),
		      ),
			    'layout' => 'block',
			    'button_label' => 'Add Testimonial'
	      ),
	    ),
	    'location' => array (
	      array (
	        array (
	          'param' => 'options_page',
	          'operator' => '==',
	          'value' => 'theme-settings-testimonials',
	        ),
	      ),
	    ),
	    'menu_order' => 1,
	    'position' => 'normal',
	    'style' => 'default',
	    'label_placement' => 'top',
	    'instruction_placement' => 'label',
	    'hide_on_screen' => '',
	  )
	);
}