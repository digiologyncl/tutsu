<?php

if( function_exists('acf_add_local_field_group') ) {
  acf_add_local_field_group(
    array (
      'key' => 'group_sections',
      'title' => 'Sections',
      'fields' => array (
        array (
          'key' => 'field_sections',
          'label' => 'Sections',
          'name' => 'sections',
          'type' => 'flexible_content',
          'instructions' => 'Placed above footer, below content.',
          'button_label' => 'Add Sections',
          'layouts' => array (
            array (
              'key' => 'field_section',
              'name' => 'section',
              'label' => 'Section',
              'display' => 'block',
              'sub_fields' => array (
                array (
                  'key' => 'field_section_active',
                  'label' => 'Active',
                  'name' => 'active',
                  'type' => 'true_false',
                  'default_value' => 1,
                  'wrapper' => array (
                    'width' => '20%',
                  ),
                ),
                array (
                  'key' => 'field_section_class',
                  'label' => 'Class',
                  'name' => 'class',
                  'type' => 'text',
                  'wrapper' => array (
                    'width' => '40%',
                  ),
                ),
                array (
                  'key' => 'field_section_id',
                  'label' => 'ID',
                  'name' => 'id',
                  'type' => 'text',
                  'wrapper' => array (
                    'width' => '40%',
                  ),
                ),
                array (
                  'key' => 'field_section_content',
                  'label' => 'Content',
                  'name' => 'content',
                  'type' => 'wysiwyg'
                ),
                array (
                  'key' => 'field_section_image',
                  'label' => 'Background Image:',
                  'name' => 'image',
                  'type' => 'image',
                  'wrapper' => array (
                    'width' => '60%',
                  ),
                ),
                array (
                  'key' => 'field_section_container',
                  'label' => 'Container Size',
                  'name' => 'container',
                  'type' => 'radio',
                  'choices' => array(
                    '' => 'Normal',
                    '-xs' => 'Small',
                    '-sm' => 'Medium',
                    '-full' => 'Full'
                  ),
                  'wrapper' => array (
                    'width' => '40%',
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
      'location' => array (
        array (
          array (
            'param' => 'post_type',
            'operator' => '!=',
            'value' => 'post',
          ),
          array (
            'param' => 'post_type',
            'operator' => '!=',
            'value' => 'form-submission',
          ),
        ),
      ),
      'menu_order' => 2,
      'position' => 'normal',
      'style' => 'default',
      'label_placement' => 'top',
      'instruction_placement' => 'label',
      'hide_on_screen' => '',
    )
  );
}