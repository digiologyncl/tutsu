<?php

if( function_exists('acf_add_local_field_group') ) {
  acf_add_local_field_group(
    array (
      'key' => 'group_featured_icon',
      'title' => 'Featured Icon',
      'fields' => array (
        array (
          'key' => 'field_featured_icon',
          'label' => 'Featured Icon',
          'name' => 'featured_icon',
          'type' => 'image',
        ),
      ),
      'location' => array (
        array (
          array (
            'param' => 'post_type',
            'operator' => '==',
            'value' => 'page',
          ),
        ),
      ),
      'menu_order' => 99,
      'position' => 'side',
      'style' => 'default',
      'label_placement' => 'top',
      'instruction_placement' => 'label',
      'hide_on_screen' => '',
    )
  );
}


