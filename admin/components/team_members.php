<?php

if( function_exists('acf_add_options_page') ) {
  acf_add_options_sub_page(
	  array(
	    'page_title'  => 'Team Members',
	    'menu_title'  => 'Team Members',
	    'parent_slug' => 'theme-settings',
	    'menu_slug'   => 'theme-settings-team-members',
	  )
  );
}

if( function_exists('acf_add_local_field_group') ) {
	acf_add_local_field_group(
	  array (
	    'key' => 'group_team_members',
	    'title' => 'Team Members',
	    'fields' => array (
	      array (
	        'key' => 'field_team_members',
	        'label' => 'Team Members',
	        'name' => 'team_members',
	        'type' => 'flexible_content',
			    'button_label' => 'Add Team Member',
          'layouts' => array (
            array (
              'key' => 'field_team_member',
              'name' => 'team_member',
              'label' => 'Team Member',
              'display' => 'block',
              'sub_fields' => array (
				      array(
				        'key' => 'sub_field_class',
				        'label' => 'Class',
				        'name' => 'class',
				        'type' => 'text',
				        'wrapper' => array (
									'width' => '50%',
									),
					      ),
					      array(
					        'key' => 'sub_field_id',
					        'label' => 'ID',
					        'name' => 'id',
					        'type' => 'text',
					        'wrapper' => array (
										'width' => '50%',
									),
					      ),
					      array (
					        'key' => 'sub_field_name',
					        'label' => 'Name',
					        'name' => 'name',
					        'type' => 'text',
					        'wrapper' => array (
										'width' => '50%',
									),
					      ),
					      array(
					        'key' => 'sub_field_role',
					        'label' => 'Role',
					        'name' => 'role',
					        'type' => 'text',
					        'wrapper' => array (
										'width' => '50%',
									),
					      ),
					      array(
					        'key' => 'sub_field_photo',
					        'label' => 'Photo',
					        'name' => 'photo',
					        'type' => 'image',
					      ),
					      array(
					        'key' => 'sub_field_bio',
					        'label' => 'Bio',
					        'name' => 'bio',
					        'type' => 'wysiwyg'
					      ),
					      array(
					        'key' => 'sub_field_phone',
					        'label' => 'Phone',
					        'name' => 'phone',
					        'type' => 'text',
					        'wrapper' => array (
										'width' => '50%',
									),
					      ),
					      array(
					        'key' => 'sub_field_email',
					        'label' => 'Email',
					        'name' => 'email',
					        'type' => 'email',
					        'wrapper' => array (
										'width' => '50%',
									),
					      ),
					      array(
					        'key' => 'sub_field_twitter',
					        'label' => 'Twitter',
					        'name' => 'twitter',
					        'type' => 'url',
					        'wrapper' => array (
										'width' => '50%',
									),
					      ),
					      array(
					        'key' => 'sub_field_linkedin',
					        'label' => 'Linkedin',
					        'name' => 'linkedin',
					        'type' => 'url',
					        'wrapper' => array (
										'width' => '50%',
									),
					      ),
					      array(
					        'key' => 'sub_field_featured',
					        'label' => 'Featured',
					        'name' => 'featured',
					        'type' => 'true_false',
					      ),
				      ),
			      ),
		      ),
	      ),
	    ),
	    'location' => array (
	      array (
	        array (
	          'param' => 'options_page',
	          'operator' => '==',
	          'value' => 'theme-settings-team-members',
	        ),
	      ),
	    ),
	    'menu_order' => 1,
	    'position' => 'normal',
	    'style' => 'default',
	    'label_placement' => 'top',
	    'instruction_placement' => 'label',
	    'hide_on_screen' => '',
	  )
	);
}