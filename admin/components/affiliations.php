<?php

if( function_exists('acf_add_options_page') ) {
  acf_add_options_sub_page(array(
    'page_title'  => 'Affiliations',
    'menu_title'  => 'Affiliations',
    'parent_slug' => 'theme-settings',
    'menu_slug'   => 'theme-settings-affiliations',
  ));
}

if( function_exists('acf_add_local_field_group') ) {
	acf_add_local_field_group(
	  array (
	    'key' => 'group_affiliations',
	    'title' => 'Affiliations',
	    'fields' => array (
	      array (
	        'key' => 'field_affiliations',
	        'label' => 'Affiliations',
	        'name' => 'affiliations',
	        'type' => 'gallery'
	      ),
	    ),
	    'location' => array (
	      array (
	        array (
	          'param' => 'options_page',
	          'operator' => '==',
	          'value' => 'theme-settings-affiliations',
	        ),
	      ),
	    ),
	    'menu_order' => 1,
	    'position' => 'normal',
	    'style' => 'default',
	    'label_placement' => 'left',
	    'instruction_placement' => 'label',
	    'hide_on_screen' => '',
	  )
	);
}