<?php

if( function_exists('acf_add_options_page') ) {
  acf_add_options_sub_page(
    array(
      'page_title'  => 'Hello Bar',
      'menu_title'  => 'Hello Bar',
      'parent_slug' => 'theme-settings',
      'menu_slug'   => 'theme-settings-hello-bar',
    )
  );
}

if( function_exists('acf_add_local_field_group') ) {
  acf_add_local_field_group(
    array (
      'key' => 'group_hello_bar',
      'title' => 'Hello Bar',
      'fields' => array (
        array (
          'key' => 'field_hello_bar_content',
          'label' => 'Content',
          'name' => 'hello_bar_content',
          'type' => 'text',
          'wrapper' => array (
            'width' => '70%',
          ),
        ),
        array (
          'key' => 'field_hello_bar_class',
          'label' => 'Class',
          'name' => 'hello_bar_class',
          'type' => 'text',
          'wrapper' => array (
            'width' => '30%',
          ),
        ),
        array (
          'key' => 'field_hello_bar_active',
          'label' => 'Active',
          'name' => 'hello_bar_active',
          'type' => 'true_false',
          'default_value' => 1,
          'wrapper' => array (
            'width' => '15%',
          ),
        ),
        array (
          'key' => 'field_hello_bar_all',
          'label' => 'All',
          'name' => 'hello_bar_all',
          'type' => 'true_false',
          'default_value' => 0,
          'instructions' => 'Display on the all pages.',
          'wrapper' => array (
            'width' => '15%',
          ),
        ),
        array (
          'return_format' => 'id',
          'key' => 'field_hello_bar_include',
          'label' => 'Include',
          'name' => 'hello_bar_include',
          'type' => 'relationship',
          'instructions' => 'Display on the following pages.',
          'post_type' => array (
            0 => 'page',
          ),
          'filters' => array (
            0 => 'search',
          ),
          'conditional_logic' => array (
            array (
              array (
                'field' => 'field_hello_bar_all',
                'operator' => '!=',
                'value' => 1,
              ),
            ),
          ),
          'wrapper' => array (
            'width' => '70%',
          ),
        ),
        array (
          'return_format' => 'id',
          'key' => 'field_hello_bar_exceptions',
          'label' => 'Exceptions',
          'name' => 'hello_bar_exceptions',
          'type' => 'relationship',
          'instructions' => 'Do not display on the following pages.',
          'post_type' => array (
            0 => 'page',
          ),
          'filters' => array (
            0 => 'search',
          ),
          'conditional_logic' => array (
            array (
              array (
                'field' => 'field_hello_bar_all',
                'operator' => '==',
                'value' => 1,
              ),
            ),
          ),
          'wrapper' => array (
            'width' => '70%',
          ),
        ),
      ),
      'location' => array (
        array (
          array (
            'param' => 'options_page',
            'operator' => '==',
            'value' => 'theme-settings-hello-bar',
          ),
        ),
      ),
      'menu_order' => 0,
      'position' => 'normal',
      'style' => 'default',
      'label_placement' => 'top',
      'instruction_placement' => 'label',
      'hide_on_screen' => '',
    )
  );
}