<?php

if( function_exists('acf_add_options_page') ) {
  acf_add_options_sub_page(
    array(
      'page_title'  => 'Banners',
      'menu_title'  => 'Banners',
      'parent_slug' => 'theme-settings',
      'menu_slug'   => 'theme-settings-banners',
    )
  );
}

if( function_exists('acf_add_local_field_group') ) {
  acf_add_local_field_group(
    array (
      'key' => 'group_banners',
      'title' => 'Banners',
      'fields' => array (
        array (
          'key' => 'field_banners',
          'label' => 'Banners',
          'name' => 'banners',
          'type' => 'flexible_content',
          'instructions' => 'Placed above footer, below content.',
          'button_label' => 'Add Banners',
          'layouts' => array (
            array (
              'key' => 'field_banner',
              'name' => 'banner',
              'label' => 'Banner',
              'display' => 'block',
              'sub_fields' => array (
                array (
                  'key' => 'field_banner_active',
                  'label' => 'Active',
                  'name' => 'active',
                  'type' => 'true_false',
                  'default_value' => 1,
                  'wrapper' => array (
                    'width' => '20%',
                  ),
                ),
                array (
                  'key' => 'field_banner_class',
                  'label' => 'Class',
                  'name' => 'class',
                  'type' => 'text',
                  'wrapper' => array (
                    'width' => '40%',
                  ),
                ),
                array (
                  'key' => 'field_banner_id',
                  'label' => 'ID',
                  'name' => 'id',
                  'type' => 'text',
                  'wrapper' => array (
                    'width' => '40%',
                  ),
                ),
                array (
                  'key' => 'field_banner_content',
                  'label' => 'Content',
                  'name' => 'content',
                  'type' => 'wysiwyg',
                  'wrapper' => array (
                    'width' => '100%',
                  ),
                ),
                array (
                  'key' => 'field_banner_image',
                  'label' => 'Background Image:',
                  'name' => 'image',
                  'type' => 'image',
                  'wrapper' => array (
                    'width' => '30%',
                  ),
                ),
                array (
                  'key' => 'field_banner_all',
                  'label' => 'All',
                  'name' => 'all',
                  'type' => 'true_false',
                  'default_value' => 0,
                  'instructions' => 'Display on the all pages.',
                  'wrapper' => array (
                    'width' => '20%',
                  ),
                ),
                array (
                  'return_format' => 'id',
                  'key' => 'field_banner_include',
                  'label' => 'Include',
                  'name' => 'include',
                  'type' => 'relationship',
                  'instructions' => 'Display on the following pages.',
                  'post_type' => array (
                    0 => 'page',
                  ),
                  'filters' => array (
                    0 => 'search',
                  ),
                  'conditional_logic' => array (
                    array (
                      array (
                        'field' => 'field_banner_all',
                        'operator' => '!=',
                        'value' => 1,
                      ),
                    ),
                  ),
                  'wrapper' => array (
                    'width' => '50%',
                  ),
                ),
                array (
                  'return_format' => 'id',
                  'key' => 'field_banner_exceptions',
                  'label' => 'Exceptions',
                  'name' => 'exceptions',
                  'type' => 'relationship',
                  'instructions' => 'Do not display on the following pages.',
                  'post_type' => array (
                    0 => 'page',
                  ),
                  'filters' => array (
                    0 => 'search',
                  ),
                  'conditional_logic' => array (
                    array (
                      array (
                        'field' => 'field_banner_all',
                        'operator' => '==',
                        'value' => 1,
                      ),
                    ),
                  ),
                  'wrapper' => array (
                    'width' => '50%',
                  ),
                ),
                array (
                  'key' => 'field_banner_container',
                  'label' => 'Container Size',
                  'name' => 'container',
                  'type' => 'select',
                  'choices' => array(
                    '' => 'Normal',
                    '-xs' => 'Small',
                    '-sm' => 'Medium',
                    '-full' => 'Full'
                  ),
                  'wrapper' => array (
                    'width' => '100%',
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
      'location' => array (
        array (
          array (
            'param' => 'options_page',
            'operator' => '==',
            'value' => 'theme-settings-banners',
          ),
        ),
      ),
      'menu_order' => 2,
      'position' => 'normal',
      'style' => 'default',
      'label_placement' => 'top',
      'instruction_placement' => 'label',
      'hide_on_screen' => '',
    )
  );
}