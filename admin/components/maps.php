<?php

if( function_exists('acf_add_options_page') ) {
  acf_add_options_sub_page(array(
    'page_title'  => 'Maps',
    'menu_title'  => 'Maps',
    'parent_slug' => 'theme-settings',
    'menu_slug'   => 'theme-settings-google-maps',
  ));
}

$styles = '[{"stylers":[{"saturation":-100},{"gamma":1}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"labels.text","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"poi.place_of_worship","elementType":"labels.text","stylers":[{"visibility":"off"}]},{"featureType":"poi.place_of_worship","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"geometry","stylers":[{"visibility":"simplified"}]},{"featureType":"water","stylers":[{"visibility":"on"},{"saturation":50},{"gamma":0},{"hue":"#50a5d1"}]},{"featureType":"administrative.neighborhood","elementType":"labels.text.fill","stylers":[{"color":"#333333"}]},{"featureType":"road.local","elementType":"labels.text","stylers":[{"weight":0.5},{"color":"#333333"}]},{"featureType":"transit.station","elementType":"labels.icon","stylers":[{"gamma":1},{"saturation":50}]}]';

if( function_exists('acf_add_local_field_group') ){
  acf_add_local_field_group(
    array (
      'key' => 'group_api_google_maps',
      'title' => 'Google Maps',
      'fields' => array (
        array (
          'key' => 'field_activate_google_maps',
          'label' => 'Activate Google Maps API',
          'name' => 'activate_google_maps',
          'type' => 'true_false',
          'default_value' => false
        ),
        array (
          'key' => 'field_api_google_maps',
          'label' => 'Key',
          'name' => 'api_google_maps',
          'type' => 'text',
          'instructions' => '<a href="https://developers.google.com/maps/documentation/javascript/" target="_blank">Get a Key</a>'
        ),
        array (
          'key' => 'field_google_maps_zoom',
          'label' => 'Default Zoom',
          'name' => 'google_maps_zoom',
          'type' => 'number',
          'default_value' => '15',
        ),
        array (
          'key' => 'field_google_maps_styles',
          'label' => 'Styles',
          'name' => 'google_maps_styles',
          'type' => 'textarea',
          'new_lines' => '',
          'default_value' => $styles,
          'placeholder' => $styles,
          'instructions' => 'Check <a href="https://snazzymaps.com/" target="_blank">snazzymaps.com</a> or <a href="https://mapstyle.withgoogle.com/" target="_blank">mapstyle.withgoogle.com</a>'
        ),
        array (
          'key' => 'field_google_maps_icon',
          'label' => 'Default Icon',
          'name' => 'google_maps_icon',
          'type' => 'image',
          'return_format' => 'array',
          'preview_size' => 'thumbnail',
          'library' => 'all',
          'max_width' => 100,
          'max_height' => 100,
          'mime_types' => 'gif, png',
          'instructions' => '100x100px max, PNG or GIF'
        ),
      ),
      'location' => array (
        array (
          array (
            'param' => 'options_page',
            'operator' => '==',
            'value' => 'theme-settings-google-maps'
          ),
        ),
      ),
      'menu_order' => 0,
      'position' => 'normal',
      'style' => 'default',
      'label_placement' => 'left',
      'instruction_placement' => 'label',
      'hide_on_screen' => '',
    )
  );
}


if( function_exists('acf_add_local_field_group') && get_field('api_google_maps', 'options') ){

	acf_add_local_field_group(array (
	  'key' => 'group_google_maps_maps',
	  'title' => 'Maps',
	  'fields' => array (
	    array (
	      'key' => 'field_google_maps_maps',
	      'label' => 'Maps',
	      'name' => 'maps',
	      'type' => 'repeater',
	      'instructions' => '',
	      'required' => 0,
	      'conditional_logic' => 0,
	      'wrapper' => array (
	        'width' => '',
	        'class' => '',
	        'id' => '',
	      ),
	      'collapsed' => '',
	      'min' => 0,
	      'max' => 0,
	      'layout' => 'block',
	      'button_label' => 'Add map',
	      'sub_fields' => array (
	        array (
	          'key' => 'field_google_maps_locations',
	          'label' => 'Locations',
	          'name' => 'locations',
	          'type' => 'repeater',
	          'layout' => 'block',
	          'button_label' => 'Add marker',
	          'sub_fields' => array (
	            array (
	              'key' => 'field_google_maps_location',
	              'label' => 'Location',
	              'name' => 'location',
	              'type' => 'google_map',
	              'height' => 200,
	            ),
	            array (
	              'key' => 'field_google_maps_title',
	              'label' => 'Title',
	              'name' => 'title',
	              'type' => 'text',
	            ),
	            array (
	              'key' => 'field_google_maps_description',
	              'label' => 'Description',
	              'name' => 'description',
	              'type' => 'textarea',
	              'new_lines' => 'wpautop',
	            ),
	          ),
	        ),
	      ),
	    ),
	  ),
	  'location' => array (
	    array (
	      array (
	        'param' => 'options_page',
	        'operator' => '==',
	        'value' => 'theme-settings-google-maps',
	      ),
	    ),
	  ),
	  'menu_order' => 0,
	  'position' => 'normal',
	  'style' => 'default',
	  'label_placement' => 'top',
	  'instruction_placement' => 'label',
	  'hide_on_screen' => '',
	  'active' => 1,
	  'description' => '',
	));

}