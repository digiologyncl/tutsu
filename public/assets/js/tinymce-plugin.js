jQuery( document ).ready( function( $ )
{
  tinymce.create( 'tinymce.plugins.tinymce_extra_buttons' ,
  {
    init : function( ed , url )
    {
      // Register commands

      // ----------------------------------------------------------------------------------------------------

      ed.addCommand( 'insert_address_shortcode' , function()
      {

        content = '<p>[address class="address" inline=0 icon=0 schema=1 name=1 p=1]</p>';

        tinymce.execCommand( 'mceInsertContent' , false , content );
      });

      // ----------------------------------------------------------------------------------------------------

      ed.addCommand( 'insert_btn_shortcode' , function()
      {
        selected = tinyMCE.activeEditor.selection.getContent();
        if( selected )
        {
          content = '[btn class="btn-1" id=""]'+selected+'[/btn]';
        }
        else
        {
          content = '[btn class="btn-1" id=""]Title[/btn]';
        }

        tinymce.execCommand( 'mceInsertContent' , false , content );
      });

      // ----------------------------------------------------------------------------------------------------

      ed.addCommand( 'insert_card_shortcode' , function()
      {
        selected = tinyMCE.activeEditor.selection.getContent();
        if( selected )
        {
          content = '<p>[card title="Title" class="" id="" width=""]</p><p>'+selected+'</p><p>[/card]</p>';
        }
        else
        {
          content = '<p>[card title="Title" class="" id="" width=""]</p><p>Content</p><p>[/card]</p>';
        }

        tinymce.execCommand( 'mceInsertContent' , false , content );
      });

      // ----------------------------------------------------------------------------------------------------

      ed.addCommand( 'insert_cols_shortcode' , function()
      {
        selected = tinyMCE.activeEditor.selection.getContent();
        if( selected )
        {
          content = '<p>[cols class=""]</p><p>[col xs=12 sm="" md=6 lg="" xl="" class=""]</p><p>'+selected+'</p><p>[/col]</p><p>[col md=6]</p><p>Content</p><p>[/col]</p><p>[/cols]</p>';
        }
        else
        {
          content = '<p>[cols class=""]</p><p>[col xs=12 sm="" md=4 lg="" xl="" class=""]</p><p>Content</p><p>[/col]</p><p>[col md=4]</p><p>Content</p><p>[/col]</p><p>[col md=4]</p><p>Content</p><p>[/col]</p><p>[/cols]</p>';
        }

        tinymce.execCommand( 'mceInsertContent' , false , content );
      });

      // ----------------------------------------------------------------------------------------------------

      ed.addCommand( 'insert_email_shortcode' , function()
      {

        content = '[email class="email" link=1 icon=0 p=0]';

        tinymce.execCommand( 'mceInsertContent' , false , content );
      });

      // ----------------------------------------------------------------------------------------------------

      ed.addCommand( 'insert_faqs_shortcode' , function()
      {
        selected = tinyMCE.activeEditor.selection.getContent();
        if( selected )
        {
          content = '<p>[faqs class="" width=""]</p><p>[faq title="Title" active=1 class=""]</p><p>'+selected+'</p><p>[/faq]</p><p>[faq title="Title"]</p><p>Content</p><p>[/faq]</p><p>[/faqs]</p>';
        }
        else
        {
          content = '<p>[faqs]</p><p>[faq title="Title" active=1]</p><p>Content</p><p>[/faq]</p><p>[faq title="Title2"]</p><p>Content2</p><p>[/faq]</p><p>[/faqs]</p>';
        }

        tinymce.execCommand( 'mceInsertContent' , false , content );
      });

      // ----------------------------------------------------------------------------------------------------

      ed.addCommand( 'insert_fax_shortcode' , function()
      {

        content = '[fax class="fax" link=1 icon=0 p=0]';

        tinymce.execCommand( 'mceInsertContent' , false , content );
      });

      // ----------------------------------------------------------------------------------------------------

      ed.addCommand( 'insert_icon_shortcode' , function()
      {

        content = '[icon icon="circle" size="" class=""]';

        tinymce.execCommand( 'mceInsertContent' , false , content );
      });

      // ----------------------------------------------------------------------------------------------------

      ed.addCommand( 'insert_lead_shortcode' , function()
      {
        selected = tinyMCE.activeEditor.selection.getContent();
        if( selected )
        {
          content = '<p>[lead]'+selected+'[/lead]</p>';
        }
        else
        {
          content = '<p>[lead]Content[/lead]</p>';
        }

        tinymce.execCommand( 'mceInsertContent' , false , content );
      });

      // ----------------------------------------------------------------------------------------------------

      ed.addCommand( 'insert_map_shortcode' , function()
      {

        content = '<p>[map class="" width=""]</p>';

        tinymce.execCommand( 'mceInsertContent' , false , content );
      });

      // ----------------------------------------------------------------------------------------------------

      ed.addCommand( 'insert_mobile_shortcode' , function()
      {

        content = '[mobile class="mobile" link=1 icon=0 p=0]';

        tinymce.execCommand( 'mceInsertContent' , false , content );
      });

      // ----------------------------------------------------------------------------------------------------

      ed.addCommand( 'insert_opening_hours_shortcode' , function()
      {

        content = '<p>[opening_hours class="opening-hours" divider=":"]</p>';

        tinymce.execCommand( 'mceInsertContent' , false , content );
      });

      // ----------------------------------------------------------------------------------------------------

      ed.addCommand( 'insert_phone_shortcode' , function()
      {

        content = '[phone class="phone" link=1 icon=0 p=0 url=0]';

        tinymce.execCommand( 'mceInsertContent' , false , content );
      });

      // ----------------------------------------------------------------------------------------------------

      ed.addCommand( 'insert_social_list_shortcode' , function()
      {

        content = '<p>[social_list class=""]</p>';

        tinymce.execCommand( 'mceInsertContent' , false , content );
      });

      // ----------------------------------------------------------------------------------------------------

      ed.addCommand( 'insert_staff_shortcode' , function()
      {

        content = '<p>[staff class="" featured=0 not_featured=0]</p>';

        tinymce.execCommand( 'mceInsertContent' , false , content );
      });

      // ----------------------------------------------------------------------------------------------------

      ed.addCommand( 'insert_tabs_shortcode' , function()
      {
        selected = tinyMCE.activeEditor.selection.getContent();
        if( selected )
        {
          content = '<p>[tabs titles="Title,Title 2" class="" width=""]</p><p>[tab title="Title" class=""]</p><p>'+selected+'</p><p>[/tab]</p><p>[tab title="Title 2"]</p><p>Content</p><p>[/tab]</p><p>[/tabs]</p>';
        }
        else
        {
          content = '<p>[tabs titles="Title,Title 2" class="" width=""]</p><p>[tab title="Title" class=""]</p><p>Content</p><p>[/tab]</p><p>[tab title="Title 2"]</p><p>Content 2</p><p>[/tab]</p><p>[/tabs]</p>';
        }

        tinymce.execCommand( 'mceInsertContent' , false , content );
      });

      // ----------------------------------------------------------------------------------------------------

      ed.addCommand( 'insert_testimonials_shortcode' , function()
      {

        content = '<p>[testimonials class="" slider=0 featured=0 include=""]</p>';

        tinymce.execCommand( 'mceInsertContent' , false , content );
      });

      // ----------------------------------------------------------------------------------------------------

      ed.addCommand( 'insert_vimeo_shortcode' , function()
      {
        selected = tinyMCE.activeEditor.selection.getContent();
        if( selected )
        {
          content = '<p>[vimeo id="'+selected+'" param="badge=0" width=""]</p>';
        }
        else
        {
          content = '<p>[vimeo id="XXXXXXXX" param="badge=0" width=""]</p>';
        }

        tinymce.execCommand( 'mceInsertContent' , false , content );
      });

      // ----------------------------------------------------------------------------------------------------

      ed.addCommand( 'insert_youtube_shortcode' , function()
      {
        selected = tinyMCE.activeEditor.selection.getContent();
        if( selected )
        {
          content = '<p>[youtube id="'+selected+'" param="rel=0" width=""]</p>';
        }
        else
        {
          content = '<p>[youtube id="XXXXXXXX" param="rel=0" width=""]</p>';
        }

        tinymce.execCommand( 'mceInsertContent' , false , content );
      });

      // ----------------------------------------------------------------------------------------------------
      // Register buttons

      ed.addButton( 'tinymce_address_button'    , { title:'Insert address'      ,  cmd:'insert_address_shortcode'     , icon: 'fa fa fa-map-signs' } );
      ed.addButton( 'tinymce_btn_button'    , { title:'Insert button'      ,  cmd:'insert_btn_shortcode'     , icon: 'fa fa fa-hand-pointer-o' } );
      ed.addButton( 'tinymce_card_button'    , { title:'Insert card'      ,  cmd:'insert_card_shortcode'     , icon: 'fa fa fa-window-maximize' } );
      ed.addButton( 'tinymce_cols_button'    , { title:'Insert columns'      ,  cmd:'insert_cols_shortcode'     , icon: 'fa fa fa-columns' } );
      ed.addButton( 'tinymce_email_button'    , { title:'Insert email'      ,  cmd:'insert_email_shortcode'     , icon: 'fa fa fa-envelope' } );
      ed.addButton( 'tinymce_faqs_button'    , { title:'Insert faqs'      ,  cmd:'insert_faqs_shortcode'     , icon: 'fa fa fa-info-circle' } );
      ed.addButton( 'tinymce_fax_button'    , { title:'Insert fax'      ,  cmd:'insert_fax_shortcode'     , icon: 'fa fa fa-fax' } );
      ed.addButton( 'tinymce_icon_button'    , { title:'Insert icon'      ,  cmd:'insert_icon_shortcode'     , icon: 'fa fa fa-font-awesome' } );
      ed.addButton( 'tinymce_lead_button'    , { title:'Insert lead'      ,  cmd:'insert_lead_shortcode'     , icon: 'fa fa fa-text-height' } );
      ed.addButton( 'tinymce_map_button'    , { title:'Insert map'      ,  cmd:'insert_map_shortcode'     , icon: 'fa fa fa-map' } );
      ed.addButton( 'tinymce_mobile_button'    , { title:'Insert mobile'      ,  cmd:'insert_mobile_shortcode'     , icon: 'fa fa fa-mobile' } );
      ed.addButton( 'tinymce_opening_hours_button'    , { title:'Insert opening hours'      ,  cmd:'insert_opening_hours_shortcode'     , icon: 'fa fa fa-clock-o' } );
      ed.addButton( 'tinymce_phone_button'    , { title:'Insert phone'      ,  cmd:'insert_phone_shortcode'     , icon: 'fa fa fa-phone' } );
      ed.addButton( 'tinymce_social_list_button'    , { title:'Insert social list'      ,  cmd:'insert_social_list_shortcode'     , icon: 'fa fa fa-ellipsis-h' } );
      ed.addButton( 'tinymce_staff_button'    , { title:'Insert staff'      ,  cmd:'insert_staff_shortcode'     , icon: 'fa fa fa-users' } );
      ed.addButton( 'tinymce_tabs_button'    , { title:'Insert tabs'      ,  cmd:'insert_tabs_shortcode'     , icon: 'fa fa fa-list-alt' } );
      ed.addButton( 'tinymce_testimonials_button'    , { title:'Insert testimonials'      ,  cmd:'insert_testimonials_shortcode'     , icon: 'fa fa fa-comments' } );
      ed.addButton( 'tinymce_vimeo_button'    , { title:'Insert vimeo video'      ,  cmd:'insert_vimeo_shortcode'     , icon: 'fa fa fa-vimeo' } );
      ed.addButton( 'tinymce_youtube_button'    , { title:'Insert youtube video'      ,  cmd:'insert_youtube_shortcode'     , icon: 'fa fa fa-youtube' } );
    },
  });

  // Register plugin

  tinymce.PluginManager.add( 'tinymce_extra_buttons' , tinymce.plugins.tinymce_extra_buttons );

});