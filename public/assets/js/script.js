/*

  • Page loader
  • Header is active
  • Menu
  • Menu collapse
  • Tabs
  • Map
  • Smooth Scroll
  • Parallax

*/

(function($) {

  /* PAGE LOADER */
  $('body').addClass('loading');
  $(document).ready(function() {
    $('body').addClass('loaded');
  });
  $(window).load(function() {
    $('body').addClass('loading-end');
  });

  /* HEADER IS ACTIVE */
  $(document).ready(function($){
    $.fn.scrollBottom = function() {
      return $(document).height() - this.scrollTop() - this.height();
    };
    var offset = $('#top_bar').height();

    $(window).scroll(function(){
      if ($(this).scrollTop() > offset) {
        $('body').addClass('header-is-active');
      } else {
        $('body').removeClass('header-is-active');
      }
    });
  });

  /* MENU */
  $(document).ready(function() {
    $('.menu-toggle').click(function(event) {
      if($('#header').hasClass('menu-is-open')) {
        $('#header').removeClass('menu-is-open');
      } else {
        $('#header').addClass('menu-is-open');
      }
    });
    $('.site-nav-overlay').click(function(event) {
      if($('#header').hasClass('menu-is-open')) {
        $('#header').removeClass('menu-is-open');
      }
    });
  });

  /* MENU COLLAPSE */
  $(document).ready(function() {
    $(".page_item_has_children, .menu-item-has-children").prepend('<div class="dropdown-toggle"></div>');
    $(".page_item_has_children .dropdown-toggle, .menu-item-has-children .dropdown-toggle").click(function (e) {
      $widget = $(this);
      $widgetHeader = $widget.next();
      $content = $widgetHeader.next();
      $widget.toggleClass('is-open');
      $content.slideToggle(300, function () {});
      e.preventDefault();
    });
    $('.site-nav-overlay').click(function(event) {
      $widget = $(".page_item_has_children .dropdown-toggle, .menu-item-has-children .dropdown-toggle");
      $widgetHeader = $widget.next();
      $content = $widgetHeader.next();
      $widget.removeClass('is-open');
      $content.hide(300, function () {});
    });
  });

  /* TABS */
  $(document).ready(function() {
    $("ul.tabs").each(function(){
      var $active, $content, $links = $(this).find('a');
      $active = $($links.filter('[href="'+location.hash+'"]')[0] || $links[0]);
      $active.addClass("active");
      $content = $($active[0].hash);
      $links.not($active).each(function () {
        $(this.hash).css({
          'height': '0'
        });
      });
      $(this).on('click', 'a', function(e){
        $active.removeClass("active");
        $content.css({
          'height': '0',
        });
        $active = $(this);
        $content = $(this.hash);
        $active.addClass("active");
        $content.css({
          'height': 'auto',
        });
        e.preventDefault();
      });
    });
  });

  $video=$('.panels .panel');
  $video_selector=$('.tabs.form-control');
  jQuery(document).ready(function($){
    $video_selector.change(function(){
      $video.removeClass("active").css('height', '0');
      // $video.hide();
      $('#'+ $(this).val()).addClass('active').css('height', 'auto');
    });
  });

  /* MAP */
  $(document).ready(function() {
    $(".map iframe").css("pointer-events", "none");
    $(".map").click(function () {
      $(".map iframe").css("pointer-events", "auto");
    });
    $( ".map" ).mouseleave(function() {
      $(".map iframe").css("pointer-events", "none");
    });
  });

  /* SMOOTH SCROLL */
  $(document).ready(function() {
    var offset = $('#header').height();
    $('a.smooth-scroll[href*=#]:not([href=#]), .smooth-scroll a[href*=#]:not([href=#])').click(function() {
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        if (target.length) {
          $('html,body').animate({
            scrollTop: target.offset().top -offset
          }, 300);
          return false;
        }
      }
    });
  });

  /* FAQS */
  $(".faqs-wrapper.not-accordion .faq .faq-title").click(function (e) {
    $faqtitle = $(this);
    $faq = $faqtitle.parent();
    $faqcontent = $faqtitle.next();
    $faq.toggleClass("is-expanded");
    $faqcontent.slideToggle(300, function () {});
    e.preventDefault();
  });
  $(".faqs-wrapper.is-accordion .faq .faq-title").click(function (e) {
    $faqtitle = $(this);
    $faq = $faqtitle.parent();
    $faqcontent = $faqtitle.next();
    $faqstitle = $('.faqs-wrapper.is-accordion .faq .faq-title');
    $faqs = $faqstitle.parent();
    $faqscontent = $faqstitle.next();
    $faqs.removeClass('still-expanded');
    if($faq.hasClass('is-expanded')){
      $faq.addClass('still-expanded');
    }
    $faqs.removeClass('is-expanded');
    $faq.addClass('is-expanded');
    if(!$faq.hasClass('still-expanded')){
      $faqscontent.slideUp(300, function () {});
      $faqcontent.slideDown(300, function () {});
    } else {
      $faq.removeClass("is-expanded");
      $faq.removeClass("still-expanded");
      $faqcontent.slideUp(300, function () {});
    }
    e.preventDefault();
  });

  /* PARALLAX */
  function parallax() {
    var ev = {
        scrollTop: document.body.scrollTop || document.documentElement.scrollTop
    };
    ev.ratioScrolled = ev.scrollTop / (document.body.scrollHeight - document.documentElement.clientHeight);
    render(ev);
  }
  function render(ev) {
    var t = ev.scrollTop;
    var y = 1 +(Math.round(t * 0.25));
    if (y < -300) y = -300;
    $('.parallax').css('background-position', 'center calc(50% + ' + y + 'px)');
  }
  $(window).on('scroll', function(){
    parallax();
  });


  $(window).bind("load resize",function(e){
    $('.home-hero .hero-content').css('height', 'auto');
    var contentHeight = 0;
    $(".home-hero .hero-content").each(function(){
      if ($(this).height() > contentHeight) { contentHeight = $(this).height(); }
    });
    $('.home-hero .hero-content').css('height', contentHeight+'px');
  });

  /* YOUTUBE VIDEO */
  ( function() {
    var youtube = document.querySelectorAll( ".youtube-video" );
    for (var i = 0; i < youtube.length; i++) {
      var source = "https://img.youtube.com/vi/"+ youtube[i].dataset.embed +"/0.jpg";
      var image = new Image();
      image.src = source;
      image.addEventListener( "load", function() {
        youtube[ i ].appendChild( image );
      }( i ) );
      youtube[i].addEventListener( "click", function() {
        var iframe = document.createElement( "iframe" );
        iframe.setAttribute( "frameborder", "0" );
        iframe.setAttribute( "allowfullscreen", "" );
        iframe.setAttribute( "src", "https://www.youtube.com/embed/"+ this.dataset.embed +"?"+ this.dataset.param );
        this.innerHTML = "";
        this.appendChild( iframe );
      } );
    };
  } )();

  /* OFFERS */
  $(document).ready(function() {
    $(".latest-offer-close").click(function(event) {
      $(".latest-offer-overlay").fadeOut();
    });
  });
  $(document).keyup(function(event) {
    if (event.keyCode === 27) $('.latest-offer-close').click();
  });

  /* LEAVING ALREADY? */
  $(document).ready(function() {
    $('body').mouseleave(function() {
      $('body').addClass('leaving-already');
    });
  });

})( jQuery );
