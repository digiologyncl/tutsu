<?php

// Customise the footer in admin area
function tutsu_footer_admin () {
	if( TUTSU_PLUGIN_WHICH_COMPANY == 'personal' ) {
	  echo 'Theme designed and developed by <a href="' . esc_url( __( 'http://tomungerer.com/', 'tutsu' ) ) . '" target="_blank">Tom Ungerer</a>.';
	}
	elseif( TUTSU_PLUGIN_WHICH_COMPANY == 'appymonkey' ) {
	  echo 'Theme designed and developed by <a href="' . esc_url( __( 'http://www.appymonkey.com/', 'tutsu' ) ) . '" target="_blank">Appy Monkey</a>.';
	}
	elseif( TUTSU_PLUGIN_WHICH_COMPANY == 'blueshark' ) {
	  echo 'Theme designed and developed by <a href="' . esc_url( __( 'http://blue-shark.co.uk/', 'tutsu' ) ) . '" target="_blank">Blue Shark</a>.';
	}
	elseif( TUTSU_PLUGIN_WHICH_COMPANY == 'digiology' ) {
	  echo 'Theme designed and developed by <a href="' . esc_url( __( 'http://digiology.tv/', 'tutsu' ) ) . '" target="_blank">Digiology</a>.';
	}
	elseif( TUTSU_PLUGIN_WHICH_COMPANY == 'dentistid' ) {
	  echo 'Theme designed and developed by <a href="' . esc_url( __( 'http://dentistidentity.net/', 'tutsu' ) ) . '" target="_blank">Dentist Identity</a>.';
	}
	elseif( TUTSU_PLUGIN_WHICH_COMPANY == 'itc' ) {
	  echo 'Theme designed and developed by <a href="' . esc_url( __( 'https://www.itcservice.co.uk/', 'tutsu' ) ) . '" target="_blank">ITC Service</a>.';
	}
	elseif( TUTSU_PLUGIN_WHICH_COMPANY == '8' ) {
	  echo 'Site web réalisé par <a href="' . esc_url( __( 'https://www.facebook.com/collectifenhuitlettres/', 'tutsu' ) ) . '" target="_blank">Collectif en huit lettres</a>.';
	}
	elseif( TUTSU_PLUGIN_WHICH_COMPANY == 'hpd' ) {
	  echo 'Theme designed and developed by <a href="' . esc_url( __( 'https://www.healthpracticedigital.com.au/', 'tutsu' ) ) . '" target="_blank">Health Practice Digital</a>.';
	}
	elseif( TUTSU_PLUGIN_WHICH_COMPANY == 'thinkmediahub' ) {
	  echo 'Theme designed and developed by <a href="' . esc_url( __( '#', 'tutsu' ) ) . '" target="_blank">Think Media Hub</a>.';
	}
	elseif( TUTSU_PLUGIN_WHICH_COMPANY == 'robsonbrown' ) {
	  echo 'Theme designed and developed by <a href="' . esc_url( __( 'http://robson-brown.co.uk/', 'tutsu' ) ) . '" target="_blank">Robson Brown</a>.';
	}
	elseif( TUTSU_PLUGIN_WHICH_COMPANY == 'creativedental' ) {
	  echo 'Theme designed and developed by <a href="' . esc_url( __( 'http://creativedentalagency.com/', 'tutsu' ) ) . '" target="_blank">Creative Dental</a>.';
	}
}
add_filter('admin_footer_text', 'tutsu_footer_admin');


// Custom CSS for the login page
function tutsu_login_css() {
	if( TUTSU_PLUGIN_WHICH_COMPANY == 'personal' ) {
		echo '<link rel="stylesheet" type="text/css" href="' . tutsu_plugin_url( 'public/partials/agencies/personal/css/wp-login.css' ) . '"/>';
	}
	elseif( TUTSU_PLUGIN_WHICH_COMPANY == 'appymonkey' ) {
		echo '<link rel="stylesheet" type="text/css" href="' . tutsu_plugin_url( 'public/partials/agencies/appymonkey/css/wp-login.css' ) . '"/>';
	}
	elseif( TUTSU_PLUGIN_WHICH_COMPANY == 'blueshark' ) {
		echo '<link rel="stylesheet" type="text/css" href="' . tutsu_plugin_url( 'public/partials/agencies/blueshark/css/wp-login.css' ) . '"/>';
	}
	elseif( TUTSU_PLUGIN_WHICH_COMPANY == 'digiology' ) {
		echo '<link rel="stylesheet" type="text/css" href="' . tutsu_plugin_url( 'public/partials/agencies/digiology/css/wp-login.css' ) . '"/>';
	}
	elseif( TUTSU_PLUGIN_WHICH_COMPANY == 'dentistid' ) {
		echo '<link rel="stylesheet" type="text/css" href="' . tutsu_plugin_url( 'public/partials/agencies/dentistid/css/wp-login.css' ) . '"/>';
	}
	elseif( TUTSU_PLUGIN_WHICH_COMPANY == 'itc' ) {
		echo '<link rel="stylesheet" type="text/css" href="' . tutsu_plugin_url( 'public/partials/agencies/itc/css/wp-login.css' ) . '"/>';
	}
	elseif( TUTSU_PLUGIN_WHICH_COMPANY == '8' ) {
		echo '<link rel="stylesheet" type="text/css" href="' . tutsu_plugin_url( 'public/partials/agencies/8/css/wp-login.css' ) . '"/>';
	}
	elseif( TUTSU_PLUGIN_WHICH_COMPANY == 'hpd' ) {
		echo '<link rel="stylesheet" type="text/css" href="' . tutsu_plugin_url( 'public/partials/agencies/hpd/css/wp-login.css' ) . '"/>';
	}
	elseif( TUTSU_PLUGIN_WHICH_COMPANY == 'thinkmediahub' ) {
		echo '<link rel="stylesheet" type="text/css" href="' . tutsu_plugin_url( 'public/partials/agencies/thinkmediahub/css/wp-login.css' ) . '"/>';
	}
	elseif( TUTSU_PLUGIN_WHICH_COMPANY == 'robsonbrown' ) {
		echo '<link rel="stylesheet" type="text/css" href="' . tutsu_plugin_url( 'public/partials/agencies/robsonbrown/css/wp-login.css' ) . '"/>';
	}
	elseif( TUTSU_PLUGIN_WHICH_COMPANY == 'creativedental' ) {
		echo '<link rel="stylesheet" type="text/css" href="' . tutsu_plugin_url( 'public/partials/agencies/creativedental/css/wp-login.css' ) . '"/>';
	}
}
add_action('login_head', 'tutsu_login_css');


// Custom copyrights at bottom of page
function tutsu_copyrights() {
	$output = '';
	if( TUTSU_PLUGIN_WHICH_COMPANY == 'personal' ) {
		$output = '';
		$output .= '<div class="site-copyright" id="copyright">';
		$output .= '<div class="container site-copyright-inner">';
		$output .= '<div class="logo-container">';
		$output .= '<a href="http://www.tomungerer.com/" target="_blank">';
		$output .= '<div class="logo">';
		$output .= '</div>';
		$output .= '</a>';
		$output .= '</div>          ';
		$output .= '<small>Copyright &copy; ' . date('Y') . ' ' . get_bloginfo('name') . '. All rights reserved.<br>';
		$output .= 'Website by <a href="http://www.tomungerer.com/" target="_blank">Tom Ungerer</a>';
		$output .= '</small>';
		$output .= '</div>';
		$output .= '</div>';
	}
	elseif( TUTSU_PLUGIN_WHICH_COMPANY == 'appymonkey' ) {
		$output = '';
		$output .= '<div class="site-copyright" id="copyright">';
		$output .= '<div class="container site-copyright-inner">';
		$output .= '<div class="logo-container">';
		$output .= '<a href="http://www.appymonkey.com/" target="_blank">';
		$output .= '<div class="logo">';
		$output .= '</div>';
		$output .= '</a>';
		$output .= '</div>          ';
		$output .= '<small>Copyright &copy; ' . date('Y') . ' ' . get_bloginfo('name') . '. All rights reserved.<br>';
		$output .= 'Website by <a href="http://www.appymonkey.com/" target="_blank">Appy Monkey</a>';
		$output .= '</small>';
		$output .= '</div>';
		$output .= '</div>';
	}
	elseif( TUTSU_PLUGIN_WHICH_COMPANY == 'digiology' ) {
		$output = '';
		$output .= '<div class="site-copyright" id="copyright">';
		$output .= '<div class="container site-copyright-inner">';
		$output .= '<div class="logo-container">';
		$output .= '<a href="http://www.digiology.tv/" target="_blank">';
		$output .= '<div class="logo">';
		$output .= '</div>';
		$output .= '</a>';
		$output .= '</div>          ';
		$output .= '<small>Copyright &copy; ' . date('Y') . ' ' . get_bloginfo('name') . '. All rights reserved.<br>';
		$output .= 'Website by <a href="http://www.digiology.tv/" target="_blank">Digiology</a>';
		$output .= '</small>';
		$output .= '</div>';
		$output .= '</div>';
	}
	elseif( TUTSU_PLUGIN_WHICH_COMPANY == 'dentistid' ) {
		$output = '';
		$output .= '<div class="site-copyright" id="copyright">';
		$output .= '<div class="container site-copyright-inner">';
		$output .= '<div class="logo-container">';
		$output .= '<a href="http://www.dentistidentity.net/" target="_blank">';
		$output .= '<div class="logo">';
		$output .= '</div>';
		$output .= '</a>';
		$output .= '</div>          ';
		$output .= '<small>Copyright &copy; ' . date('Y') . ' ' . get_bloginfo('name') . '. All rights reserved.<br>';
		$output .= 'Website by <a href="http://www.dentistidentity.net/" target="_blank">Dentist Identity</a>';
		$output .= '</small>';
		$output .= '</div>';
		$output .= '</div>';
	}
	elseif( TUTSU_PLUGIN_WHICH_COMPANY == 'itc' ) {
		$output = '';
		$output .= '<div class="site-copyright" id="copyright">';
		$output .= '<div class="container site-copyright-inner">';
		$output .= '<div class="logo-container">';
		$output .= '<a href="https://www.itcservice.co.uk/" target="_blank">';
		$output .= '<div class="logo">';
		$output .= '</div>';
		$output .= '</a>';
		$output .= '</div>          ';
		$output .= '<small>Copyright &copy; ' . date('Y') . ' ' . get_bloginfo('name') . '. All rights reserved.<br>';
		$output .= 'Website by <a href="https://www.itcservice.co.uk/" target="_blank">ITC Service</a>';
		$output .= '</small>';
		$output .= '</div>';
		$output .= '</div>';
	}
	elseif( TUTSU_PLUGIN_WHICH_COMPANY == '8' ) {
		$output = '';
		$output .= '<div class="site-copyright" id="copyright">';
		$output .= '<div class="container site-copyright-inner">';
		$output .= '<div class="logo-container">';
		$output .= '<a href="https://www.facebook.com/collectifenhuitlettres/" target="_blank">';
		$output .= '<div class="logo">';
		$output .= '</div>';
		$output .= '</a>';
		$output .= '</div>          ';
		$output .= '<small>Copyright &copy; ' . date('Y') . ' ' . get_bloginfo('name') . '. Tous droits réservés.<br>';
		$output .= 'Réalisation web par <a href="https://www.facebook.com/collectifenhuitlettres/" target="_blank">Collectif en huit lettres</a>';
		$output .= '</small>';
		$output .= '</div>';
		$output .= '</div>';
	}
	elseif( TUTSU_PLUGIN_WHICH_COMPANY == 'hpd' ) {
		$output = '';
		$output .= '<div class="site-copyright" id="copyright">';
		$output .= '<div class="container site-copyright-inner">';
		$output .= '<div class="logo-container">';
		$output .= '<a href="https://www.healthpracticedigital.com.au/" target="_blank">';
		$output .= '<div class="logo">';
		$output .= '</div>';
		$output .= '</a>';
		$output .= '</div>          ';
		$output .= '<small>Copyright &copy; ' . date('Y') . ' ' . get_bloginfo('name') . '. All rights reserved.<br>';
		$output .= 'Website by <a href="https://www.healthpracticedigital.com.au/" target="_blank">Health Practice Digital</a>';
		$output .= '</small>';
		$output .= '</div>';
		$output .= '</div>';
	}
	elseif( TUTSU_PLUGIN_WHICH_COMPANY == 'thinkmediahub' ) {
		$output = '';
		$output .= '<div class="site-copyright" id="copyright">';
		$output .= '<div class="container site-copyright-inner">';
		$output .= '<div class="logo-container">';
		$output .= '<a href="#" target="_blank">';
		$output .= '<div class="logo">';
		$output .= '</div>';
		$output .= '</a>';
		$output .= '</div>          ';
		$output .= '<small>Copyright &copy; ' . date('Y') . ' ' . get_bloginfo('name') . '. All rights reserved.<br>';
		$output .= 'Website by <a href="#" target="_blank">Think Media Hub</a>';
		$output .= '</small>';
		$output .= '</div>';
		$output .= '</div>';
	}
	elseif( TUTSU_PLUGIN_WHICH_COMPANY == 'robsonbrown' ) {
		$output = '';
		$output .= '<div class="site-copyright" id="copyright">';
		$output .= '<div class="container site-copyright-inner">';
		$output .= '<div class="logo-container">';
		$output .= '<a href="http://robson-brown.co.uk/" target="_blank">';
		$output .= '<div class="logo">';
		$output .= '</div>';
		$output .= '</a>';
		$output .= '</div>          ';
		$output .= '<small>Copyright &copy; ' . date('Y') . ' ' . get_bloginfo('name') . '. All rights reserved.<br>';
		$output .= 'Website by <a href="http://robson-brown.co.uk/" target="_blank">Robson Brown</a>';
		$output .= '</small>';
		$output .= '</div>';
		$output .= '</div>';
	}
	elseif( TUTSU_PLUGIN_WHICH_COMPANY == 'creativedental' ) {
		$output = '';
		$output .= '<div class="site-copyright" id="copyright">';
		$output .= '<div class="container site-copyright-inner">';
		$output .= '<div class="logo-container">';
		$output .= '<a href="http://creativedentalagency.com/" target="_blank">';
		$output .= '<div class="logo">';
		$output .= '</div>';
		$output .= '</a>';
		$output .= '</div>          ';
		$output .= '<small>Copyright &copy; ' . date('Y') . ' ' . get_bloginfo('name') . '. All rights reserved.<br>';
		$output .= 'Website by <a href="http://creativedentalagency.com/" target="_blank">Creative Dental</a>';
		$output .= '</small>';
		$output .= '</div>';
		$output .= '</div>';
	}
	echo $output;
}
add_action('wp_footer', 'tutsu_copyrights');

// Custom style for company
function tutsu_company_style() {
	if( TUTSU_PLUGIN_WHICH_COMPANY == 'personal' ) {
		echo '<link rel="stylesheet" type="text/css" href="' . tutsu_plugin_url( 'public/partials/agencies/personal/css/style.css' ) . '"/>';
	}
	elseif( TUTSU_PLUGIN_WHICH_COMPANY == 'appymonkey' ) {
		echo '<link rel="stylesheet" type="text/css" href="' . tutsu_plugin_url( 'public/partials/agencies/appymonkey/css/style.css' ) . '"/>';
	}
	elseif( TUTSU_PLUGIN_WHICH_COMPANY == 'blueshark' ) {
		echo '<link rel="stylesheet" type="text/css" href="' . tutsu_plugin_url( 'public/partials/agencies/blueshark/css/style.css' ) . '"/>';
	}
	elseif( TUTSU_PLUGIN_WHICH_COMPANY == 'digiology' ) {
		echo '<link rel="stylesheet" type="text/css" href="' . tutsu_plugin_url( 'public/partials/agencies/digiology/css/style.css' ) . '"/>';
	}
	elseif( TUTSU_PLUGIN_WHICH_COMPANY == 'dentistid' ) {
		echo '<link rel="stylesheet" type="text/css" href="' . tutsu_plugin_url( 'public/partials/agencies/dentistid/css/style.css' ) . '"/>';
	}
	elseif( TUTSU_PLUGIN_WHICH_COMPANY == 'itc' ) {
		echo '<link rel="stylesheet" type="text/css" href="' . tutsu_plugin_url( 'public/partials/agencies/itc/css/style.css' ) . '"/>';
	}
	elseif( TUTSU_PLUGIN_WHICH_COMPANY == '8' ) {
		echo '<link rel="stylesheet" type="text/css" href="' . tutsu_plugin_url( 'public/partials/agencies/8/css/style.css' ) . '"/>';
	}
	elseif( TUTSU_PLUGIN_WHICH_COMPANY == 'hpd' ) {
		echo '<link rel="stylesheet" type="text/css" href="' . tutsu_plugin_url( 'public/partials/agencies/hpd/css/style.css' ) . '"/>';
	}
	elseif( TUTSU_PLUGIN_WHICH_COMPANY == 'thinkmediahub' ) {
		echo '<link rel="stylesheet" type="text/css" href="' . tutsu_plugin_url( 'public/partials/agencies/thinkmediahub/css/style.css' ) . '"/>';
	}
	elseif( TUTSU_PLUGIN_WHICH_COMPANY == 'robsonbrown' ) {
		echo '<link rel="stylesheet" type="text/css" href="' . tutsu_plugin_url( 'public/partials/agencies/robsonbrown/css/style.css' ) . '"/>';
	}
	elseif( TUTSU_PLUGIN_WHICH_COMPANY == 'creativedental' ) {
		echo '<link rel="stylesheet" type="text/css" href="' . tutsu_plugin_url( 'public/partials/agencies/creativedental/css/style.css' ) . '"/>';
	}
}
add_action('wp_head', 'tutsu_company_style');

// Remove dashboard widgets
function remove_dashboard_meta() {
  remove_meta_box( 'dashboard_welcome_panel', 'dashboard', 'normal' );
  remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
  remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
  remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
  remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
  remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
  remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
  remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
  remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
  remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');
}
add_action( 'admin_init', 'remove_dashboard_meta' );

// Remove weclome panel
remove_action( 'welcome_panel', 'wp_welcome_panel' );
