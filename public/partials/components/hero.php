<?php

function show_hero( $atts = '' ) {

  if(is_page() || is_single()){
    $subtitle = get_field('subtitle', get_the_ID());
  } elseif(is_home()) {
    $subtitle = get_field('subtitle', get_option( 'page_for_posts' ));
  } else {
    $subtitle = '';
  }

  $icon = get_field('featured_icon', get_the_ID());

  $defaults = array(
    'spacer' => 0,
    'title' => 1,
    'subtitle' => 1,
    'icon' => 1,
    'feat_on_single' => 0,
    'feat_on_pages' => 1,
    'title_on_single' => 0
  );

  $atts = wp_parse_args( $atts, $defaults );

  $spacer = $atts['spacer'];
  $title = $atts['title'];
  $show_subtitle = $atts['subtitle'];
  $show_icon = $atts['icon'];
  $feat_on_single = $atts['feat_on_single'];
  $feat_on_pages = $atts['feat_on_pages'];
  $title_on_single = $atts['title_on_single'];

  $output = '';

  $feat = 0;

  if(!is_front_page()) :


    if(is_home() || is_search() || is_archive()) {

      if(get_the_post_thumbnail_url(get_option( 'page_for_posts' ))){
        $feat = get_the_post_thumbnail_url(get_option( 'page_for_posts' ), 'hero');
      } else {
        $feat = 0;
      }

    } elseif( is_single() ) {

      if(get_the_post_thumbnail(get_the_ID()) && $feat_on_single){

        $feat = get_the_post_thumbnail_url(get_the_ID(), 'hero');

      } else {

        if(get_the_post_thumbnail_url(get_option( 'page_for_posts' ))){

          $feat = get_the_post_thumbnail_url(get_option( 'page_for_posts' ), 'hero');

        } else {

          $feat = 0;

        }

      }

    } elseif( is_page() ) {

      if(get_the_post_thumbnail(get_the_ID()) && $feat_on_pages){
        $feat = get_the_post_thumbnail_url(get_the_ID(), 'hero');
      } else {
        if(get_the_post_thumbnail_url(get_option( 'page_for_posts' ))){
          $feat = get_the_post_thumbnail_url(get_option( 'page_for_posts' ), 'hero');
        } else {
          $feat = 0;
        }
      }

    } else {

      $feat = get_the_post_thumbnail_url(get_the_ID(), 'hero');

    }


    $output .= '<div class="hero parallax';
    if($feat) {
      $output .= ' has-featured-image';
    }
    $output .= '"';
    if($feat) {
      $output .=' style="background-image:url(' . $feat . ')"';
    }
    $output .= '>';
    $output .= '<div class="hero-overlay">';
    if($spacer) {
      $output .= '<div class="site-header-spacer"></div>';
    }
    $output .= '<div class="hero-inner">';
    $output .= '<div class="hero-content">';
    $output .= '<div class="container">';
    $output .= '<div class="hero-content-inner">';
    if($title) {
      if(is_home()) {
        $output .= '<h1 class="hero-title">' . get_the_title(get_option( 'page_for_posts' )) . '</h1>';
      } elseif(is_archive()) {
        $output .= '<h1 class="hero-title">' . str_replace("Archives: ", "", get_the_archive_title()) . '</h1>';
      } elseif(is_404()) {
        $output .= '<h1 class="hero-title">Error 404</h1>';
      } elseif(is_search()) {
        $output .= '<h1 class="hero-title">';
        $output .= 'Search Results for: <span>' . get_search_query() . '</span>';
        $output .= '</h1>';
      } elseif(is_singular('post') && !$title_on_single) {
        $output .= '<h2 class="hero-title">' . get_the_title(get_option( 'page_for_posts' )) . '</h2>';
      } else {
        $output .= '<h1 class="hero-title">' . get_the_title() . '</h1>';
      }
    }
    if($subtitle && $show_subtitle){
      $output .= '<div class="hero-subtitle">' . $subtitle . '</div>';
    }
    if($icon && $show_icon){
      $output .= '<div class="hero-icon"><img src="' . $icon[url] . '" alt="' . $icon[alt] . '"></div>';
    }
    $output .= '</div>';
    $output .= '</div>';
    $output .= '</div>';
    $output .= '</div>';
    $output .= '</div>';
    $output .= '</div>';

  endif;

  if(!is_page('landing-page')){
    echo $output;
  }

}


