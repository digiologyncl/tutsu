<?php

function show_sections() {
  $current_id = get_the_ID();
  $sections = get_field('sections', $current_id);

  $output = '';
  if($sections){
    if(is_page() || is_home() || is_single()){
      $i = 0;
      foreach ($sections as $section) {
        $active = $section['active'];
        $class = $section['class'];
        $id = $section['id'];
        $content = $section['content'];
        $image = $section['image'];
        $image_url = $image['url'];
        $container = $section['container'];

        $i++;

        if($active){
          $output .= '

<section class="section-' . $i;
          if($class){
            $output .= ' ' . $class;
          }
          $output .= '"';
          if($id){
            $output .= ' id="' . $id . '"';
          }
          $output .= '>';
          if($image){
            $output .= '
  <div class="section-image" style="background-image:url(' . $image_url . ')"></div>';
          }
          $output .= '
  <div class="container';
          $output .= $container;
          $output .= '">
    <div class="section-inner">
      ';
          $output .=
      apply_filters('the_content', $content);
          $output .= '
    </div>
  </div>';
          $output .= '
</section>';
        }
      }
    }
  }

  echo $output;

}

add_action( 'get_footer', 'show_sections', 10 );