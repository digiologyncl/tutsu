<?php

function show_hello_bar() {
  $current_id = get_the_ID();
  $content = get_field('hello_bar_content', 'options');
  $class = get_field('hello_bar_class', 'options');
  $active = get_field('hello_bar_active', 'options');
  $all = get_field('hello_bar_all', 'options');
  $includes = get_field('hello_bar_include', 'options');
  $exceptions = get_field('hello_bar_exceptions', 'options');

  $output = '';
  if($content){
    if(is_page() || is_home()){

      $is_current_page = array();
      if($all && $exceptions){
        foreach ($exceptions as $exception){
          array_push($is_current_page, $exception);
        }
      } elseif(!$all && $includes){
        foreach ($includes as $include){
          array_push($is_current_page, $include);
        }
      } else {
        $exceptions = array();
      }

      $activated_output = '';
      $activated_output .= '

  <div class="hello-bar';
      if($class){
        $activated_output .= ' ' . $class;
      }
      $activated_output .= '">';
    }
    $activated_output .= '
<div class="hello-bar-close"></div>
<div class="container">';
    $activated_output .=
apply_filters('the_content', $content);
    $activated_output .= '
</div>';
    $activated_output .= '
</div>';

    if(is_page() || is_home()) {
      if ($all) {
        if($active && !in_array($current_id, $is_current_page)){
          $output .= $activated_output;
        }
      } else {
        if($active && in_array($current_id, $is_current_page)){
          $output .= $activated_output;
        }
      }
    }
  }

  echo $output;

}


add_filter( 'wp_footer', 'show_hello_bar', 11 );

function show_hello_bar_styles() {
  $output = '

<style type="text/css">

.hello-bar{
  display: none;
  position: fixed;
  bottom: -100%;
  transition: bottom .5s ease-in-out;
  left: 0;
  right: 0;
  padding: .5rem 3rem;
  text-align: center;
  z-index: 999999999999999;
  box-shadow: 0 0 .5rem rgba(0,0,0,0.25);
}

.loading .hello-bar{
  display: block;
}

.loading-end .hello-bar{
  bottom: 0;
}
.hello-bar.is-closed{
  bottom: -100%;
}
.hello-bar .hello-bar-close{
  position: absolute;
  top: .75rem;
  right: 1rem;
  cursor: pointer;
}
.hello-bar .hello-bar-close:before{
  font-family: "FontAwesome";
  content: "\f00d";
  font-size: 1rem;
  line-height: 1;
}

.hello-bar .btn{
  min-width: inherit;
  margin-left: .5rem;
  margin-right: .5rem;
}

.hello-bar p{
  margin-bottom: 0;
}

</style>

';

  echo $output;
}
add_filter('wp_head', 'show_hello_bar_styles');

function show_hello_bar_scripts() {
  $output = '
<script>

(function($) {
  $(document).ready(function() {
    $(".hello-bar-close").click(function(event) {
      $(".hello-bar").addClass("is-closed");
      // $(".hello-bar").fadeOut();
    });
  });
})( jQuery );

</script>

';

  echo $output;
}
add_filter('wp_head', 'show_hello_bar_scripts');