<?php

// HOME PAGE POP UP
function show_offer_on_offer( $atts = '' ) {
  $output = '';
  if(is_front_page()) {
    $offers = get_posts(array(
      'numberposts' => 1,
      'orderby' => 'post_date',
      'order' => 'DESC',
      'post_type' => 'offer',
    ));
    if($offers) {
      foreach($offers as $offer) {
        $offer_modal = get_field('offer_modal', $offer->ID);
        $activate = get_field('offer_modal_activate_on_home', $offer->ID);
        if($offer_modal && $activate) {
         $output .= '<div class="latest-offer-overlay">';
         $output .= '<div class="latest-offer">';
         $output .= '<span class="latest-offer-close"></span>';
         $output .= '<div class="latest-offer-inner">' . $offer_modal . '</div>';
         $output .= '</div>';
         $output .= '</div>';
        }
      }
    }
    echo $output;
  }
}
add_action( 'body_begin', 'show_offer_on_offer', 98 );

// LEAVING POP UP
function show_offer_on_leave( $atts = '' ) {
  $activate = get_field('offer_modal_activate_on_leave');
  $offer_modal = get_field('offer_modal');
  $output = '';
  if($offer_modal) {
    $output .= '<div class="latest-offer-overlay">';
    $output .= '<div class="latest-offer">';
    $output .= '<span class="latest-offer-close"></span>';
    $output .= '<div class="latest-offer-inner">' . $offer_modal . '</div>';
    $output .= '</div>';
    $output .= '</div>';
  }
  if($activate && is_singular('offer') && ($_SERVER['REQUEST_METHOD'] != 'POST') && !isset($_GET['key'])) {
    echo $output;
  }
}
add_action( 'body_begin', 'show_offer_on_leave', 98 );