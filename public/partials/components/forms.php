<?php

$optional_settings = get_field('admin_setup_optional_settings', 'option');

if( $optional_settings && in_array('forms', $optional_settings) ) {

// BRAND FROM EMAIL
// function tutsu_branded_email_from_email( $email ) {
//     return "no-reply@" . get_option('home');
// }
// add_filter( 'wp_mail_from', 'tutsu_branded_email_from_email' );


// BRAND FROM NAME
function tutsu_branded_email_from_name( $name ) {
  return get_option('blogname');
}
add_filter( 'wp_mail_from_name', 'tutsu_branded_email_from_name' );



$forms=glob( TUTSU_PLUGIN_DIR . '/public/partials/forms/*.php');
foreach( $forms as $form ) {
  include( $form );
}


// KEY CHECKER
function tutsu_forms_form_key_checker(){
  $output = '';
  if (isset($_GET['key'])) {
    if($_SERVER['REQUEST_METHOD'] != 'POST') {
      $output .= '<form method="POST" id="keyForm" style="visibility: hidden">';
      $output .= '<input type="text" name="key" value="' . htmlspecialchars($_GET["key"]) . '">';
      $output .= '</form>';
      $output .= '<script type="text/javascript">';
      $output .= 'document.getElementById("keyForm").submit();';
      $output .= 'var element = document.getElementById("keyForm");';
      $output .= 'element.parentNode.removeChild(element);';
      $output .= '</script>';
    }
  }
  echo $output;
}
add_action('wp_footer', 'tutsu_forms_form_key_checker', 12);

}