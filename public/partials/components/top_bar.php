<?php

function show_top_bar( $atts = '' ) {
  $top_bar = get_field('activate_top_bar', 'option');
  $site_logo = get_field('site_logo', 'option');

  $top_left_items = get_field('top_left_items', 'option');
  $top_right_items = get_field('top_right_items', 'option');

  $defaults = array(
    'echo' => '1',
  );

  $atts = wp_parse_args( $atts, $defaults );

  $echo = $atts['echo'];

  $output  = '';


  $output  .= '<div class="site-top-bar" id="top_bar">';
  if($top_left_items || $top_right_items){
    $output  .= '<div class="container">';
    $output  .= '<div class="row items-middle-xs">';
    if($top_left_items){
      $output  .= '<div class="col-xs">';
      $output  .= '<ul class="top-bar-left">';
      foreach ($top_left_items as $top_left_item) {
        $item = $top_left_item['item'];
        $output .= '<li>' . do_shortcode($item) . '</li>';
      }
      $output  .= '</ul>';
      $output  .= '</div>';
    }
    if($top_right_items){
      $output  .= '<div class="col-xs text-xs-right">';
      $output  .= '<ul class="top-bar-right">';
      foreach ($top_right_items as $top_right_item) {
        $item = $top_right_item['item'];
        $output .= '<li>' . do_shortcode($item) . '</li>';
      }
      $output  .= '</ul>';
      $output  .= '</div>';
    }
    $output  .= '</div>';
    $output  .= '</div>';
  }
  $output  .= '</div>';

  if($top_bar){
    if($echo){
      echo $output;
    } else {
      return $output;
    }
  }

}
add_action( 'body_begin', 'show_top_bar' );

function tutsu_add_css_has_top_bar( $classes ) {
  $top_bar = get_field('activate_top_bar', 'option');
  if($top_bar){
    $classes[] = 'has-top-bar';
  }
  return $classes;
}
add_filter( 'body_class', 'tutsu_add_css_has_top_bar' );
