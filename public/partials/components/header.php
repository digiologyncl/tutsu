<?php

function show_header( $atts = '' ) {
  $header = get_field('activate_header', 'option');
  $site_logo = get_field('site_logo', 'option');

  $output  = '';
  $output .= '<header class="site-header" id="header">';
  $output .= '<div class="site-header-inner">';
  $output .= '<div class="container clearfix">';

  $output .= '<div class="menu-toggle">';
  $output .= '<span class="hamburger"></span>';
  $output .= '<span class="menu-toggle-alert"></span>';
  $output .= '</div>';
  $output .= '<a href="' . esc_url( home_url() ) . '" class="site-header-logo">';
  if($site_logo) {
    $output .= '<img class="logo" src="' . $site_logo['url'] . '" alt="' . get_bloginfo('name') . '">';
  } else {
    $output .= '<svg class="logo"><use xlink:href="#dg-logo" /></svg>';
  }
  $output .= '</a>';

  $output .= '<div class="site-nav">';

  $output .= '<div class="site-nav-overlay"></div>';

  $output .= '<div class="site-nav-inner" id="site-nav-inner">';

  $output .= '<div class="site-nav-header">';
  $output .= '</div>';

  $output .= '<nav class="main-nav">';
  $args = array(
    'theme_location'  => 'primary',
    'menu_class'      => '',
    'container'       => 'ul',
    'depth'           => '2',
    'echo'            => false
  );
  $output .= wp_nav_menu( $args );
  $output .= '</nav>';

  $output .= '<nav class="secondary-nav">';
  $output  .= '<ul>';
  if(get_phone()){
    $output .= '<li>' . get_phone('icon=1') . '</li>';
  }
  if(get_fax()){
    $output .= '<li>' . get_fax('icon=1') . '</li>';
  }
  if(get_email()){
    $output .= '<li>' . get_email('icon=1') . '</li>';
  }
  $output  .= '</ul>';
  $output .= '</nav>';

  $output .= '<div class="site-nav-social-media">';
  $output .= get_social_list();
  $output .= '</div>';

  $output .= '<div class="site-nav-searchform">';
  $output .= get_search_form($echo = false);
  $output .= '</div>';

  $output .= '</div>';

  $output .= '</div>';

  $output .= '</div>';
  $output .= '</div>';
  $output .= '</header>';

  if($header){
    echo $output;
  }

}

add_action( 'body_begin', 'show_header', 99 );

function tutsu_add_css_has_header( $classes ) {
  $header = get_field('activate_header', 'option');
  if($header){
    $classes[] = 'has-site-header';
  }
  return $classes;
}
add_filter( 'body_class', 'tutsu_add_css_has_header' );
