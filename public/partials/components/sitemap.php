<?php

function show_sitemap( $atts = '' ) {

  $defaults = array(
    'title' => 'Sitemap',
  );

  $atts = wp_parse_args( $atts, $defaults );

  wp_nav_menu( array(
    'theme_location'  => 'footer',
    'menu_class'      => 'sitemap',
    'container'       => 'ul',
    'depth'           => '1'
  ) );

}
