<?php

function show_home_slider( $atts = '' ) {
  $current_id = get_the_ID();
  $slides = get_field('home_slides', $current_id);

  $slide_count = count($slides);

  $defaults = array(
    'spacer' => 0,
    'scroll' => '',
  );

  $atts = wp_parse_args( $atts, $defaults );

  $spacer = $atts['spacer'];
  $scroll = $atts['scroll'];

  $output = '';
  if($slides && is_front_page()){
    $output .= '<div class="home-slider">';

    if($slide_count > 1){
      $output .= '<div class="slick-home-slider">';
    }

    if(is_page() || is_home()){
      $i = 0;
      foreach ($slides as $slide) {
        $i++;
        $active = $slide['active'];
        $class = $slide['class'];
        $id = $slide['id'];
        $content = $slide['content'];
        $image = $slide['image'];
        $image_url = $image['url'];


        if($active){
          if($slide_count > 1){
            $output .= '<div>';
          }
          $output .= '<div class="home-hero';
          if($class){
            $output .= ' ' . $class . '"';
          } else {
            $output .= '"';
          }
          if($id){
            $output .= ' id="' . $id . '"';
          }
          if($image){
            $output .= ' style="';
            if($image){
              $output .= 'background-image:url(' . $image_url . ');';
            }
            $output .= '"';
          }
          $output .= '>';
          $output .= '<div class="hero-overlay">';
          if($spacer){
            $output .= '<div class="site-header-spacer"></div>';
          }
          $output .= '<div class="hero-content">';
          if($content){
            $output .= '<div class="container">';
            $output .= '<div class="hero-content-inner">';
            $output .= apply_filters('the_content', $content);
            $output .= '</div>';
            $output .= '</div>';
          }
          $output .= '</div>';
          $output .= '</div>';
          $output .= '</div>';
          if($slide_count > 1){
            $output .= '</div>';
          }
        }
      }
    }

    if($slide_count > 1){
      $output .= '</div>';
    }

    if($scroll){
      $output .= '<a class="smooth-scroll" href="#' . $scroll . '"><span></span><span></span><span></span></a>';
    }
    $output .= '</div>';

  }

  echo $output;

}


