<?php

function show_banners() {
  $current_id = get_the_ID();
  $banners = get_field('banners', 'options');

  $output = '';
  if($banners){
    if(is_page() || is_home()){
      $i = 0;
      foreach ($banners as $banner) {
        $active = $banner['active'];
        $class = $banner['class'];
        $id = $banner['id'];
        $content = $banner['content'];
        $all = $banner['all'];
        $includes = $banner['include'];
        $exceptions = $banner['exceptions'];
        $image = $banner['image'];
        $image_url = $image['url'];
        $container = $banner['container'];

        $i++;

        $is_current_page = array();
        if($all && $exceptions){
          foreach ($exceptions as $exception){
            array_push($is_current_page, $exception);
          }
        } elseif(!$all && $includes){
          foreach ($includes as $include){
            array_push($is_current_page, $include);
          }
        } else {
          $exceptions = array();
        }

        $activated_output = '';
        $activated_output .= '

<section class="banner-' . $i;
          if($class){
            $activated_output .= ' ' . $class;
          }
          $activated_output .= '"';
          if($id){
            $activated_output .= ' id="' . $id . '"';
          }
          $activated_output .= '>';
        if($image){
          $activated_output .= '
  <div class="section-image" style="background-image:url(' . $image_url . ');"></div>';
        }
        $activated_output .= '
  <div class="container';
        if($container){
          $activated_output .= $container;
        }
        $activated_output .= '">';
        $activated_output .= '
    <div class="section-inner">
      ';
        $activated_output .=
    apply_filters('the_content', $content);
        $activated_output .= '
    </div>';
        $activated_output .= '
  </div>';
        $activated_output .= '
</section>';


        if(is_page() || is_home()) {
          if ($all) {
            if($active && !in_array($current_id, $is_current_page)){
              $output .= $activated_output;
            }
          } else {
            if($active && in_array($current_id, $is_current_page)){
              $output .= $activated_output;
            }
          }
        }
      }
    }
  }

  echo $output;

}


add_filter( 'get_footer', 'show_banners', 11 );