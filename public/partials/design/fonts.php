<?php

function font_1(){
	return get_field('font_1', 'option');
}
function font_1_styles(){
	return get_field('font_1_styles', 'option');
}
function font_1_typeface(){
	return get_field('font_1_typeface', 'option');
}
function font_2(){
	return get_field('font_2', 'option');
}
function font_2_styles(){
	return get_field('font_2_styles', 'option');
}
function font_2_typeface(){
	return get_field('font_2_typeface', 'option');
}
function font_3(){
	return get_field('font_3', 'option');
}
function font_3_styles(){
	return get_field('font_3_styles', 'option');
}
function font_3_typeface(){
	return get_field('font_3_typeface', 'option');
}

function tutsu_load_fonts_js() {
	//var
	$font_source = get_field('font_source', 'option');
	$font_2 = get_field('font_2', 'option');
	$font_3 = get_field('font_3', 'option');
	$typekit_id = get_field('typekit_id', 'option');

  $output  = '
<script type=\'text/javascript\'>';
	$output .= 'WebFont.load({';
	// Google Font
	if($font_source == "google") {
		$output .= 'google: {';
		$output .= 'families: [\'';
		$output .= font_1() . ':' . font_1_styles();
		if($font_2){
			$output .= '\', \'';
			$output .= font_2() . ':' . font_2_styles();
		}
		if($font_3){
			$output .= '\', \'';
			$output .= font_3() . ':' . font_3_styles();
		}
		$output .= '\']';
		$output .= '}';
	}
	// Typekit
	if($font_source == "typekit") {
		$output .= 'typekit: {';
		$output .= 'id: \'' . $typekit_id . '\'';
		$output .= '}';
	}
	$output .= '});';
	$output .= '(function(d) {';
	$output .= 'var wf = d.createElement(\'script\'), s = d.scripts[0];';
	$output .= 'wf.src = \'https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js\';';
	$output .= 's.parentNode.insertBefore(wf, s);';
	$output .= '})(document);';
	$output .= '</script>
';

	echo $output;
}
add_action( 'wp_footer', 'tutsu_load_fonts_js', 9000 );
