<?php
function tutsu_mce4_options($init) {

  $custom_colours = '
    "' . str_replace('#', '', get_field('color_1', 'option')) . '", "Color 1",
    "' . str_replace('#', '', get_field('color_2', 'option')) . '", "Color 2",
    "' . str_replace('#', '', get_field('color_black', 'option')) . '", "Black",
    "' . str_replace('#', '', get_field('color_dark', 'option')) . '", "Dark",
    "' . str_replace('#', '', get_field('color_grey', 'option')) . '", "Grey",
    "' . str_replace('#', '', get_field('color_faded', 'option')) . '", "Faded"
  ';

  // build colour grid default+custom colors
  $init['textcolor_map'] = '['.$custom_colours.']';

  // change the number of rows in the grid if the number of colors changes
  // 8 swatches per row
  $init['textcolor_rows'] = 1;

  return $init;
}
add_filter('tiny_mce_before_init', 'tutsu_mce4_options');
