<?php

$components_files=glob( TUTSU_PLUGIN_DIR . '/public/partials/components/*.php');
foreach( $components_files as $components_file ) {
  $components_slug = basename($components_file, ".php");
  include( $components_file );
}
