<?php

$apis_files=glob( TUTSU_PLUGIN_DIR . '/public/partials/apis/*.php');
foreach( $apis_files as $apis_file ) {
  $apis_slug = basename($apis_file, ".php");
  include( $apis_file );
}