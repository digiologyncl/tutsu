<?php

// Clean shortcodes
function tutsu_clean_shortcodes($content){
  $array = array (
      '<p>[' => '[',
      ']</p>' => ']',
      ']<br />' => ']',
      '<p><span>[' => '[',
      ']</span></p>' => ']',
      ']<br>' => ']',
      ']<br />' => ']',
      '<p></p>' => '',
  );
  $content = strtr($content, $array);
  return $content;
}
add_filter('the_content', 'tutsu_clean_shortcodes');

// img unautop
function filter_ptags_on_images($content){
  return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}
add_filter('the_content', 'filter_ptags_on_images');

// Automatically fetch files within the /shortcodes folder and add them as shortcodes
$shortcode_files=glob( TUTSU_PLUGIN_DIR . '/public/partials/shortcodes/*.php');
foreach( $shortcode_files as $shortcode_file ) {
  $shortcode_slug = basename($shortcode_file, ".php");
  include( $shortcode_file );
  add_shortcode( $shortcode_slug , 'get_'.$shortcode_slug );
}



// ----------------------------------------------------------------------------------------------------
// Init process for registering extra buttons

add_action( 'init' , 'tinymce_extra_buttons_init' );

function tinymce_extra_buttons_init()
{
  //Abort early if the user will never see TinyMCE
  if( !current_user_can( 'edit_posts' ) && !current_user_can( 'edit_pages' ) && get_user_option( 'rich_editing' ) == 'true' ) return;

  //Add a callback to regiser our tinymce plugin
  add_filter( 'mce_external_plugins' , 'tinymce_extra_buttons_register' );

  // Add a callback to add our button to the TinyMCE toolbar
  add_filter( 'mce_buttons_3' , 'tinymce_extra_buttons' , 91 );
}

// --------------------------------------------------
// This callback registers our plug-in

function tinymce_extra_buttons_register( $plugin_array )
{
  $plugin_array[ 'tinymce_extra_buttons' ] = plugins_url( '../../public/assets/js/tinymce-plugin.js' , __FILE__ );
  return $plugin_array;
}

// --------------------------------------------------
// This callback adds our button to the toolbar

function tinymce_extra_buttons( $buttons )
{
  //Add the button ID to the $button array
  $buttons[] = 'tinymce_email_button';
  $buttons[] = 'tinymce_phone_button';
  $buttons[] = 'tinymce_mobile_button';
  $buttons[] = 'tinymce_fax_button';
  $buttons[] = 'tinymce_address_button';
  $buttons[] = 'tinymce_opening_hours_button';
  $buttons[] = 'tinymce_icon_button';
  $buttons[] = 'tinymce_btn_button';
  $buttons[] = 'tinymce_lead_button';
  $buttons[] = 'tinymce_social_list_button';
  $buttons[] = 'tinymce_map_button';
  $buttons[] = 'tinymce_vimeo_button';
  $buttons[] = 'tinymce_youtube_button';
  $buttons[] = 'tinymce_card_button';
  $buttons[] = 'tinymce_cols_button';
  $buttons[] = 'tinymce_faqs_button';
  $buttons[] = 'tinymce_tabs_button';
  $buttons[] = 'tinymce_staff_button';
  $buttons[] = 'tinymce_testimonials_button';
  return $buttons;
}

