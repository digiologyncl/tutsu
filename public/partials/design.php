<?php

// Automatically fetch files within the /design folder
$design_files=glob( TUTSU_PLUGIN_DIR . '/public/partials/design/*.php');
foreach( $design_files as $design_file ) {
  $design_slug = basename($design_file, ".php");
  require_once TUTSU_PLUGIN_DIR . '/public/partials/design/' . $design_slug . '.php';
}
