<?php

function tutsu_enqueue_scripts() {

	$external_scripts = get_field('admin_setup_external_scripts', 'option');
	$optional_settings = get_field('admin_setup_optional_settings', 'option');

	// Theme initial stylesheet.
	wp_enqueue_style( 'tutsu-init', tutsu_plugin_url( 'public/assets/css/init.css' ), array(), null );

	// Theme grid stylesheet.
	wp_enqueue_style( 'tutsu-grid', tutsu_plugin_url( 'public/assets/css/grid.css' ), array(), null );
	if( $external_scripts && in_array('material_icons', $external_scripts) ) {
		// Material Icons.
		wp_enqueue_style( 'tutsu-material-icons', tutsu_plugin_url( 'public/assets/fonts/material-icons.css' ), array(), null );
	}
	// Theme components stylesheet.
	wp_enqueue_style( 'tutsu-components', tutsu_plugin_url( 'public/assets/css/components.css' ), array(), null );
	if( $external_scripts && in_array('font_awesome', $external_scripts) ) {
	// 	// Font Awesome stylesheet.
		wp_enqueue_style( 'tutsu-font-awesome', tutsu_plugin_url( 'public/assets/css/font-awesome.min.css' ), array(), '4.7.1' );
	}
	if( $external_scripts && in_array('lightbox', $external_scripts) ) {
		// Lightbox2 stylesheet.
		wp_enqueue_style( 'tutsu-lightbox2-style', tutsu_plugin_url( 'public/assets/css/lightbox.min.css' ), array(), '2.9.0' );
	}
	if( $external_scripts && in_array('slick_slider', $external_scripts) ) {
		// Slick stylesheet.
		wp_enqueue_style( 'tutsu-slick-style', tutsu_plugin_url( 'public/assets/css/slick.css' ), array(), '1.6.0' );
		wp_enqueue_style( 'tutsu-slick-theme-style', tutsu_plugin_url( 'public/assets/css/slick-theme.css' ), array(), '1.6.0' );
	}

	if(get_field('api_rateabiz_id', 'option')){
		wp_enqueue_style( 'tutsu-rateabiz-style', tutsu_plugin_url( 'public/assets/css/rateabiz-widget.css' ), array(), null );
	}


	// Web Font Loader.
	wp_enqueue_script( 'tutsu-web-font-loader', tutsu_plugin_url( 'public/assets/js/webfont.js' ), array(), '1.6.26', true );

	// if( $external_scripts && in_array('font_awesome', $external_scripts) ) {
	// 	// Font Awesome CDN.
	// 	wp_enqueue_script( 'tutsu-font-awesome-script', 'https://use.fontawesome.com/ea5c2bd848.js', array(), '4.7.0', true );
	// }
	if( $external_scripts && in_array('lightbox', $external_scripts) ) {
		// Lightbox2 javascript.
		wp_enqueue_script( 'tutsu-lightbox2', tutsu_plugin_url( 'public/assets/js/lightbox.min.js' ), array( 'jquery' ), '2.9.0', true );
	}
	if( $external_scripts && in_array('waypoints', $external_scripts) ) {
		// Waypoints javascript.
		wp_enqueue_script( 'tutsu-waypoints', tutsu_plugin_url( 'public/assets/js/jquery.waypoints.min.js' ), array( 'jquery' ), '4.0.1', true );
	}
	if( $external_scripts && in_array('slick_slider', $external_scripts) ) {
		// Slick javascript.
		wp_enqueue_script( 'tutsu-slick', tutsu_plugin_url( 'public/assets/js/slick.min.js' ), array( 'jquery' ), '1.6.0', true );
	}

	// Theme javascript.
	wp_enqueue_script( 'tutsu-javascript', tutsu_plugin_url( 'public/assets/js/script.js' ), array( 'jquery' ), '1.0.0', true );

	// Google Recaptcha
	if(get_field('tutsu_forms_settings_recaptcha_activate', 'options')){
		wp_enqueue_script( 'tutsu-recaptcha', 'https://www.google.com/recaptcha/api.js', array(), '2', true );
	}

}
add_action( 'wp_enqueue_scripts', 'tutsu_enqueue_scripts' );

function tutsu_enqueue_admin_scripts() {
	wp_enqueue_style( 'tutsu-admin-style', tutsu_plugin_url( 'public/assets/css/admin-style.css' ), array(), null );
	wp_enqueue_script( 'tutsu-font-awesome-admin-script', 'https://use.fontawesome.com/ea5c2bd848.js', array(), '4.7.0', true );
}
add_action( 'admin_enqueue_scripts', 'tutsu_enqueue_admin_scripts' );
