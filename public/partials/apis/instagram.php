<?php

// ADD INSTAGRAM
function tutsu_instagram_api(){

  $activate = get_field('activate_instagram_api', 'option');
  $username = get_field('api_instagram_username', 'option');

  if($username){
    $str = file_get_contents('https://www.instagram.com/' . $username . '/?__a=1');
    $json = json_decode($str, true);
    $id = $json['user']['id'];
  }

  if($activate) {
    return $json;
  }

}

