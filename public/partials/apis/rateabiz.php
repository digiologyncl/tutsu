<?php

// ADD RATEABIZ API
function tutsu_rateabiz_api(){

  $activate = get_field('field_activate_rateabiz', 'option');
  $id = get_field('api_rateabiz_id', 'option');
  $divid = get_field('api_rateabiz_divid', 'option');
  $name = get_field('api_rateabiz_seofriendlyname', 'option');

  $output = '';

  if($id){

    $output .= '
<script type="text/javascript">(function(w, d){w._rab_review_q = w._rab_review_q || []; w._rab_review_q.push({id: \'' . $id . '\', reviewDivId: \'' . $divid . '\', seoFriendlyName: \'' . $name . '\'}); var el = d.createElement("script"); el.async=true; el.src="' . tutsu_plugin_url( 'public/assets/js/rateabiz-widget-loader.js' ) . '"; el.type="text/javascript"; var s0 = d.getElementsByTagName("script")[0]; s0.parentNode.insertBefore(el, s0);})(window, document);</script>
';

  }

  if($activate){
    echo $output;
  }

}

add_action('wp_footer', 'tutsu_rateabiz_api', 9000);