<?php

// Register Product Post Type
function product_post_type() {
  $labels = array(
    'name'                  => _x( 'Products', 'Post Type General Name', 'tutsu' ),
    'singular_name'         => _x( 'Product', 'Post Type Singular Name', 'tutsu' ),
    'menu_name'             => __( 'Products', 'tutsu' ),
    'name_admin_bar'        => __( 'Product', 'tutsu' ),
    'archives'              => __( 'Product Archives', 'tutsu' ),
    'parent_item_colon'     => __( 'Parent Product:', 'tutsu' ),
    'all_items'             => __( 'All Products', 'tutsu' ),
    'add_new_item'          => __( 'Add New Product', 'tutsu' ),
    'add_new'               => __( 'Add New', 'tutsu' ),
    'new_item'              => __( 'New Product', 'tutsu' ),
    'edit_item'             => __( 'Edit Product', 'tutsu' ),
    'update_item'           => __( 'Update Product', 'tutsu' ),
    'view_item'             => __( 'View Product', 'tutsu' ),
    'search_items'          => __( 'Search Product', 'tutsu' ),
    'not_found'             => __( 'Not found', 'tutsu' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'tutsu' ),
    'featured_image'        => __( 'Featured Image', 'tutsu' ),
    'set_featured_image'    => __( 'Set featured image', 'tutsu' ),
    'remove_featured_image' => __( 'Remove featured image', 'tutsu' ),
    'use_featured_image'    => __( 'Use as featured image', 'tutsu' ),
    'insert_into_item'      => __( 'Insert into Product', 'tutsu' ),
    'uploaded_to_this_item' => __( 'Uploaded to this product', 'tutsu' ),
    'items_list'            => __( 'Products list', 'tutsu' ),
    'items_list_navigation' => __( 'Products list navigation', 'tutsu' ),
    'filter_items_list'     => __( 'Filter products list', 'tutsu' ),
  );
  $args = array(
    'label'                 => __( 'Product', 'tutsu' ),
    'description'           => __( 'This is where our products go', 'tutsu' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'menu_icon'             => 'dashicons-products',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'rewrite'               => array( 'slug' => 'products' ),
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
  );
  register_post_type( 'product', $args );
}
add_action( 'init', 'product_post_type', 0 );

