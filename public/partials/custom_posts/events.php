<?php

// Register Event Post Type
function event_post_type() {
  $labels = array(
    'name'                  => _x( 'Events', 'Post Type General Name', 'tutsu' ),
    'singular_name'         => _x( 'Event', 'Post Type Singular Name', 'tutsu' ),
    'menu_name'             => __( 'Events', 'tutsu' ),
    'name_admin_bar'        => __( 'Event', 'tutsu' ),
    'archives'              => __( 'Event Archives', 'tutsu' ),
    'parent_item_colon'     => __( 'Parent Event:', 'tutsu' ),
    'all_items'             => __( 'All Events', 'tutsu' ),
    'add_new_item'          => __( 'Add New Event', 'tutsu' ),
    'add_new'               => __( 'Add New', 'tutsu' ),
    'new_item'              => __( 'New Event', 'tutsu' ),
    'edit_item'             => __( 'Edit Event', 'tutsu' ),
    'update_item'           => __( 'Update Event', 'tutsu' ),
    'view_item'             => __( 'View Event', 'tutsu' ),
    'search_items'          => __( 'Search Event', 'tutsu' ),
    'not_found'             => __( 'Not found', 'tutsu' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'tutsu' ),
    'featured_image'        => __( 'Featured Image', 'tutsu' ),
    'set_featured_image'    => __( 'Set featured image', 'tutsu' ),
    'remove_featured_image' => __( 'Remove featured image', 'tutsu' ),
    'use_featured_image'    => __( 'Use as featured image', 'tutsu' ),
    'insert_into_item'      => __( 'Insert into Event', 'tutsu' ),
    'uploaded_to_this_item' => __( 'Uploaded to this event', 'tutsu' ),
    'items_list'            => __( 'Events list', 'tutsu' ),
    'items_list_navigation' => __( 'Events list navigation', 'tutsu' ),
    'filter_items_list'     => __( 'Filter events list', 'tutsu' ),
  );
  $args = array(
    'label'                 => __( 'Event', 'tutsu' ),
    'description'           => __( 'This is where our events go', 'tutsu' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'menu_icon'             => 'dashicons-tickets-alt',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'rewrite'               => array( 'slug' => 'events' ),
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
  );
  register_post_type( 'event', $args );
}
add_action( 'init', 'event_post_type', 0 );
