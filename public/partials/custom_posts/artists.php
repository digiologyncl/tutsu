<?php

// Register Artist Post Type
function artist_post_type() {
  $labels = array(
    'name'                  => _x( 'Artists', 'Post Type General Name', 'tutsu' ),
    'singular_name'         => _x( 'Artist', 'Post Type Singular Name', 'tutsu' ),
    'menu_name'             => __( 'Artists', 'tutsu' ),
    'name_admin_bar'        => __( 'Artist', 'tutsu' ),
    'archives'              => __( 'Artist Archives', 'tutsu' ),
    'parent_item_colon'     => __( 'Parent Artist:', 'tutsu' ),
    'all_items'             => __( 'All Artists', 'tutsu' ),
    'add_new_item'          => __( 'Add New Artist', 'tutsu' ),
    'add_new'               => __( 'Add New', 'tutsu' ),
    'new_item'              => __( 'New Artist', 'tutsu' ),
    'edit_item'             => __( 'Edit Artist', 'tutsu' ),
    'update_item'           => __( 'Update Artist', 'tutsu' ),
    'view_item'             => __( 'View Artist', 'tutsu' ),
    'search_items'          => __( 'Search Artist', 'tutsu' ),
    'not_found'             => __( 'Not found', 'tutsu' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'tutsu' ),
    'featured_image'        => __( 'Featured Image', 'tutsu' ),
    'set_featured_image'    => __( 'Set featured image', 'tutsu' ),
    'remove_featured_image' => __( 'Remove featured image', 'tutsu' ),
    'use_featured_image'    => __( 'Use as featured image', 'tutsu' ),
    'insert_into_item'      => __( 'Insert into Artist', 'tutsu' ),
    'uploaded_to_this_item' => __( 'Uploaded to this artist', 'tutsu' ),
    'items_list'            => __( 'Artists list', 'tutsu' ),
    'items_list_navigation' => __( 'Artists list navigation', 'tutsu' ),
    'filter_items_list'     => __( 'Filter artists list', 'tutsu' ),
  );
  $args = array(
    'label'                 => __( 'Artist', 'tutsu' ),
    'description'           => __( 'This is where our artists go', 'tutsu' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions' ),
    'taxonomies'            => array( 'post_tag', 'category' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'menu_icon'             => 'dashicons-album',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'rewrite'               => array( 'slug' => 'artists' ),
    'exclude_from_search'   => true,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
  );
  register_post_type( 'artist', $args );
}
add_action( 'init', 'artist_post_type', 0 );
