<?php

// Register Case Study Post Type
function case_study_post_type() {
  $labels = array(
    'name'                       => _x( 'Tags', 'taxonomy general name', 'tutsu' ),
    'singular_name'              => _x( 'Tag', 'taxonomy singular name', 'tutsu' ),
    'search_items'               => __( 'Search Tags', 'tutsu' ),
    'popular_items'              => __( 'Popular Tags', 'tutsu' ),
    'all_items'                  => __( 'All Tags', 'tutsu' ),
    'parent_item'                => null,
    'parent_item_colon'          => null,
    'edit_item'                  => __( 'Edit Tag', 'tutsu' ),
    'update_item'                => __( 'Update Tag', 'tutsu' ),
    'add_new_item'               => __( 'Add New Tag', 'tutsu' ),
    'new_item_name'              => __( 'New Tag Name', 'tutsu' ),
    'separate_items_with_commas' => __( 'Separate tags with commas', 'tutsu' ),
    'add_or_remove_items'        => __( 'Add or remove tags', 'tutsu' ),
    'choose_from_most_used'      => __( 'Choose from the most used tags', 'tutsu' ),
    'not_found'                  => __( 'No tags found.', 'tutsu' ),
    'menu_name'                  => __( 'Tags', 'tutsu' ),
  );
  $args = array(
    'hierarchical'          => false,
    'labels'                => $labels,
    'show_ui'               => true,
    'show_admin_column'     => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var'             => true,
    'rewrite'               => array( 'slug' => 'case-studies/tags' ),
  );
  register_taxonomy( 'case-study-tags', 'case-study', $args );

  $labels = array(
    'name'                  => _x( 'Case Studies', 'Post Type General Name', 'tutsu' ),
    'singular_name'         => _x( 'Case Study', 'Post Type Singular Name', 'tutsu' ),
    'menu_name'             => __( 'Case Studies', 'tutsu' ),
    'name_admin_bar'        => __( 'Case Study', 'tutsu' ),
    'archives'              => __( 'Case Study Archives', 'tutsu' ),
    'parent_item_colon'     => __( 'Parent Case Study:', 'tutsu' ),
    'all_items'             => __( 'All Case Studies', 'tutsu' ),
    'add_new_item'          => __( 'Add New Case Study', 'tutsu' ),
    'add_new'               => __( 'Add New', 'tutsu' ),
    'new_item'              => __( 'New Case Study', 'tutsu' ),
    'edit_item'             => __( 'Edit Case Study', 'tutsu' ),
    'update_item'           => __( 'Update Case Study', 'tutsu' ),
    'view_item'             => __( 'View Case Study', 'tutsu' ),
    'search_items'          => __( 'Search Case Study', 'tutsu' ),
    'not_found'             => __( 'Not found', 'tutsu' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'tutsu' ),
    'featured_image'        => __( 'Featured Image', 'tutsu' ),
    'set_featured_image'    => __( 'Set featured image', 'tutsu' ),
    'remove_featured_image' => __( 'Remove featured image', 'tutsu' ),
    'use_featured_image'    => __( 'Use as featured image', 'tutsu' ),
    'insert_into_item'      => __( 'Insert into Case Study', 'tutsu' ),
    'uploaded_to_this_item' => __( 'Uploaded to this case_study', 'tutsu' ),
    'items_list'            => __( 'Case Studies list', 'tutsu' ),
    'items_list_navigation' => __( 'Case Studies list navigation', 'tutsu' ),
    'filter_items_list'     => __( 'Filter case studies list', 'tutsu' ),
  );
  $args = array(
    'label'                 => __( 'Case Study', 'tutsu' ),
    'description'           => __( 'This is where our case studies go', 'tutsu' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions' ),
    'taxonomies'            => array( 'case-study-tags' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'menu_icon'             => 'dashicons-book-alt',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'rewrite'               => array( 'slug' => 'case-studies' ),
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
  );
  register_post_type( 'case-study', $args );
}

add_action( 'init', 'case_study_post_type', 0 );