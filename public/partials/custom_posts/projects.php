<?php

// Register Project Post Type
function project_post_type() {
  $labels = array(
    'name'                  => _x( 'Projects', 'Post Type General Name', 'tutsu' ),
    'singular_name'         => _x( 'Project', 'Post Type Singular Name', 'tutsu' ),
    'menu_name'             => __( 'Projects', 'tutsu' ),
    'name_admin_bar'        => __( 'Project', 'tutsu' ),
    'archives'              => __( 'Project Archives', 'tutsu' ),
    'parent_item_colon'     => __( 'Parent Project:', 'tutsu' ),
    'all_items'             => __( 'All Projects', 'tutsu' ),
    'add_new_item'          => __( 'Add New Project', 'tutsu' ),
    'add_new'               => __( 'Add New', 'tutsu' ),
    'new_item'              => __( 'New Project', 'tutsu' ),
    'edit_item'             => __( 'Edit Project', 'tutsu' ),
    'update_item'           => __( 'Update Project', 'tutsu' ),
    'view_item'             => __( 'View Project', 'tutsu' ),
    'search_items'          => __( 'Search Project', 'tutsu' ),
    'not_found'             => __( 'Not found', 'tutsu' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'tutsu' ),
    'featured_image'        => __( 'Featured Image', 'tutsu' ),
    'set_featured_image'    => __( 'Set featured image', 'tutsu' ),
    'remove_featured_image' => __( 'Remove featured image', 'tutsu' ),
    'use_featured_image'    => __( 'Use as featured image', 'tutsu' ),
    'insert_into_item'      => __( 'Insert into Project', 'tutsu' ),
    'uploaded_to_this_item' => __( 'Uploaded to this project', 'tutsu' ),
    'items_list'            => __( 'Projects list', 'tutsu' ),
    'items_list_navigation' => __( 'Projects list navigation', 'tutsu' ),
    'filter_items_list'     => __( 'Filter projects list', 'tutsu' ),
  );
  $args = array(
    'label'                 => __( 'Project', 'tutsu' ),
    'description'           => __( 'This is where our projects go', 'tutsu' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'menu_icon'             => 'dashicons-art',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'rewrite'               => array( 'slug' => 'projects' ),
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
  );
  register_post_type( 'project', $args );
}
add_action( 'init', 'project_post_type', 0 );

