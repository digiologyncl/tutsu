<?php

// Register Offer Post Type
function offer_post_type() {
  $labels = array(
    'name'                  => _x( 'Offers', 'Post Type General Name', 'tutsu' ),
    'singular_name'         => _x( 'Offer', 'Post Type Singular Name', 'tutsu' ),
    'menu_name'             => __( 'Offers', 'tutsu' ),
    'name_admin_bar'        => __( 'Offer', 'tutsu' ),
    'archives'              => __( 'Offer Archives', 'tutsu' ),
    'parent_item_colon'     => __( 'Parent Offer:', 'tutsu' ),
    'all_items'             => __( 'All Offers', 'tutsu' ),
    'add_new_item'          => __( 'Add New Offer', 'tutsu' ),
    'add_new'               => __( 'Add New', 'tutsu' ),
    'new_item'              => __( 'New Offer', 'tutsu' ),
    'edit_item'             => __( 'Edit Offer', 'tutsu' ),
    'update_item'           => __( 'Update Offer', 'tutsu' ),
    'view_item'             => __( 'View Offer', 'tutsu' ),
    'search_items'          => __( 'Search Offer', 'tutsu' ),
    'not_found'             => __( 'Not found', 'tutsu' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'tutsu' ),
    'featured_image'        => __( 'Featured Image', 'tutsu' ),
    'set_featured_image'    => __( 'Set featured image', 'tutsu' ),
    'remove_featured_image' => __( 'Remove featured image', 'tutsu' ),
    'use_featured_image'    => __( 'Use as featured image', 'tutsu' ),
    'insert_into_item'      => __( 'Insert into Offer', 'tutsu' ),
    'uploaded_to_this_item' => __( 'Uploaded to this offer', 'tutsu' ),
    'items_list'            => __( 'Offers list', 'tutsu' ),
    'items_list_navigation' => __( 'Offers list navigation', 'tutsu' ),
    'filter_items_list'     => __( 'Filter offers list', 'tutsu' ),
  );
  $args = array(
    'label'                 => __( 'Offer', 'tutsu' ),
    'description'           => __( 'This is where our offers go', 'tutsu' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'menu_icon'             => 'dashicons-money',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'rewrite'               => array( 'slug' => 'offers' ),
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
  );
  register_post_type( 'offer', $args );
}
add_action( 'init', 'offer_post_type', 0 );



if( function_exists('acf_add_local_field_group') ) {
  acf_add_local_field_group(
    array (
      'key' => 'group_offer_image',
      'title' => 'Offer Image',
      'fields' => array (
        array (
          'key' => 'field_offer_modal',
          'label' => 'Modal',
          'name' => 'offer_modal',
          'type' => 'wysiwyg',
        ),
        array (
          'key' => 'field_offer_modal_activate_on_home',
          'label' => 'Activate on Home Page',
          'name' => 'offer_modal_activate_on_home',
          'type' => 'true_false',
        ),
        array (
          'key' => 'field_offer_modal_activate_on_leave',
          'label' => 'Activate on Leave',
          'name' => 'offer_modal_activate_on_leave',
          'type' => 'true_false',
        ),
      ),
      'location' => array (
        array (
          array (
            'param' => 'post_type',
            'operator' => '==',
            'value' => 'offer',
          ),
        ),
      ),
      'menu_order' => 99,
      'style' => 'default',
      'label_placement' => 'top',
      'instruction_placement' => 'label',
      'hide_on_screen' => '',
    )
  );
}
