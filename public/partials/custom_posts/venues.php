<?php

// Register Venue Post Type
function venue_post_type() {
  $labels = array(
    'name'                  => _x( 'Venues', 'Post Type General Name', 'tutsu' ),
    'singular_name'         => _x( 'Venue', 'Post Type Singular Name', 'tutsu' ),
    'menu_name'             => __( 'Venues', 'tutsu' ),
    'name_admin_bar'        => __( 'Venue', 'tutsu' ),
    'archives'              => __( 'Venue Archives', 'tutsu' ),
    'parent_item_colon'     => __( 'Parent Venue:', 'tutsu' ),
    'all_items'             => __( 'All Venues', 'tutsu' ),
    'add_new_item'          => __( 'Add New Venue', 'tutsu' ),
    'add_new'               => __( 'Add New', 'tutsu' ),
    'new_item'              => __( 'New Venue', 'tutsu' ),
    'edit_item'             => __( 'Edit Venue', 'tutsu' ),
    'update_item'           => __( 'Update Venue', 'tutsu' ),
    'view_item'             => __( 'View Venue', 'tutsu' ),
    'search_items'          => __( 'Search Venue', 'tutsu' ),
    'not_found'             => __( 'Not found', 'tutsu' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'tutsu' ),
    'featured_image'        => __( 'Featured Image', 'tutsu' ),
    'set_featured_image'    => __( 'Set featured image', 'tutsu' ),
    'remove_featured_image' => __( 'Remove featured image', 'tutsu' ),
    'use_featured_image'    => __( 'Use as featured image', 'tutsu' ),
    'insert_into_item'      => __( 'Insert into Venue', 'tutsu' ),
    'uploaded_to_this_item' => __( 'Uploaded to this venue', 'tutsu' ),
    'items_list'            => __( 'Venues list', 'tutsu' ),
    'items_list_navigation' => __( 'Venues list navigation', 'tutsu' ),
    'filter_items_list'     => __( 'Filter venues list', 'tutsu' ),
  );
  $args = array(
    'label'                 => __( 'Venue', 'tutsu' ),
    'description'           => __( 'This is where our venues go', 'tutsu' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'menu_icon'             => 'dashicons-store',
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'rewrite'               => array( 'slug' => 'venues' ),
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
  );
  register_post_type( 'venue', $args );
}
add_action( 'init', 'venue_post_type', 0 );
