<?php

function tutsu_email_template_contact_us($atts) {

  $defaults = array(
    'body'   => '',
    'image'   => '',
    'link'   => '#',
    'title'   => 'Thank you for your enquiry',
    'name'   => '',
  );

  $atts = wp_parse_args( $atts, $defaults );

  $content = $atts['body'];
  $image = $atts['image'];
  $link = $atts['link'];
  $title = $atts['title'];
  $name = ' ' . $atts['name'];

  $color_1 = get_field('color_1', 'option');
  if(!$color_1){
    $color_1 = '#2b3a63';
  }

  if($image){
    $image = '<img src="' . $image['url'] . '" width="100%" height="auto" style="margin:0; padding:0; border:none; display:block;" border="0" alt="hero_image" />';
  }

    $body = '';
    $body .= '

      <table width="640" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:640px; width:100%;" bgcolor="#FFFFFF">
        <tr>
          <td align="center" valign="top">
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td align="center" valign="top">
                  ' . $image . '
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td align="center" valign="top" style="padding:10px;">

            <table width="600" cellspacing="0" cellpadding="0" border="0" align="center" style="max-width:600px; width:100%;">
              <tr>
                <td align="center" valign="top" style="padding:10px;">
                  <h2>' . $title . '</h2>';

    if($content){
      $body .= $content;
    } else {
      $body .= '
                  Hi' . $name . ', thank you for your enquiry.<br>
                  Could you please verify your email by clicking the button below.
                </td>
              </tr>
              <tr>
                <td align="center" valign="top" style="padding:10px;">
                  <table width="200" height="44" cellpadding="0" cellspacing="0" border="0" bgcolor="' . $color_1 . '" style="border-radius:4px;">
                    <tr>
                      <td align="center" valign="middle" height="44" style="font-family: Arial, sans-serif; font-size:14px; font-weight:bold;">
                        <a href="' . $link . '" target="_blank" style="font-family: Arial, sans-serif; color:#ffffff; display: inline-block; text-decoration: none; line-height:44px; width:200px; font-weight:bold;">Verify Email</a>
                      </td>
                    </tr>
                  </table>';
    }

    $body .= '
                </td>
              </tr>
              <tr>
                <td height="30" style="font-size:30px; line-height:30px;">&nbsp;</td>
              </tr>
            </table>

          </td>
        </tr>
      </table>';

  $atts = array(
    'title' => $title,
    'body'  => $body
  );

  return tutsu_email_template_default($atts);

}