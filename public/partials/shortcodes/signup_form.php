<?php
function registration_form( $username, $password, $email ) {

    echo '
<form action="' . $_SERVER['REQUEST_URI'] . '" method="post">
<fieldset>
<label for="username"><strong>Username *</strong></label>
<input class="form-control" type="text" name="username" value="' . ( isset( $_POST['username'] ) ? $username : null ) . '">
</fieldset>
<fieldset>
<label for="email"><strong>Email *</strong></label>
<input class="form-control" type="email" name="email" value="' . ( isset( $_POST['email']) ? $email : null ) . '">
</fieldset>
<fieldset>
<label for="password"><strong>Password *</strong></label>
<input class="form-control" type="password" name="password" value="' . ( isset( $_POST['password'] ) ? $password : null ) . '">
</fieldset>
<input class="btn btn-1" type="submit" name="submit" value="Register"/>
</form>
    ';
}

function registration_validation( $username, $password, $email )  {
	global $reg_errors;
	$reg_errors = new WP_Error;

	if ( empty( $username ) || empty( $password ) || empty( $email ) ) {
    $reg_errors->add('field', 'Required form field is missing');
	}

	if ( 5 > strlen( $password ) ) {
    $reg_errors->add( 'password', 'Password length must be greater than 5' );
  }

  if ( !is_email( $email ) ) {
    $reg_errors->add( 'email_invalid', 'Email is not valid' );
	}

	if ( email_exists( $email ) ) {
    $reg_errors->add( 'email', 'Email Already in use' );
	}

	if ( is_wp_error( $reg_errors ) ) {
    foreach ( $reg_errors->get_error_messages() as $error ) {
      echo '<div>';
      echo '<strong>ERROR</strong>:';
      echo $error . '<br/>';
      echo '</div>';
    }
	}
}

function complete_registration() {
	global $reg_errors, $username, $password, $email;
	if ( 1 > count( $reg_errors->get_error_messages() ) ) {
	  $userdata = array(
	  'user_login'    =>   $username,
	  'user_pass'     =>   $password,
	  'user_email'    =>   $email,
	  );
	  $user = wp_insert_user( $userdata );
	  echo 'Registration complete. Go to <a href="' . get_site_url() . '/login/"><strong>login page</strong></a>.';
	}
}

function custom_registration_function() {
	if ( isset($_POST['submit'] ) ) {
		registration_validation(
			$_POST['username'],
			$_POST['password'],
			$_POST['email']
		);

		// sanitize user form input
		global $username, $password, $email;

		$username   =   sanitize_user( $_POST['username'] );
		$password   =   esc_attr( $_POST['password'] );
		$email      =   sanitize_email( $_POST['email'] );

		// call @function complete_registration to create the user
		// only when no WP_error is found
		complete_registration(
			$username,
			$password,
			$email
		);
	}

	registration_form(
		$username,
		$password,
		$email
	);
}

function get_signup_form() {
    ob_start();
    custom_registration_function();
    return ob_get_clean();
}