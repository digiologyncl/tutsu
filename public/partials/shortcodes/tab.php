<?php

function get_tab( $atts = '' , $content = null ) {

  $defaults = array(
    'class' => '',
    'title' => ' ',
  );

  $atts = wp_parse_args( $atts, $defaults );

  $title = $atts['title'];
  $class = $atts['class'];

  $output  = '<div class="panel';
  if($class){
    $output  .= ' ' . $class;
  }
  $output  .= '" id="tab_' . str_replace(' ', '', strtolower($title)) . '">';
  $output .= '<div class="panel-content">' . do_shortcode( $content ) . '</div>';
  $output .= '</div>';

  return $output;
}