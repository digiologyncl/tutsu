<?php

function get_email( $atts = '', $content = null ) {
  $email = get_field('email', 'options');

  $defaults = array (
    'class' => '',
    'link'  => 1,
    'icon'  => 0,
    'p'     => 0,
    'pre'   => 0,
    'echo'  => 0
  );

  $atts = wp_parse_args( $atts, $defaults );

  $external_scripts = get_field('admin_setup_external_scripts', 'option');

  $class = $atts['class'];
  $link = $atts['link'];
  $icon = $atts['icon'];
  $p = $atts['p'];
  $pre   = $atts['pre'];
  $echo = $atts['echo'];

  $output = '';
  if ($email) {
    if($p) {
      $output .='<p>';
    }
    if ($link) {
      $output .='<a href="mailto:' . $email . '"';
    } else {
      $output .= '<span';
    }
    $output .= ' class="email';
    if($class) {
      $output .= ' ' . $class;
    }
    $output   .= '">';
    if($icon) {
      if( $external_scripts && in_array('material_icons', $external_scripts) ) {
        $output .= '<i class="material-icons">mail_outline</i> ';
      } elseif( $external_scripts && in_array('font_awesome', $external_scripts) ) {
        $output .= '<i class="fa fa-envelope-o"></i> ';
      } else {
        $output .= '<svg class="dg dg-envelope-o"><use xlink:href="#dg-envelope-o"></use></svg> ';
      }
    }
    if($pre && !$icon) {
      $output .= '<strong>' . $pre . '</strong> ';
    }
    $output   .= $email;
    if ($link) {
      $output .= '</a>';
    } else {
      $output .= '</span>';
    }
    if($icon && !$p) {
      $output .='</br>';
    }
    if($pre && !$p) {
      $output .='</br>';
    }
    if($p) {
      $output .='</p>';
    }

    if($echo) {
      echo $output;
    } else {
      return $output;
    }
  }
}