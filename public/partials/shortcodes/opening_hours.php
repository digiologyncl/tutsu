<?php

function get_opening_hours( $atts = '' , $content = null ) {
  $opening_hours = get_field('office_hours', 'options');
  $notes = get_field('office_hours_note', 'options');

  $defaults = array(
    'class' => 'opening-hours',
    'divider' => ':',
    'title' => '',
    'echo' => 0
  );

  $atts = wp_parse_args( $atts, $defaults );

  $class    = $atts['class'];
  $divider  = $atts['divider'];
  $title  = $atts['title'];
  $echo     = $atts['echo'];

  $output  = '';
  if($opening_hours){
    $output .= '<p class="';
    if($class){
      $output .= $class;
    }
    $output .= '">';
    if($title){
      $output .= '<strong>' . $title . '</strong><br>';
    }
    foreach ($opening_hours as $opening_hour) {
      $days = $opening_hour['days'];
      $hours = $opening_hour['hours'];

      $output .= '<span class="days">' . $days . '</span>';
      if($divider){
        $output .= '<span class="divider"> ' . $divider . ' </span>';
      }
      $output .= '<span class="hours">' . $hours . '</span><br>';
    }
  }
  if($notes){
    $output .= '<span class="notes">' . $notes . '</span>';
  }
  if($opening_hours){
    $output .= '</p>';
  }

  if($echo){
    echo $output;
  } else {
    return $output;
  }
}

