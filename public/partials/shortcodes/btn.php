<?php

function get_btn( $atts = '' , $content = null ) {
	$defaults = array(
    'class' => 'btn-1',
    'id' => '',
    'url' => '',
    'p' => 0,
    'icon' => '',
  );

  $atts = wp_parse_args( $atts, $defaults );
  
  $external_scripts = get_field('admin_setup_external_scripts', 'option');

  $class = $atts['class'];
  $id = $atts['id'];
  $url = $atts['url'];
  $p = $atts['p'];
  $icon = $atts['icon'];

  $output  = '';
  if($p){
    $output  .= '<p>';
  }
  if($id && !$url){
    $output  .= '<a href="' . get_permalink($id) . '" class="btn';
  } elseif(!$id && $url) {
    $output  .= '<a href="' . $url . '" class="btn';
  } else {
    $output  .= '<span class="btn';
  }
  if($class) {
	  $output .= ' ' . $class;
	}
  $output .= '">';
  if($icon) {
    if( $external_scripts && in_array('material_icons', $external_scripts) ) {
      $output .= '<i class="material-icons">' . $icon . '</i> ';
    } elseif( $external_scripts && in_array('font_awesome', $external_scripts) ) {
      $output .= '<i class="fa fa-' . $icon . '"></i> ';
    } else {
      $output .= '<svg class="dg dg-' . $icon . '"><use xlink:href="#dg-' . $icon . '"></use></svg> ';
    }
  }
  $output .= do_shortcode( $content );
  if($id || $url){
    $output .= '</a>';
  } else {
    $output .= '</span>';
  }
  if($p){
    $output  .= '</p>';
  }

  return $output;
}