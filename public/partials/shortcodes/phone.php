<?php

function get_phone( $atts = '', $content = null ) {
  $phone = get_field('phone', 'options');
  $phone_slug = str_replace(' ', '', $phone);

  $defaults = array(
    'class' => '',
    'link'  => 1,
    'icon'  => 0,
    'p'     => 0,
    'pre'   => 0,
    'url'  => 0,
    'echo'  => 0
  );

  $atts = wp_parse_args( $atts, $defaults );

  $external_scripts = get_field('admin_setup_external_scripts', 'option');

  $class  = $atts['class'];
  $link   = $atts['link'];
  $icon   = $atts['icon'];
  $p      = $atts['p'];
  $pre   = $atts['pre'];
  $url   = $atts['url'];
  $echo   = $atts['echo'];

  $output = '';
  if ($phone) {
    if($p) {
      $output .='<p>';
    }
    if ($link) {
      $output .='<a href="tel:' . $phone_slug . '"';
    } else {
      $output .= '<span';
    }
    $output .= ' class="phone';
    if($class) {
      $output .= ' ' . $class;
    }
    $output   .= '">';
    if($icon) {
      if( $external_scripts && in_array('material_icons', $external_scripts) ) {
        $output .= '<i class="material-icons">phone</i> ';
      } elseif( $external_scripts && in_array('font_awesome', $external_scripts) ) {
        $output .= '<i class="fa fa-phone"></i> ';
      } else {
        $output .= '<svg class="dg dg-phone"><use xlink:href="#dg-phone"></use></svg> ';
      }
    }
    if($pre && !$icon) {
      $output .= '<strong>' . $pre . '</strong> ';
    }
    $output   .= $phone;
    if ($link) {
      $output .= '</a>';
    } else {
      $output .= '</span>';
    }
    if($icon && !$p) {
      $output .='</br>';
    }
    if($pre && !$p) {
      $output .='</br>';
    }
    if($p) {
      $output .='</p>';
    }

    if ($url) {
      if ($echo) {
        echo $phone_slug;
      } else {
        return $phone_slug;
      }
    } else {
      if ($echo) {
        echo $output;
      } else {
        return $output;
      }
    }
  }
}