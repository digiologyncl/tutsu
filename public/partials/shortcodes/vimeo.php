<?php

function get_vimeo( $atts = '', $content = null ) {

  $defaults = array (
    'id' => '',
    'param' => 'badge=0',
    'echo' => 0,
    'width'  => '',
  );

  $atts = wp_parse_args( $atts, $defaults );

  // get iframe HTML
  $id = $atts['id'];
  $param = $atts['param'];
  $echo = $atts['echo'];
  $width  = $atts['width'];

  $output  = '';
  if($width){
    $output  .= '<div style="max-width:' . $width . 'px">';
  }
  $output .= '<div class="embed-container">';
  $output .= '<iframe src="https://player.vimeo.com/video/'.$id.'?' . $param . '" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
  $output .= '</div>';
  if($width){
    $output .= '</div>';
  }

  if($echo){
    echo $output;
  } else {
    return $output;
  }
}
