<?php

function get_faq( $atts = '' , $content = null ) {

  $defaults = array(
    'class' => '',
    'title' => ' ',
    'active' => 0,
  );

  $atts = wp_parse_args( $atts, $defaults );

  $class = $atts['class'];
  $title = $atts['title'];
  $active = $atts['active'];

  $output  = '<div class="faq';
  if($class) {
    $output .= ' ' . $class;
  }
  if($active) {
    $output .= ' is-expanded';
  }
  $output .= '">';
  $output .= '<h3 class="faq-title">' . $title . '</h3>';
  $output .= '<div class="faq-content"';
  if($active) {
    $output .= ' style="display:block"';
  }
  $output .= '>';
  $output .= do_shortcode( $content ) . '</div>';
  $output .= '</div>';

  return $output;
}