<?php

function get_icon( $atts = '' , $content = null ) {

	$defaults = array(
    'icon'  => '',
    'size'  => '',
    'class' => ''
  );

  $atts = wp_parse_args( $atts, $defaults );

  $icon = $atts['icon'];
  $size = $atts['size'];
  $class = $atts['class'];

  $external_scripts = get_field('admin_setup_external_scripts', 'option');

  $output = '';

  if( $external_scripts && in_array('material_icons', $external_scripts) ) {

    $output .= '<i class="material-icons';
    if($size) {
      $output .= ' md-' . $size;
    }
    if($class) {
      $output .= ' ' . $class;
    }
    $output  .= '">' . $atts['icon'] . '</i>';

  } else {

    $output .= '<i class="fa fa-' . $atts['icon'];
    if($size) {
      $output .= ' fa-' . $size . 'x';
    }
    if($class) {
      $output .= ' ' . $class;
    }
    $output  .= '"></i>';

  }

  return $output;
}