<?php

function get_show_page( $atts = '' ) {
  $defaults = array(
    'class'         => 'page',
    'id'            => '',
    'digicon'       => '',
    'show_image'    => 1,
    'show_icon'     => 1,
    'title'         => '',
    'show_title'    => 1,
    'show_subtitle' => 1,
    'show_date'     => 0,
    'show_excerpt'  => 1,
    'show_content'  => 0,
    'link_class'    => '',
    'link_text'     => 'Read More',
    'show_share'    => 1,
    'image_size'    => 'large'
  );

  $atts = wp_parse_args( $atts, $defaults );

  $class            = $atts['class'];
  $id               = $atts['id'];
  $show_image       = $atts['show_image'];
  $show_icon        = $atts['show_icon'];
  $digicon          = $atts['digicon'];
  $title            = $atts['title'];
  $show_title       = $atts['show_title'];
  $show_subtitle    = $atts['show_subtitle'];
  $show_date        = $atts['show_date'];
  $show_excerpt     = $atts['show_excerpt'];
  $link_class       = $atts['link_class'];
  $link_text        = $atts['link_text'];
  $image_size       = $atts['image_size'];

  $current_id       = get_the_ID();

  $output = '';

  if ($id) {
    $image          = get_the_post_thumbnail_url($id, $image_size);
    if(!$title) {
      $title        = get_post($id)->post_title;
    }
    $subtitle       = get_field('subtitle', $id);
    $date           = date(get_option('date_format'), strtotime(get_post($id)->post_date));
    $excerpt        = get_post($id)->post_excerpt;
    $permalink      = get_the_permalink($id);
    $featured_icon  = get_field('featured_icon', $id);

    $output   .= '<div class="' . $class;
    if($current_id == $id){
      $output .= ' is-current';
    }
    $output .= '">';
    $output .= '<div class="item-inner">';
    if($show_icon && $featured_icon){
      $output .= '<div class="item-icon"><a href="' . $permalink . '"><img src="' . $featured_icon['url'] . '" alt="' . $featured_icon['alt'] . '" /></a></div>';
    }
    if($show_icon && $digicon){
      $output .= '<div class="item-icon"><a href="' . $permalink . '"><svg class="dg dg-' . $digicon . '"><use xlink:href="#dg-' . $digicon . '" /></svg></a></div>';
    }
    if($show_title){
      $output .= '<h4 class="item-title"><a href="' . $permalink . '">' . $title . '</a></h4>';
    }
    if($show_subtitle && $subtitle){
      $output .= '<div class="item-subtitle">' . $subtitle . '</div>';
    }
    if($show_image && $image){
      $output .= '<div class="item-image-wrapper"><a href="' . $permalink . '"><div class="item-image" style="background-image:url(' . $image . ')"></div></a></div>';
    }
    if($show_excerpt && $excerpt){
      $output .= '<div class="item-excerpt">' . $excerpt . '</div>';
    }
    if($link_text){
      $output .= '<div class="item-permalink"><a href="' . $permalink . '" class="' . $link_class . '">' . $link_text . '</a></div>';
    }
    $output .= '</div>';
    $output .= '</div>';
  }

  return $output;

}