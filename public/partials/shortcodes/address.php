<?php

function get_address( $atts = '' ) {
  $company_name = get_field('company_name', 'option');
  $street_line_1 = get_field('street_line_1', 'option');
  $street_line_2 = get_field('street_line_2', 'option');
  $city = get_field('city', 'option');
  $region = get_field('region', 'option');
  $postcode = get_field('postcode', 'option');
  $country = get_field('country', 'option');

  $defaults = array(
    'class'   => 'address',
    'inline'  => 0,
    'icon'    => 0,
    'schema'  => 1,
    'name'    => 1,
    'p'       => 1,
    'echo'    => 0,
    'plain'   => 0,
  );

  $atts = wp_parse_args( $atts, $defaults );

  $external_scripts = get_field('admin_setup_external_scripts', 'option');

  $class  = $atts['class'];
  $inline = $atts['inline'];
  $icon   = $atts['icon'];
  $schema = $atts['schema'];
  $name   = $atts['name'];
  $p      = $atts['p'];
  $echo   = $atts['echo'];
  $plain   = $atts['plain'];

  $output  = '';
  if($street_line_1 || $street_line_2 || $city || $region || $postcode || $country) {

    if($p) {
      $output .= '<p';
    } else {
      $output .= '<span';
    }
    if($schema) {
      $output .= ' itemprop="address" itemscope itemtype="http://schema.org/PostalAddress"';
    }
    if($class) {
      $output .= ' class="' . $class . '"';
    }
    $output .= '>';
    if($icon) {
      if( $external_scripts && in_array('material_icons', $external_scripts) ) {
        $output .= '<i class="material-icons">place</i> ';
      } elseif( $external_scripts && in_array('font_awesome', $external_scripts) ) {
        $output .= '<i class="fa fa-map"></i> ';
      } else {
        $output .= '<svg class="dg dg-location"><use xlink:href="#dg-location"></use></svg> ';
      }
    }
    if($company_name && $name) {
      $output .= '<strong';
      if($schema) {
        $output .= ' itemprop="name"';
      }
      $output .= ' class="company-name">' .  $company_name;
      if($street_line_1 || $street_line_2 || $city || $region || $postcode || $country) {
        $output .= ', ';
      }
      $output .= '</strong>';
      if(!$inline) {
        $output .= '<br>';
      }
    }
    if($street_line_1) {
      $output .= '<span';
      if($schema) {
        $output .= ' itemprop="streetAddress"';
      }
      $output .= ' class="street-line-1">' .  $street_line_1;
      if($street_line_2 || $city || $region || $postcode || $country) {
        $output .= ', ';
      }
      $output .= '</span>';
      if(!$inline) {
        $output .= '<br>';
      }
    }
    if($street_line_2) {
      $output .= '<span';
      if($schema) {
        $output .= ' itemprop="streetAddress"';
      }
      $output .= ' class="street-line-2">' .  $street_line_2;
      if($city || $region || $postcode || $country) {
        $output .= ', ';
      }
      $output .= '</span>';
      if(!$inline) {
        $output .= '<br>';
      }
    }
    if($city) {
      $output .= '<span';
      if($schema) {
        $output .= ' itemprop="addressLocality"';
      }
      $output .= ' class="city">' .  $city;
      if($region || $postcode || $country) {
        $output .= ', ';
      }
      $output .= '</span>';
      if(!$inline) {
        $output .= '<br>';
      }
    }
    if($region) {
      $output .= '<span';
      if($schema) {
        $output .= ' itemprop="addressRegion"';
      }
      $output .= ' class="region">' .  $region;
      if($postcode || $country) {
        $output .= ', ';
      }
      $output .= '</span>';
    }
    if($postcode) {
      $output .= '<span';
      if($schema) {
        $output .= ' itemprop="postalCode"';
      }
      $output .= ' class="postcode">' .  $postcode;
      if($country) {
        $output .= ', ';
      }
      $output .= '</span>';
      if(!$inline) {
        $output .= '<br>';
      }
    }
    if($country) {
      $output .= '<span';
      if($schema) {
        $output .= ' itemprop="addressCountry"';
      }
      $output .= ' class="country">' .  $country . '</span>';
    }
    if($p) {
      $output .= '</p>';
    } else {
      $output .= '</span>';
    }

    if($plain) {
      return $street_line_1 . ' ' . $street_line_2 . ' ' . $city . ' ' . $region . ' ' . $postcode . ' ' . $country;
    } else {
      if($echo) {
        echo $output;
      } else {
        return $output;
      }
    }
  }

}

