<?php

function get_testimonials( $atts = '' ) {

  $testimonials = get_field('testimonials', 'option');
  $i = 0;

  $defaults = array(
    'class'     => '',
    'echo'      => 0,
    'slider'    => 0,
    'featured'  => 0,
    'include'   => '' // i.e. '1,2,5'
  );

  $atts = wp_parse_args( $atts, $defaults );

  $class = $atts['class'];
  $echo = $atts['echo'];
  $feat = $atts['featured'];
  $slider = $atts['slider'];
  $include = $atts['include'];

  if (!empty($include)) {
    $include = explode( ',', $include );
  }

  $output = '';
  if ($testimonials) {
    $output .= '<div class="testimonials';
    if ($class) {
      $output .= ' ' . $class;
    }
    $output .= '">';
    if ($slider) {
      $output .= '<div class="slider-testimonials">';
    }
    foreach ($testimonials as $testimonial) {
      $i++;
      $name = $testimonial['name'];
      $role = $testimonial['role'];
      $location = $testimonial['location'];
      $rating = $testimonial['rating'];
      $photo = $testimonial['photo'];
      $title = $testimonial['title'];
      $date = $testimonial['date'];
      $review = $testimonial['review'];
      $featured = $testimonial['featured'];

      $testimonial_output = '';
      if ($slider) {
        $testimonial_output .= '<div>';
      }
      $testimonial_output .= '<div class="testimonial">';
      if ($photo) {
        $testimonial_output .= '<div class="photo"><img src="' . $photo['url'] . '" alt="' . $photo['alt'] . '"></div>';
      }
      if ($rating) {
        $testimonial_output .= '<div class="rating">';
        if ($rating == 1) {
          $testimonial_output .= '<i class="fa fa-star"></i>';
        }
        elseif ($rating == 1.5) {
          $testimonial_output .= '<i class="fa fa-star"></i><i class="fa fa-star-half"></i>';
        }
        elseif ($rating == 2) {
          $testimonial_output .= '<i class="fa fa-star"></i><i class="fa fa-star"></i>';
        }
        elseif ($rating == 2.5) {
          $testimonial_output .= '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half"></i>';
        }
        elseif ($rating == 3) {
          $testimonial_output .= '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>';
        }
        elseif ($rating == 3.5) {
          $testimonial_output .= '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half"></i>';
        }
        elseif ($rating == 4) {
          $testimonial_output .= '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>';
        }
        elseif ($rating == 4.5) {
          $testimonial_output .= '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half"></i>';
        }
        elseif ($rating == 5) {
          $testimonial_output .= '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>';
        }
        $testimonial_output .= '</div>';
      }
      if ($title) {
        $testimonial_output .= '<h4 class="title">' . $title . '</h4>';
      }
      if ($review) {
        $testimonial_output .= '<div class="review">' . $review . '</div>';
      }
      if ($name || $role || $location) {
        $testimonial_output .= '<div class="author-meta">';
        if ($name) {
          $testimonial_output .= '<span class="name">' . $name . '</span>';
        }
        if ($role) {
          $testimonial_output .= '<span class="role">' . $role . '</span>';
        }
        if ($location) {
          $testimonial_output .= '<span class="location">' . $location . '</span>';
        }
        if ($date) {
          $testimonial_output .= '<span class="date">' . $date . '</span>';
        }
        $testimonial_output .= '</div>';
      }
      $testimonial_output .= '</div>';
      if ($slider) {
        $testimonial_output .= '</div>';
      }

      if ($feat) {
        if ($featured) {
          $output .= $testimonial_output;
        }
      } elseif (!empty($include)) {
        if (in_array($i, $include)) {
          $output .= $testimonial_output;
        }
      } else {
        $output .= $testimonial_output;
      }

    }
    if ($slider) {
      $output .= '</div>';
    }
    $output .= '</div>';

    if ($echo) {
      echo $output;
    } else {
      return $output;
    }
  }

}
