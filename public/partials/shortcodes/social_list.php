<?php

function get_social_list( $atts = '' , $content = null ) {
  $facebook = get_field('facebook', 'option');
  $twitter = get_field('twitter', 'option');
  $google_plus = get_field('google_plus', 'option');
  $instagram = get_field('instagram', 'option');
  $linkedin = get_field('linkedin', 'option');
  $pinterest = get_field('pinterest', 'option');
  $yelp = get_field('yelp', 'option');
  $youtube = get_field('youtube', 'option');

  $defaults = array(
    'class' => '',
    'inline' => 1,
    'echo' => 0,
  );

  $atts = wp_parse_args( $atts, $defaults );

  $external_scripts = get_field('admin_setup_external_scripts', 'option');

  $class = $atts['class'];
  $inline = $atts['inline'];
  $echo = $atts['echo'];


  $output  = '';
  $output .= '<ul class="social-list';
  if($class){
    $output .= ' ' . $class;
  }
  if($inline){
    $output .= ' is-inline';
  }
  $output .= '">';
  if( $external_scripts && in_array('font_awesome', $external_scripts) ) {
    if($facebook){
      $output .= '<li><a href="' . $facebook . '" target="_blank" class="icon-facebook"><i class="fa fa-facebook"></i> <span>Facebook</span></a></li>';
    }
    if($twitter){
      $output .= '<li><a href="' . $twitter . '" target="_blank" class="icon-twitter"><i class="fa fa-twitter"></i> <span>Twitter</span></a></li>';
    }
    if($google_plus){
      $output .= '<li><a href="' . $google_plus . '" target="_blank" class="icon-google-plus"><i class="fa fa-google-plus"></i> <span>Google +</span></a></li>';
    }
    if($instagram){
      $output .= '<li><a href="' . $instagram . '" target="_blank" class="icon-instagram"><i class="fa fa-instagram"></i> <span>Instagram</span></a></li>';
    }
    if($linkedin){
      $output .= '<li><a href="' . $linkedin . '" target="_blank" class="icon-linkedin"><i class="fa fa-linkedin"></i> <span>Linkedin</span></a></li>';
    }
    if($pinterest){
      $output .= '<li><a href="' . $pinterest . '" target="_blank" class="icon-pinterest"><i class="fa fa-pinterest"></i> <span>Pinterest</span></a></li>';
    }
    if($yelp){
      $output .= '<li><a href="' . $yelp . '" target="_blank" class="icon-yelp"><i class="fa fa-yelp"></i> <span>Yelp</span></a></li>';
    }
    if($youtube){
      $output .= '<li><a href="' . $youtube . '" target="_blank" class="icon-youtube"><i class="fa fa-youtube"></i> <span>Youtube</span></a></li>';
    }
  } else {
    if($facebook){
      $output .= '<li><a href="' . $facebook . '" target="_blank" class="icon-facebook"><i><svg class="dg dg-facebook"><use xlink:href="#dg-facebook"></use></svg></i> <span>Facebook</span></a></li>';
    }
    if($twitter){
      $output .= '<li><a href="' . $twitter . '" target="_blank" class="icon-twitter"><i><svg class="dg dg-twitter"><use xlink:href="#dg-twitter"></use></svg></i> <span>Twitter</span></a></li>';
    }
    if($google_plus){
      $output .= '<li><a href="' . $google_plus . '" target="_blank" class="icon-google-plus"><i><svg class="dg dg-google-plus"><use xlink:href="#dg-google-plus"></use></svg></i> <span>Google +</span></a></li>';
    }
    if($instagram){
      $output .= '<li><a href="' . $instagram . '" target="_blank" class="icon-instagram"><i><svg class="dg dg-instagram"><use xlink:href="#dg-instagram"></use></svg></i> <span>Instagram</span></a></li>';
    }
    if($linkedin){
      $output .= '<li><a href="' . $linkedin . '" target="_blank" class="icon-linkedin"><i><svg class="dg dg-linkedin"><use xlink:href="#dg-linkedin"></use></svg></i> <span>Linkedin</span></a></li>';
    }
    if($pinterest){
      $output .= '<li><a href="' . $pinterest . '" target="_blank" class="icon-pinterest"><i><svg class="dg dg-pinterest"><use xlink:href="#dg-pinterest"></use></svg></i> <span>Pinterest</span></a></li>';
    }
    if($yelp){
      $output .= '<li><a href="' . $yelp . '" target="_blank" class="icon-yelp"><i><svg class="dg dg-yelp"><use xlink:href="#dg-yelp"></use></svg></i> <span>Yelp</span></a></li>';
    }
    if($youtube){
      $output .= '<li><a href="' . $youtube . '" target="_blank" class="icon-youtube"><i><svg class="dg dg-youtube"><use xlink:href="#dg-youtube"></use></svg></i> <span>Youtube</span></a></li>';
    }
  }
  $output .= '</ul>';

  if ($echo) {
    echo $output;
  } else {
    return $output;
  }
}