<?php

function get_sharelinks() {
  return '
<ul class="sharelinks">
  <li class="sharelink sharelink-title">Share</li>
  <li class="sharelink sharelink-icon"><a target="_blank" href="https://www.facebook.com/sharer.php?t=' . urlencode(get_the_title() . ' – ' . get_bloginfo('name')) . '&u=' . urlencode(get_the_permalink()) . '"><i class="fa fa-facebook"></i></a></li>
  <li class="sharelink sharelink-icon"><a target="_blank" href="https://twitter.com/intent/tweet?text=' . urlencode(get_the_title() . ' - ' . get_bloginfo('name')) . '&url=' . urlencode(get_the_permalink()) . '"><i class="fa fa-twitter"></i></a></li>
  <li class="sharelink sharelink-icon"><a target="_blank" href="https://plus.google.com/share?url=' . urlencode(get_the_permalink()) . '"><i class="fa fa-google-plus"></i></a></li>
</ul>
';
}