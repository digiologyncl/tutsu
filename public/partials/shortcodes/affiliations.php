<?php

function get_affiliations( $atts = '' ) {
  $affiliations = get_field('affiliations', 'option');

  $defaults = array(
    'echo'   => 0,
    'slider'   => 1,
    'include'   => '' // i.e. '1,2,5'
  );

  $atts = wp_parse_args( $atts, $defaults );

  $echo = $atts['echo'];
  $slides = $atts['slider'];
  $include = $atts['include'];

  if (!empty($include)) {
    $include = explode( ',', $include );
  }

  $i = 0;
  define("affiliations_slides", round($slides));

  $output  = '';
  $affiliation_output = '';

  if($affiliations) {
    $output .= '<div class="affiliations">';
    if($slides != 0) {
      $output .= '<div class="slider-affiliations">';
    }
    foreach ($affiliations as $affiliation) {
      $i++;

      if (!empty($include)) {

        if (in_array($i, $include)) {

          if($slides != 0) {
            $output .= '<div>';
          }
          $output .= '<div class="affiliation">';
          $output .= '<img src="' . $affiliation['url'] . '" alt="' . $affiliation['alt'] . '">';
          $output .= '<div class="title">' . $affiliation['title'] . '</div>';
          $output .= '<div class="caption">' . $affiliation['caption'] . '</div>';
          $output .= '</div>';
          if($slides != 0) {
            $output .= '</div>';
          }

        }

      } else {

        if($slides != 0) {
          $output .= '<div>';
        }
        $output .= '<div class="affiliation">';
        $output .= '<img src="' . $affiliation['url'] . '" alt="' . $affiliation['alt'] . '">';
        $output .= '<div class="title">' . $affiliation['title'] . '</div>';
        $output .= '<div class="caption">' . $affiliation['caption'] . '</div>';
        $output .= '</div>';
        if($slides != 0) {
          $output .= '</div>';
        }

      }
    }

    if($slides != 0) {
      $output .= '</div>';
    }
    $output .= '</div>';

    if($echo){
      echo $output;
    } else {
      return $output;
    }
  }
}

