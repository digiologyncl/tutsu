<?php

function get_tweets( $atts = '' , $content = null ) {

  $tweets = tutsu_twitter_api_returnTweet();

  $output = '';
  $output .= '<div class="tweets">';

// loop all the tweets and display each of them
  foreach($tweets as $tweet){

    // https://www.scribd.com/doc/30146338/map-of-a-tweet

    $output .= '<div class="tweet-wrapper">';
    $output .= '<div class="tweet">';
    $output .= '<div class="tweet-content">';
    $output .= '<div class="tweet-icon"><a href="https://twitter.com/' . $tweet["user"]["screen_name"] . '/status/' . $tweet["id"] . '" target="_blank"><i class="fa fa-twitter"></i></a></div>';
    $output .= '<div class="tweet-username"><a href="' . $tweet["user"]["url"] . '" target="_blank">' . $tweet["user"]["screen_name"] . '</a></div>';
    $output .= '<div class="tweet-date">' . date(get_option('date_format'), strtotime($tweet["created_at"])) . '</div>';
    $output .= '<div class="tweet-text">' . preg_replace('!(http|ftp|scp)(s)?:\/\/[a-zA-Z0-9.?&_/]+!', "<a href=\"\\0\" target=\"_blank\">\\0</a>", $tweet["text"]) . '</div>';
    $output .= '<div class="tweet-link"><a href="https://twitter.com/' . $tweet["user"]["screen_name"] . '" target="_blank">Follow us</a></div>';
    $output .= '</div>';
    $output .= '</div>';
    $output .= '</div>';
  }

  $output .= '</div>';

  return $output;
}