<?php

function get_cols( $atts = '' , $content = null ) {
  $defaults = array(
    'class' => '',
  );

  $atts = wp_parse_args( $atts, $defaults );

  $class = $atts['class'];

  $output  = '<div class="row';
  if($class) {
    $output .= ' ' . $class;
  }
  $output .= '">' . do_shortcode( $content ) . '</div>';

  return $output;
}