<?php

function get_mobile( $atts = '', $content = null ) {
  $mobile = get_field('mobile', 'options');
  $mobile_slug = str_replace(' ', '', $mobile);

  $defaults = array(
    'class' => 'mobile',
    'link'  => 1,
    'icon'  => 0,
    'p'     => 0,
    'pre'   => 0,
    'echo'  => 0
  );

  $atts = wp_parse_args( $atts, $defaults );

  $external_scripts = get_field('admin_setup_external_scripts', 'option');

  $class  = $atts['class'];
  $link   = $atts['link'];
  $icon   = $atts['icon'];
  $p      = $atts['p'];
  $pre   = $atts['pre'];
  $echo   = $atts['echo'];

  $output = '';
  if ($mobile) {
    if($p) {
      $output .='<p>';
    }
    if ($link) {
      $output .='<a href="tel:' . $mobile_slug . '"';
    } else {
      $output .= '<span';
    }
    $output .= ' class="mobile';
    if($class) {
      $output .= ' ' . $class;
    }
    $output   .= '">';
    if($icon) {
      if( $external_scripts && in_array('material_icons', $external_scripts) ) {
        $output .= '<i class="material-icons">phone_android</i> ';
      } elseif( $external_scripts && in_array('font_awesome', $external_scripts) ) {
        $output .= '<i class="fa fa-mobile"></i> ';
      } else {
        $output .= '<svg class="dg dg-mobile"><use xlink:href="#dg-mobile"></use></svg> ';
      }
    }
    if($pre && !$icon) {
      $output .= '<strong>' . $pre . '</strong> ';
    }
    $output   .= $mobile;
    if ($link) {
      $output .= '</a>';
    } else {
      $output .= '</span>';
    }
    if($icon && !$p) {
      $output .='</br>';
    }
    if($pre && !$p) {
      $output .='</br>';
    }
    if($p) {
      $output .='</p>';
    }

    if ($echo) {
      echo $output;
    } else {
      return $output;
    }
  }
}