<?php
function get_latest_posts( $atts = '' ) {
  $defaults = array(
    'image_size' => 'medium',
    'pre_date' => 'Posted on ',
    'link_class' => 'btn',
    'link_text' => 'Read more',
  );

  $atts = wp_parse_args( $atts, $defaults );

  $image_size = $atts['image_size'];
  $pre_date = $atts['pre_date'];
  $link_class = $atts['link_class'];
  $link_text = $atts['link_text'];

  $args = array(
    'post_type' => 'post',
  );

  $output = '';

  $the_query = new WP_Query( $args );
  if ( $the_query->have_posts() ) :
  $output .= '<div class="latest-posts">';
    while ( $the_query->have_posts() ) : $the_query->the_post();

      $output .= '<div class="latest-post">';
      $output .= '<div class="inner">';

      $output .= '<div class="meta">';
      $output .= '<div class="image-wrapper"><a href="' . get_permalink() . '">' . get_the_post_thumbnail($page->ID, $image_size) . '</a></div>';
      $output .= '<div class="text">';
      $output .= '<h5 class="title"><a href="' . get_permalink() . '">' . get_the_title() . '</a></h5>';
      $output .= '<p class="date">';
      if($pre_date){
        $output .= $pre_date . ' ';
      }
      $output .= get_the_date() . '</p>';
      $output .= '<p class="excerpt">' . get_the_excerpt() . '</p>';
      if($link_text){
        $output .= '<div class="permalink"><a href="' . get_permalink() . '" class="' . $link_class . '">' . $link_text . '</a></div>';
      }
      $output .= '</div>';
      $output .= '</div>';

      $output .= '</div>';
      $output .= '</div>';
    endwhile;
    wp_reset_postdata();
  $output .= '</div>';

  return $output;
  endif;
}
