<?php

function get_map( $atts = '', $content = null ) {

  $defaults = array(
    'class' => '',
    'width' => '',
    'echo'  => 0,
    'id'  => 'map',
    'map' => 1
  );

  $atts = wp_parse_args( $atts, $defaults );

  $class = $atts['class'];
  $width = $atts['width'];
  $echo = $atts['echo'];
  $id = $atts['id'];
  $map = $atts['map'];

  $output = '';
  if( have_rows('maps', 'options') ):
    $i = 0;
    while ( have_rows('maps', 'options') ) : the_row();
      $i++;
      if($i == $map) {
        if($width){
          $output  .= '<div style="max-width:' . $width . 'px">';
        }
        if( have_rows('locations', 'options') ):
          $output .= '<div class="embed-container map acf-map " id="' . $id . '">';
          while ( have_rows('locations', 'options') ) : the_row();

            $location = get_sub_field('location', 'options');

            $output .= '<div class="marker" data-lat="' . $location['lat'] . '" data-lng="' . $location['lng'] . '">';
            if(get_sub_field('title')){
              $output .= '<h4>' . get_sub_field('title') . '</h4>';
            }
            $output .= '<p class="address"><strong>Address:</strong><br>' . $location['address'] . '</p>';
            if(get_sub_field('description')){
              $output .=  apply_filters('the_content', get_sub_field('description'));
            }
            $output .= '</div>';
          endwhile;
          $output .= '</div>';
        endif;
        if($width){
          $output .= '</div>';
        }
      }
    endwhile;
  endif;

  if($echo){
    echo $output;
  } else {
    return $output;
  }
}
