<?php

function get_show_posts( $atts = '' ) {
  $defaults = array(
    'class' => 'posts',
    'child_of' => '',
    'exclude' => '',
    'name' => '',
    'posts_per_page' => '',
    'order' => 'DESC',
    'orderby' => 'date',
    'post_type' => 'post',
    'metakey' => '',
    'metavalue' => '',
    'metatype' => '',
    'tag' => '',
    'container_class' => '',
    'item_container_class' => '',
    'item_class' => '',
    'show_image' => 1,
    'show_icon' => 1,
    'show_title' => 1,
    'show_subtitle' => 1,
    'show_date' => 0,
    'show_excerpt' => 1,
    'show_content' => 0,
    'show_categories' => 0,
    'show_venue' => 0,
    'show_startdate' => 0,
    'show_time' => 0,
    'link_class' => '',
    'link_text' => 'Read More',
    'show_share' => 1,
    'show_header' => 1,
    'show_body' => 1,
    'show_footer' => 1,
    'image_size' => 'large',
    'custom_url' => 0
  );

  $atts = wp_parse_args( $atts, $defaults );

  $class = $atts['class'];
  $child_of = $atts['child_of'];
  $exclude = array($atts['exclude']);
  $name = $atts['name'];
  $post_type = $atts['post_type'];
  $order = $atts['order'];
  $orderby = $atts['orderby'];
  $posts_per_page = $atts['posts_per_page'];
  $metakey = $atts['metakey'];
  $metavalue = $atts['metavalue'];
  $metatype = $atts['metatype'];
  $tag = $atts['tag'];
  $container_class = $atts['container_class'];
  $item_container_class = $atts['item_container_class'];
  $item_class = $atts['item_class'];
  $show_image = $atts['show_image'];
  $show_icon = $atts['show_icon'];
  $show_title = $atts['show_title'];
  $show_subtitle = $atts['show_subtitle'];
  $show_date = $atts['show_date'];
  $show_excerpt = $atts['show_excerpt'];
  $show_content = $atts['show_content'];
  $show_categories = $atts['show_categories'];
  $show_venue = $atts['show_venue'];
  $show_startdate = $atts['show_startdate'];
  $show_time = $atts['show_time'];
  $link_class = $atts['link_class'];
  $link_text = $atts['link_text'];
  $show_share = $atts['show_share'];
  $show_header = $atts['show_header'];
  $show_body = $atts['show_body'];
  $show_footer = $atts['show_footer'];
  $image_size = $atts['image_size'];
  $custom_url = $atts['custom_url'];

  $current_id = get_the_ID();

  $args = array(
    'post_type' => $post_type,
    'post_parent' => $child_of,
    'pexclude' => $exclude,
    'posts_per_page' => $posts_per_page,
    'order' => $order,
    'orderby' => $orderby,
    'tag' => $tag,
    'name' => $name,
  );

  if($metakey || $metavalue || $metatype){
    $metaargs = array(
      'meta_key' => $metakey,
      'meta_value' => $metavalue,
      'meta_type' => $metatype,
    );

    $args = array_merge($args, $metaargs);
  }



  $posts = get_posts( $args );

  $output = '';

  if ( $posts ) {

    if($class){
      $output  .= '<div class="' . $class . '">';
    }
    if($container_class){
      $output  .= '<div class="' . $container_class . '">';
    }

    foreach( $posts as $p ) {
      $image = get_the_post_thumbnail_url($p->ID, $image_size);
      $title = get_post($p->ID)->post_title;
      $subtitle = get_field('subtitle', $p->ID);
      $date = date(get_option('date_format'), strtotime(get_post($p->ID)->post_date));
      $excerpt = get_post($p->ID)->post_excerpt;
      $content = get_post($p->ID)->post_content;
      if($content){
        $content = str_replace(']]>', ']]&gt;', apply_filters('the_content', $content));
      }
      $permalink = get_the_permalink($p->ID);
      $venue = get_field('venue', $p->ID);
      $startdate = get_field('time', $p->ID);
      $time = get_field('time', $p->ID);
      if($startdate){
        $startdate = date(get_option('date_format'), strtotime($startdate));
      }
      if($time){
        $time = date(get_option('time_format'), strtotime($time));
      }
      $icon = get_field('featured_icon', $p->ID);
      $categories = get_the_category( $p->ID );

      if($item_class){
        $post_type = $item_class;
      }
      if($item_container_class){
        $output .= '<div class="' . $item_container_class . '">';
      }
      $output .= '<div class="' . $post_type;
      if($current_id == $p->ID){
        $output .= ' is-current';
      }
      if($categories){
        foreach ($categories as $category) {
          $output .= ' category-' . $category->slug;
        }
      }
      $output .= '">';
      if($show_image){
        $output .= '<div class="' . $post_type . '-image-wrapper"><a href="' . $permalink . '"><span class="' . $post_type . '-image" style="background-image:url(' . $image . ')"></span></a></div>';
      }
      $output .= '<div class="' . $post_type . '-inner">';
      if($show_icon && $icon){
        $output .= '<div class="' . $post_type . '-icon"><a href="' . $permalink . '"><img src="' . $icon['url'] . '" alt="' . $icon['alt'] . '" /></a></div>';
      }
      $output .= '<div class="' . $post_type . '-text">';
      if($show_header){
        $output .= '<div class="' . $post_type . '-header">';
        if($show_title){
          $output .= '<h4 class="' . $post_type . '-title">' . $title . '</h4>';
        }
        if($show_subtitle && $subtitle){
          $output .= '<div class="' . $post_type . '-subtitle">' . $subtitle . '</div>';
        }
        if($show_date){
          $output .= '<div class="' . $post_type . '-date">' . $date . '</div>';
        }
        if($show_categories && $categories){
          $output .= '<div class="' . $post_type . '-categories">';
          foreach ($categories as $category) {
            $output .= '<span>' . $category->name . '</span>';
          }
          $output .= '</div>';
        }
        if($show_venue && $venue){
          $output .= '<div class="' . $post_type . '-venue">' . $venue . '</div>';
        }
        if($show_startdate && $startdate){
          $output .= '<div class="' . $post_type . '-startdate">' . $startdate . '</div>';
        }
        if($show_time && $time){
          $output .= '<div class="' . $post_type . '-time">' . $time . '</div>';
        }
        $output .= '</div>';
      }
      if($show_body){
        if($content || $excerpt){
          $output .= '<div class="' . $post_type . '-body">';
          if($show_excerpt && !$show_content && $excerpt){
            $output .= '<div class="' . $post_type . '-excerpt">' . $excerpt . '</div>';
          }
          if($show_content && $content){
            $output .= '<div class="' . $post_type . '-content">' . $content . '</div>';
          }
          $output .= '</div>';
        }
      }
      if($show_footer){
        $output .= '<div class="' . $post_type . '-footer">';
        if($link_text){
          $output .= '<div class="' . $post_type . '-permalink">';
          if($custom_url){
            $output .= '<a href="' . $custom_url . '" class="' . $link_class . '" target="_blank">' . $link_text . '</a>';
          } else {
            $output .= '<a href="' . $permalink . '" class="' . $link_class . '">' . $link_text . '</a>';
          }
          $output .= '</div>';
        }
        if($show_share && !$post_type == 'page'){
          $output .= '<div class="' . $post_type . '-share">' . get_sharelinks() . '</div>';
        }
        $output .= '</div>';
      }
      $output .= '</div>';
      $output .= '</div>';
      $output .= '</div>';

      if($item_container_class){
        $output .= '</div>';
      }

    }
    if($class){
      $output .= '</div>';
    }
    if($container_class){
      $output .= '</div>';
    }

  }

  return $output;
  print_r($atts);

}