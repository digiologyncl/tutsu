<?php

function get_faqs( $atts = '' , $content = null ) {

  $defaults = array(
    'class' => '',
    'width' => '',
    'accordion' => 1,
  );


  $atts = wp_parse_args( $atts, $defaults );

  $class = $atts['class'];
  $width = $atts['width'];
  $accordion = $atts['accordion'];

  $output   = '';
  $output  .= '<div class="faqs-wrapper';
  if($class) {
    $output .= ' ' . $class;
  }
  if($accordion) {
    $output .= ' is-accordion';
  } else {
    $output .= ' not-accordion';
  }
  if($width){
    $output  .= '" style="max-width:' . $width . 'px';
  }
  $output .= '">' . do_shortcode( $content ) . '</div>';

  return $output;
}