<?php

function get_subpages( $atts = '' ) {
  $defaults = array(
    'child_of' => '',
    'exclude' => '',
    'posts_per_page' => '',
    'container_class' => '',
    'item_class' => 'col-sm-6 col-lg-4',
    'show_featured_image' => 1,
    'show_title' => 1,
    'show_excerpt' => 0,
    'link_class' => '',
    'link_text' => 'Read More'
  );

  $atts = wp_parse_args( $atts, $defaults );

  $child_of = $atts['child_of'];
  $exclude = array($atts['exclude']);
  $posts_per_page = $atts['posts_per_page'];
  $container_class = $atts['container_class'];
  $item_class = $atts['item_class'];
  $show_featured_image = $atts['show_featured_image'];
  $show_title = $atts['show_title'];
  $show_excerpt = $atts['show_excerpt'];
  $link_class = $atts['link_class'];
  $link_text = $atts['link_text'];

  $args = array(
    'post_type' => 'page',
    'post_parent' => $child_of,
    'post__not_in' => $exclude,
    'posts_per_page' => $posts_per_page,
  );

  $subpages_query = new WP_Query( $args );

  $output = '';

  if ( $subpages_query->have_posts() ) {

    $output  .= '<div class="subpages">';
    $output  .= '<div class="row ';
    if($container_class){
      $output  .= ' ' . $container_class;
    }
    $output .= '">';

    while ( $subpages_query->have_posts() ) {
      $subpages_query->the_post();
      $image = get_the_post_thumbnail_url();
      $title = get_the_title();
      $excerpt = get_the_excerpt();
      $permalink = get_the_permalink();

      $output .= '<div class="col-xs-12';
      if($item_class){
        $output  .= ' ' . $item_class;
      }
      $output .= '">';
      $output .= '<div class="subpage">';
      if($show_featured_image && $image){
        $output .= '<div class="subpage-image" style="background-image:url(' . $image . ')"></div>';
      }
      $output .= '<div class="subpage-inner">';
      $output .= '<div class="subpage-content">';
      if($show_title){
        $output .= '<h4 class="subpage-title">' . $title . '</h4>';
      }
      if($show_excerpt){
        $output .= '<div class="subpage-excerpt">' . $excerpt . '</div>';
      }
      $output .= '<div class="subpage-permalink"><a href="' . $permalink . '" class="' . $link_class . '">' . $link_text . '</a></div>';
      $output .= '</div>';
      $output .= '</div>';
      $output .= '</div>';
      $output .= '</div>';
    }
    $output .= '</div>';
    $output .= '</div>';

    wp_reset_postdata();
  }


  return $output;
}