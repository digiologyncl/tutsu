<?php

function get_lead( $atts = '' , $content = null ) {
  $defaults = array(
    'class' => '',
    'p' => 1,
  );

  $atts = wp_parse_args( $atts, $defaults );

  $class = $atts['class'];
  $p = $atts['p'];

  $output = '';
  if($p) {
    $output .= '<p class="lead';
  } else {
    $output .= '<span class="lead';
  }
  if($class) {
    $output .= ' ' . $class;
  }
  $output .= '">' . do_shortcode( $content );
  if($p) {
    $output .= '</p>';
  } else {
    $output .= '</span>';
  }
  return $output;
}