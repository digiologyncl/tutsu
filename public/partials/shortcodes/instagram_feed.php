<?php

function get_instagram_feed( $atts = '' , $content = null ) {

  $defaults = array(
    'show_header'  => 0,
    'limit'  => 12,
  );

  $atts = wp_parse_args( $atts, $defaults );

  $show_header   = $atts['show_header'];
  $limit = $atts['limit'];

  $instagrams = tutsu_instagram_api();

  $images = $instagrams[user][media][nodes];

  $output = '';
  $output .= '<div class="instagram-feed">';
  if($show_header){
    $output .= '<div class="instagram-header">';
    $output .= '<div class="instagram-avatar"><img src="' . $instagrams[user][profile_pic_url] . '" alt="" /></div>';
    $output .= '<div class="instagram-followers">' . $instagrams[user][followed_by][count] . '</div>';
    $output .= '<div class="instagram-biography">' . $instagrams[user][biography] . '</div>';
    $output .= '<div class="instagram-name">' . $instagrams[user][full_name] . '</div>';
    $output .= '<div class="instagram-username">' . $instagrams[user][username] . '</div>';
    $output .= '</div>';
  }
  $output .= '<div class="instagram-images">';

  // loop all the instagrams and display each of them
  $i = -1;
  foreach($images as $image){
    $i++;
    // $height = $image[dimensions][height];
    // $width = $image[dimensions][width];
    // $likes = $image[likes][count];
    // $is_video = $image[is_video];
    // $video_views = $image[video_views];
    $date = date(get_option('date_format'), $image[date]);

    if($i < $limit) {

      $output .= '<div class="instagram-image">';
      $output .= '<a href="' . $image[display_src] . '" data-lightbox="instagram-gallery" data-title="' . $date . ' - ' . $image[caption] . '">';
      $output .= '<img src="' . $image[thumbnail_src] . '">';
      $output .= '</a>';
      $output .= '</div>';
    }
  }

  $output .= '</div>';
  $output .= '</div>';

  return $output;

}