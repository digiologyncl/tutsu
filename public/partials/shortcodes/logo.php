<?php

function get_logo( $atts = '' , $content = null ) {
  $site_logo = get_field('site_logo', 'option');
  $defaults = array(
    'class' => '',
  );

  $atts = wp_parse_args( $atts, $defaults );

  $class = $atts['class'];

  $output  = '';
  $output  .= '<a href="' . esc_url( home_url() ) . '" class="logo';
  if($class) {
    $output .= ' ' . $class;
  }
  $output .= '">';
  if($site_logo){
    $output .= '<img src="' . $site_logo['url'] . '" alt="' . get_bloginfo('name') . '">';
  } else {
    $output .= '<svg><use xlink:href="#dg-logo" /></svg>';
  }
  $output .= '</a>';

  return $output;
}