<?php

function get_staff( $atts = '' ) {
  $team_members = get_field('team_members', 'option');

  $defaults = array(
    'class'    => '',
    'featured'    => 0,
    'not_featured'    => 0,
    'echo'    => 0,
  );

  $atts = wp_parse_args( $atts, $defaults );

  $class = $atts['class'];
  $featured_only = $atts['featured'];
  $not_featured_only = $atts['not_featured'];
  $echo = $atts['echo'];

  $output = '';

  if($team_members) {
    $output .= '<div class="team-members';
    if($class) {
      $output .= ' ' . $class;
    }
    $output .= '">';

    foreach ($team_members as $team_member) {
      $class_ind = $team_member['class'];
      $id_ind = $team_member['id'];
      $name = $team_member['name'];
      $role = $team_member['role'];
      $photo = $team_member['photo'];
      $bio = $team_member['bio'];
      $phone = $team_member['phone'];
      $email = $team_member['email'];
      $twitter = $team_member['twitter'];
      $twitter_username = str_replace('/', '', str_replace('https://twitter.com/', '@', $team_member['twitter']));
      $linkedin = $team_member['linkedin'];
      $is_featured = $team_member['featured'];

      $team_member_output = '';
      $team_member_output .= '<div class="team-member';
      if($is_featured) {
        $team_member_output .= ' is-featured';
      }
      if($class_ind) {
        $team_member_output .= ' ' . $class_ind;
      }
      $team_member_output .= '"';
      if($id_ind) {
        $team_member_output .= ' id="' . $id_ind . '"';
      }
      $team_member_output .= '>';
      $team_member_output .= '<div class="team-member-inner">';
      if($photo) {
        $team_member_output .= '<div class="photo"><img src="'. $photo['url'] .'" alt="' . $photo['alt'] . '"></div>';
      }
      $team_member_output .= '<div class="text">';
      $team_member_output .= '<div class="meta">';
      if($name) {
        $team_member_output .= '<h4 class="name">' . $name . '</h4>';
      }
      if($role) {
        $team_member_output .= '<div class="role">' . $role . '</div>';
      }
      if($phone) {
        $team_member_output .= '<div class="phone"><a href="tel:' . str_replace(' ', '', $phone) . '">' . $phone . '</a></div>';
      }
      if($email) {
        $team_member_output .= '<div class="email"><a href="mailto:' . $email . '">' . $email . '</a></div>';
      }
      if($twitter) {
        $team_member_output .= '<a href="' . $twitter . '" class="twitter" target="_blank"><i class="fa fa-twitter"></i> <span>' . $twitter_username . '</span></a>';
      }
      if($linkedin) {
        $team_member_output .= '<a href="' . $linkedin . '" class="linkedin" target="_blank"><i class="fa fa-linkedin"></i></a>';
      }
      $team_member_output .= '</div>';
      if($bio) {
        $team_member_output .= '<div class="bio">' . $bio . '</div>';
      }
      $team_member_output .= '</div>';
      $team_member_output .= '</div>';
      $team_member_output .= '</div>';

      if($featured_only){
        if($is_featured){
          $output .= $team_member_output;
        } else {
          $output .= '';
        }
      }
      elseif($not_featured_only){
        if($is_featured){
          $output .= '';
        } else {
          $output .= $team_member_output;
        }
      }
      else {
        $output .= $team_member_output;
      }

    }

    $output .= '</div>';
  }

  if($echo){
    echo $output;
  } else {
    return $output;
  }

}
