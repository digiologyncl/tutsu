<?php

function get_tabs( $atts = '' , $content = null ) {

  $defaults = array(
    'class' => '',
    'titles' => array(),
    'width' => '',
    'type' => 'tabs',
  );

  $atts = wp_parse_args( $atts, $defaults );

  $class = $atts['class'];
  $titles = $atts['titles'];
  $titles = explode( ',', $titles );
  $width = $atts['width'];
  $type = $atts['type'];

  $output   = '';
  $output  .= '<div class="tabs-wrapper';
  if($class) {
    $output .= ' ' . $class;
  }
  if($width){
    $output  .= '" style="max-width:' . $width . 'px';
  }
  $output .= '">';
  if($type == 'select') {
    $output  .= '<select class="tabs form-control">';
    $i = 0;
    foreach ($titles as $title) {
      $output  .= '<option value="tab_' . str_replace(' ', '', strtolower($title)) . '">' . $title . '</option>';
    }
    $output  .= '</select>';
  } else {
    $output  .= '<ul class="tabs">';
    $i = 0;
    foreach ($titles as $title) {
      $i++;
      $output  .= '<li>';
      $output  .= '<a href="#tab_' . str_replace(' ', '', strtolower($title));
      if($i == 1){
        $output  .= '" class="active';
      }
      $output  .= '">' . $title . '</a>';
      $output  .= '</li>';
    }
    $output  .= '</ul>';
  }
  $output  .= '<div class="panels">' . do_shortcode( $content ) . '</div>';
  $output  .= '</div>';

  return $output;
}

