<?php

function get_digicon( $atts = '' , $content = null ) {

	$defaults = array(
    'icon'  => '',
    'class' => ''
  );

  $atts = wp_parse_args( $atts, $defaults );

  $icon = $atts['icon'];
  $class = $atts['class'];

  $output = '';

  $output .= '<svg class="dg dg-' . $icon;
  if($class){
    $output .= ' ' . $class;
  }
  $output .= '"><use xlink:href="#dg-' . $icon . '" /></svg>';

  return $output;
}