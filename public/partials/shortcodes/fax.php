<?php

function get_fax( $atts = '', $content = null ) {
  $fax = get_field('fax', 'options');
  $fax_slug = str_replace(' ', '', $fax);

  $defaults = array(
    'class' => '',
    'link'  => 1,
    'icon'  => 0,
    'p'     => 0,
    'pre'   => 0,
    'echo'  => 0
  );

  $atts = wp_parse_args( $atts, $defaults );

  $class  = $atts['class'];
  $link   = $atts['link'];
  $icon   = $atts['icon'];
  $p      = $atts['p'];
  $pre   = $atts['pre'];
  $echo   = $atts['echo'];

  $output = '';
  if ($fax) {
    if($p) {
      $output .='<p>';
    }
    if ($link) {
      $output .='<a href="tel:' . $fax_slug . '"';
    } else {
      $output .= '<span';
    }
    $output .= ' class="fax';
    if($class) {
      $output .= ' ' . $class;
    }
    $output   .= '">';
    if($icon) {
      if( $external_scripts && in_array('material_icons', $external_scripts) ) {
        $output .= '<i class="material-icons">print</i> ';
      } elseif( $external_scripts && in_array('font_awesome', $external_scripts) ) {
        $output .= '<i class="fa fa-fax"></i> ';
      } else {
        $output .= '<svg class="dg dg-fax"><use xlink:href="#dg-fax"></use></svg> ';
      }
    }
    if($pre && !$icon) {
      $output .= '<strong>' . $pre . '</strong> ';
    }
    $output   .= $fax;
    if ($link) {
      $output .= '</a>';
    } else {
      $output .= '</span>';
    }
    if($icon && !$p) {
      $output .='</br>';
    }
    if($pre && !$p) {
      $output .='</br>';
    }
    if($p) {
      $output .='</p>';
    }

    if ($echo) {
      echo $output;
    } else {
      return $output;
    }
  }
}