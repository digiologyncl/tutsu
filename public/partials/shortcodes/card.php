<?php

function get_card( $atts = '' , $content = null ) {
  $defaults = array(
    'class' => '',
    'id' => '',
    'title' => '',
    'width' => '',
    'icon' => '',
    'digicon' => '',
  );

  $atts = wp_parse_args( $atts, $defaults );

  $external_scripts = get_field('admin_setup_external_scripts', 'option');

  $class = $atts['class'];
  $id = $atts['id'];
  $title = $atts['title'];
  $width = $atts['width'];
  $icon = $atts['icon'];
  $digicon = $atts['digicon'];

  $output  = '<div class="card';
  if($class){
	  $output  .= ' ' . $class;
  }
  if($width){
    $output  .= '" style="max-width:' . $width . 'px';
  }
  $output .= '"';
  if($id){
    $output  .= ' id="' . $id . '"';
  }
  $output .= '>';
  if($title){
    $output  .= '<div class="card-title">';
    if($icon) {
      $output  .= '<span class="card-icon">';
      if( $external_scripts && in_array('material_icons', $external_scripts) ) {
        $output .= '<i class="material-icons">' . $icon . '</i> ';
      } elseif( $external_scripts && in_array('font_awesome', $external_scripts) ) {
        $output .= '<i class="fa fa-' . $icon . '"></i> ';
      } else {
        $output .= '<svg class="dg dg-' . $icon . '"><use xlink:href="#dg-' . $icon . '"></use></svg> ';
      }
      $output  .= '</span>';
    }
    if($digicon) {
      $output  .= '<span class="card-icon">';
      $output .= '<svg class="dg dg-' . $digicon . '"><use xlink:href="#dg-' . $digicon . '"></use></svg> ';
      $output  .= '</span>';
    }
    $output  .= $title . '</div>';
  }
  $output .= '<div class="card-content">' . do_shortcode( $content ) . '</div>';
  $output .= '</div>';

  return $output;
}