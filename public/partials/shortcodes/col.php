<?php

function get_col( $atts = '' , $content = null ) {
	$defaults = array(
    'class' => '',
    'xs' => '12',
    'sm' => 0,
    'md' => 0,
    'lg' => 0,
    'xl' => 0,
    'xs-o' => 0,
    'sm-o' => 0,
    'md-o' => 0,
    'lg-o' => 0,
    'xl-o' => 0,
  );

  $atts = wp_parse_args( $atts, $defaults );

  $class = $atts['class'];
  $xs = $atts['xs'];
  $sm = $atts['sm'];
  $md = $atts['md'];
  $lg = $atts['lg'];
  $xl = $atts['xl'];
  $xs_o = $atts['xs-o'];
  $sm_o = $atts['sm-o'];
  $md_o = $atts['md-o'];
  $lg_o = $atts['lg-o'];
  $xl_o = $atts['xl-o'];

  $output  = '<div class="col-xs-' . $xs;
  if($sm) {
    $output .= ' col-sm-' . $sm;
  }
  if($md) {
    $output .= ' col-md-' . $md;
  }
  if($lg) {
    $output .= ' col-lg-' . $lg;
  }
  if($xl) {
    $output .= ' col-xl-' . $xl;
  }
  if($xs_o) {
    $output .= ' offset-xs-' . $xs_o;
  }
  if($sm_o) {
    $output .= ' offset-sm-' . $sm_o;
  }
  if($md_o) {
    $output .= ' offset-md-' . $md_o;
  }
  if($lg_o) {
    $output .= ' offset-lg-' . $lg_o;
  }
  if($xl_o) {
    $output .= ' offset-xl-' . $xl_o;
  }
  if($class) {
    $output .= ' ' . $class;
  }
  $output .= '">' . do_shortcode( $content ) . '</div>';

  return $output;
}