<?php

function get_youtube( $atts = '', $content = null ) {
  $defaults = array (
    'id'    => '',
    'echo'  => 0,
    'width'  => '',
    'param'  => 'rel=0&showinfo=0&autoplay=1',
    'title'  => '',
    'info'  => '',
  );

  $atts = wp_parse_args( $atts, $defaults );

  // get iframe HTML
  $id       = $atts['id'];
  $echo     = $atts['echo'];
  $width    = $atts['width'];
  $param    = $atts['param'];
  $title    = $atts['title'];
  $info    = $atts['info'];

  if($param){
    $param =  'rel=0&showinfo=0&autoplay=1&' . $param;
  } else {
    $param = 'rel=0&showinfo=0&autoplay=1';
  }

  $output  = '';
  $output .= '<div class="youtube-video-wrapper"';
  if($width){
    $output  .= ' style="max-width:' . $width . 'px"';
  }
  $output  .= '>';
  $output .= '<div class="youtube-video" data-embed="' . $id . '" data-param="' . $param . '">';
  $output .= '<div class="play-button"></div>';
  $output .= '</div>';
  if($title){
    $output .= '<div class="youtube-video-title">' . $title . '</div>';
  }
  if($info){
    $output .= '<div class="youtube-video-info">' . $info . '</div>';
  }
  $output .= '</div>';

  if($echo){
    echo $output;
  } else {
    return $output;
  }
}