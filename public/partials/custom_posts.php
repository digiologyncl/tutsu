<?php

$custom_posts = get_field('admin_setup_custom_posts', 'option');

if( $custom_posts && in_array('projects', $custom_posts) ) {
  include TUTSU_PLUGIN_DIR . '/public/partials/custom_posts/projects.php';
}

if( $custom_posts && in_array('case_studies', $custom_posts) ) {
  include TUTSU_PLUGIN_DIR . '/public/partials/custom_posts/case_studies.php';
}

if( $custom_posts && in_array('products', $custom_posts) ) {
  include TUTSU_PLUGIN_DIR . '/public/partials/custom_posts/products.php';
}

if( $custom_posts && in_array('artists', $custom_posts) ) {
  include TUTSU_PLUGIN_DIR . '/public/partials/custom_posts/artists.php';
}

if( $custom_posts && in_array('events', $custom_posts) ) {
  include TUTSU_PLUGIN_DIR . '/public/partials/custom_posts/events.php';
}

if( $custom_posts && in_array('venues', $custom_posts) ) {
  include TUTSU_PLUGIN_DIR . '/public/partials/custom_posts/venues.php';
}

if( $custom_posts && in_array('offers', $custom_posts) ) {
  include TUTSU_PLUGIN_DIR . '/public/partials/custom_posts/offers.php';
}