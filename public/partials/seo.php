<?php

// ADD GEO META TAGS
function tutsu_geo_meta_tags(){
  // We define our address
  $address = get_address('plain=1');
  if ($address) {
  // We get the JSON results from this request
  $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');
  // We convert the JSON to an array
  $geo = json_decode($geo, true);
  // If everything is cool
  if ($geo['status'] = 'OK') {
    // We set our values
    $latitude = $geo['results'][0]['geometry']['location']['lat'];
    $longitude = $geo['results'][0]['geometry']['location']['lng'];
    $countrycode = $geo['results'][0]['address_components'][5]['short_name'];
    $city = $geo['results'][0]['address_components'][2]['long_name'];
    // $formatted_address = $geo['results'][0]['formatted_address'];
  }


  $output = '<meta name="geo.region" content="' . $countrycode . '" />
<meta name="geo.placename" content="' . $city . '" />
<meta name="geo.position" content="' . $latitude . ';' . $longitude . '" />
<meta name="ICBM" content="' . $latitude . ', ' . $longitude . '" />
';

    echo $output;
  }

}

// add_action('wp_head', 'tutsu_geo_meta_tags', 5);


// ADD 'locations.kml' AND 'geositemap.xml' FILES
function tutsu_geomap_kml(){
  // We define our address
  $address = get_address('plain=1');
  if ($address) {
  // We get the JSON results from this request
  $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($address).'&sensor=false');
  // We convert the JSON to an array
  $geo = json_decode($geo, true);
  // If everything is cool
  if ($geo['status'] = 'OK') {
    // We set our values
    $latitude = $geo['results'][0]['geometry']['location']['lat'];
    $longitude = $geo['results'][0]['geometry']['location']['lng'];
    $formatted_address = $geo['results'][0]['formatted_address'];
  }

  $output_1 = '<?xml version="1.0" encoding="UTF-8"?>
<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom">
  <Document>
    <name>Locations for ' . get_bloginfo('name') . '.</name>
    <atom:link rel="related" href="' . get_bloginfo('url') . '" />
    <Folder>
      <Placemark>
        <name><![CDATA[' . get_bloginfo('name') . ']]></name>
        <address><![CDATA[' . $formatted_address . ']]></address>
        <description><![CDATA[' . get_bloginfo('description') . ']]></description>
        <Point>
          <coordinates>' . $latitude . ',' . $longitude . ',0</coordinates>
        </Point>
      </Placemark>
    </Folder>
  </Document>
</kml>';

  $output_2 = '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:geo="http://www.google.com/geo/schemas/sitemap/1.0">
  <url>
    <loc>' . get_bloginfo('url') . '/locations.kml</loc>
  </url>
</urlset>';

    file_put_contents( 'locations.kml', $output_1);
    file_put_contents( 'geositemap.xml', $output_2);
  }
}

// add_action('wp_head', 'tutsu_geomap_kml', 5);


// ADD GOOGLE ANALYTICS CODE
function tutsu_google_analytics(){
  $id = get_field('google_analytics', 'options');
  $output = '
  <script>
    (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,\'script\',\'https://www.google-analytics.com/analytics.js\',\'ga\');

    ga(\'create\', \'' . $id . '\', \'auto\');
    ga(\'send\', \'pageview\');

  </script>';
  if($id){
    echo $output;
  }
}
add_action('wp_footer', 'tutsu_google_analytics', 9001);


// // HTACCESS REWRITE
// function tutsu_htaccess_rewrite(){
//   $content = get_field('htaccess_rewrite', 'options');

// $output = '';
// if($content){
// $output .= '
// # BEGIN TUSTU Custom
// ';
//   $output .= $content;
//   $output .= '
// # END TUSTU Custom';
// }


//   if($content && !exec('grep '.escapeshellarg('# BEGIN TUSTU Custom').' .htaccess')){
//     file_put_contents( '.htaccess', $output, FILE_APPEND);
//   }
//   elseif (exec('grep '.escapeshellarg('# BEGIN TUSTU Custom').' .htaccess')) {
//     $htaccess = file_get_contents('.htaccess');
//     $htaccess = tutsu_delete_all_between('# BEGIN TUSTU Custom', '# END TUSTU Custom', $htaccess);
//     file_put_contents( '.htaccess', $htaccess);
//     file_put_contents( '.htaccess', $output, FILE_APPEND);
//   }

// }

// add_action( 'save_post', tutsu_htaccess_rewrite() );



