<?php

// STEPS :
// 1. Update admin
// 2. Send email to user
// 3. Verify email and update admin
// 4. Send email to admin

function tutsu_form_appointment_submission(){

  // Fields
  if (isset($_POST["your-fname"])) {
    $your_fname = $_POST['your-fname'];
  } else {
    $your_fname = null;
  }

  if (isset($_POST["your-lname"])) {
    $your_lname = $_POST['your-lname'];
  } else {
    $your_lname = null;
  }
  if (isset($_POST["parent-fname"])) {
    $parent_fname = $_POST['parent-fname'];
  } else {
    $parent_fname = null;
  }

  if (isset($_POST["parent-lname"])) {
    $parent_lname = $_POST['parent-lname'];
  } else {
    $parent_lname = null;
  }

  if (isset($_POST["your-email"])) {
    $your_email = $_POST['your-email'];
  } else {
    $your_email = null;
  }

  if (isset($_POST["your-phone"])) {
    $your_phone = $_POST['your-phone'];
  } else {
    $your_phone = null;
  }

  if (isset($_POST["your-preferred-time-h"])) {
    $your_preferred_time_h = $_POST['your-preferred-time-h'];
  } else {
    $your_preferred_time_h = null;
  }

  if (isset($_POST["your-preferred-time-m"])) {
    $your_preferred_time_m = $_POST['your-preferred-time-m'];
  } else {
    $your_preferred_time_m = null;
  }

  if (isset($_POST["your-preferred-time-ampm"])) {
    $your_preferred_time_ampm = $_POST['your-preferred-time-ampm'];
  } else {
    $your_preferred_time_ampm = null;
  }

  if (isset($_POST["your-source"])) {
    $your_source = $_POST['your-source'];
  } else {
    $your_source = null;
  }

  if (isset($_POST["your-message"])) {
    $your_message = $_POST['your-message'];
  } else {
    $your_message = null;
  }

  // Page ID
  if (isset($_POST["the-page-id"])) {
    $page_id = $_POST['the-page-id'];
  } else {
    $page_id = null;
  }

  // Unique User Key
  $key = str_split('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._~');
  shuffle($key);
  $key = implode('', $key);

  $your_preferred_time = $your_preferred_time_h . ':' . $your_preferred_time_m . ' ' . $your_preferred_time_ampm;

  // Create submission
  $submission = array(
    'fname'   => $your_fname,
    'lname'   => $your_lname,
    'parent_fname'   => $parent_fname,
    'parent_lname'   => $parent_lname,
    'email'   => $your_email,
    'phone'   => $your_phone,
    'preferred_time' => $your_preferred_time,
    'source' => $your_source,
    'message' => $your_message,
    'page_id' => $page_id,
    'key'     => $key,
  );

  // Update admin + send email to client on form submit
  if(isset($_POST['appointment_form_submit']) && $submission ){

    tutsu_form_appointment_update($submission);
    tutsu_form_appointment_send_email_to_user($submission);

  }

}
add_action( 'init', 'tutsu_form_appointment_submission' );

// 1. UPDATE ADMIN
function tutsu_form_appointment_update($submission){

  $row_id = add_row('field_tutsu_forms_submissions_appointment', $submission, 'options');

}

// 2. SEND EMAIL TO USER
function tutsu_form_appointment_send_email_to_user($submission){

    // User info
    $user_fname   = $submission['fname'];
    $user_lname   = $submission['lname'];
    $user_name    = $submission['fname'] . ' ' . $submission['lname'];
    $user_email   = $submission['email'];
    $user_phone   = $submission['phone'];
    $user_page_id = $submission['page_id'];
    $user_key     = $submission['key'];

    // Verification link
    $link = esc_url( get_permalink($user_page_id) ) . '?key=' . $user_key;

    // Email image header
    $image = get_field('tutsu_forms_settings_appointment_header_image', 'options');

    // Email recipient
    $to = get_field('tutsu_forms_settings_appointment_email', 'options') . ',' . get_option( 'admin_email' );
    $sanitized_to = sanitize_email( $to );

    // Email subject
    $subject = get_field('tutsu_forms_settings_appointment_subject', 'options');
    if(!$subject){
      $subject = 'You have booked an appointment at ' . get_option('blogname');
    }

    // Email body
    $body = get_field('tutsu_forms_settings_appointment_body', 'options');
    if($body){
      $body = str_replace('{{name}}', $user_name, $body);
      $body = str_replace('{{fname}}', $user_fname, $body);
      $body = str_replace('{{lname}}', $user_lname, $body);
      $body = str_replace('{{email}}', $user_email, $body);
      $body = str_replace('{{phone}}', $user_phone, $body);
      $body = str_replace('{{link}}', $link, $body);
    }

    // Fetch email template
    $atts = array (
      'body' => $body,
      'link' => $link,
      'image' => $image,
      'title' => $subject,
      'name' => $user_name
    );
    $body = tutsu_email_template_appointment($atts);

    // Email headers
    $headers = array('Content-Type: text/html; charset=UTF-8');

    // Send email
    if ( $sanitized_to != '' ) {
      wp_mail( $to, $subject, $body, $headers );
    }

}

// 3. VERIFY EMAIL AND UPDATE ADMIN
function tutsu_form_appointment_verify_email_and_update_admin(){

  if (isset($_POST["key"])) {
    $tutsu_offer_submission_key = $_POST['key'];
  } else {
    $tutsu_offer_submission_key = null;
  }

  $key_submission = array(
    'key' => $tutsu_offer_submission_key,
  );

  $key_submission_verified = array(
    'verified' => true,
  );

  $submissions = get_field('tutsu_forms_submissions_appointment', 'options');
  if( $submissions ) {
    $i = 0;
    foreach ($submissions as $submission) {
      $i++;
      $key = $submission['key'];
      $verified = $submission['verified'];
      if($key == $tutsu_offer_submission_key){
        // update row
        update_row('tutsu_forms_submissions_appointment', $i, $key_submission_verified, 'options');
        // email admin
        tutsu_form_appointment_send_email_to_admin($submission);
      }
    }
  }

}
add_action( 'init', 'tutsu_form_appointment_verify_email_and_update_admin' );

// 4. SEND EMAIL TO ADMIN
  function tutsu_form_appointment_send_email_to_admin($submission){

    // Email recipient
    $to = get_field('tutsu_forms_settings_appointment_email', 'options');
    if(!$to){
      $to = get_option( 'admin_email' );
    }
    $sanitized_to = sanitize_email( $to );

    // Email subject
    $subject = 'New Submission on ' . get_option('blogname');

    // Link
    $link = get_option( 'home' ) . '/wp-admin/admin.php?page=theme-settings-forms-appointment';

    // Fetch email template
    $atts = array (
      'link' => $link,
      'submission' => $submission
    );
    $body = tutsu_email_template_appointment_admin($atts);

    // Email headers
    $headers = array('Content-Type: text/html; charset=UTF-8');

    // Send email
    if ( $sanitized_to != '' ) {
      wp_mail( $to, $subject, $body, $headers );
    }

}

// Shortcode
function tutsu_form_appointment_shortcode($atts = ''){

  $recaptcha = get_field('tutsu_forms_settings_recaptcha_activate', 'options');
  $recaptcha_key = get_field('tutsu_forms_settings_recaptcha_site_key', 'options');

  $defaults = array(
    'redirect' => '',
  );

  $atts = wp_parse_args( $atts, $defaults );

  $redirect = $atts['redirect'];

  $output = '';

  $output .= '<div class="form-wrapper form-wrapper-appointment" id="form_appointment">';
  $output .= '<form id="form_appointment" method="POST"';
  if($redirect){
    $output .= 'action="' . $redirect . '"';
  }
  $output .= ' style="text-align: left;">';
  if ($_SERVER['REQUEST_METHOD'] == 'POST'){
    if (!isset($_GET['key'])) {
      $output .= '<div style="text-align: center">Thank you for your enquiry.<br>Please check your emails.</div>';
    } else {
      $output .= '<div style="text-align: center">Thank you, your email has been verified. We\'ll be in touch.</div>';
    }
  } else {
    $output .= '<div class="row gutter-sm">';
    $output .= '<div class="col-xs-12 col-sm-6">';
    $output .= '<fieldset class="form-group">';
    $output .= '<label for="your-fname">First Name <span>*</span></label>';
    $output .= '<input type="text" name="your-fname" class="form-control" placeholder="Your first name..." required>';
    $output .= '</fieldset>';
    $output .= '</div>';
    $output .= '<div class="col-xs-12 col-sm-6">';
    $output .= '<fieldset class="form-group">';
    $output .= '<label for="your-lname">Last Name <span>*</span></label>';
    $output .= '<input type="text" name="your-lname" class="form-control" placeholder="Your last name..." required>';
    $output .= '</fieldset>';
    $output .= '</div>';
    $output .= '</div>';
    $output .= '<div>';
    $output .= '<small>(If patient is under 18 years of age.)</small>';
    $output .= '</div>';
    $output .= '<div class="row gutter-sm">';
    $output .= '<div class="col-xs-12 col-sm-6">';
    $output .= '<fieldset class="form-group">';
    $output .= '<label for="parent-fname">Parent First Name</label>';
    $output .= '<input type="text" name="parent-fname" class="form-control" placeholder="Parent first name...">';
    $output .= '</fieldset>';
    $output .= '</div>';
    $output .= '<div class="col-xs-12 col-sm-6">';
    $output .= '<fieldset class="form-group">';
    $output .= '<label for="parent-lname">Parent Last Name</label>';
    $output .= '<input type="text" name="parent-lname" class="form-control" placeholder="Parent last name...">';
    $output .= '</fieldset>';
    $output .= '</div>';
    $output .= '</div>';
    $output .= '<div class="row gutter-sm">';
    $output .= '<div class="col-xs-12 col-sm-6">';
    $output .= '<fieldset class="form-group">';
    $output .= '<label for="your-email">Email Address <span>*</span></label>';
    $output .= '<input type="email" name="your-email" class="form-control" placeholder="Your email address..." required>';
    $output .= '</fieldset>';
    $output .= '</div>';
    $output .= '<div class="col-xs-12 col-sm-6">';
    $output .= '<fieldset class="form-group">';
    $output .= '<label for="your-phone">Phone <span>*</span></label>';
    $output .= '<input type="tel" name="your-phone" class="form-control" placeholder="Your phone number..." required>';
    $output .= '</fieldset>';
    $output .= '</div>';
    $output .= '</div>';
    $output .= '<div class="row gutter-sm items-sm-top">';
    $output .= '<div class="col-xs-12 col-sm-4">';
    $output .= '<div><label for="your-preferred-time">Preferred Time <span>*</span></label></div>';
    $output .= '<fieldset class="form-group" style="max-width: 200px;">';
    $output .= '<div class="row gutter-xs">';
    $output .= '<div class="col-xs-4">';
    $output .= '<select class="form-control" name="your-preferred-time-h"><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option><option value="11">11</option><option value="12">12</option></select>';
    $output .= '</div>';
    $output .= '<div class="col-xs-4">';
    $output .= '<select class="form-control" name="your-preferred-time-m"><option value="00">00</option><option value="10">10</option><option value="20">20</option><option value="30">30</option><option value="40">40</option><option value="50">50</option></select>';
    $output .= '</div>';
    $output .= '<div class="col-xs-4">';
    $output .= '<select class="form-control" name="your-preferred-time-ampm"><option value="AM">AM</option><option value="PM">PM</option></select>';
    $output .= '</div>';
    $output .= '</div>';
    $output .= '</fieldset>';
    $output .= '</div>';
    $output .= '<div class="col-xs-12 col-sm-8">';
    $output .= '<fieldset class="form-group">';
    $output .= '<label for="your-source">How did you hear about the practice? <span>*</span></label>';
    $output .= '<select class="form-control" name="your-source"><option value="Google">Google</option><option value="Print Ad">Print Ad</option><option value="Billboard">Billboard</option><option value="Referral">Referral</option></select>';
    $output .= '</fieldset>';
    $output .= '</div>';
    $output .= '</div>';
    $output .= '<fieldset class="form-group">';
    $output .= '<label for="your-message">Tell us about your dental needs: <span>*</span></label>';
    $output .= '<textarea type="textarea" name="your-message" rows="6" class="form-control" placeholder="Your message..." required></textarea>';
    $output .= '</fieldset>';
    $output .= '<fieldset>';
    $output .= '<input type="text" name="the-page-id" class="form-control" value="' . get_the_ID() .'" style="visibility: hidden; height: 0; border: 0; padding: 0;">';
    $output .= '</fieldset>';
    if($recaptcha){
      $output .= '<fieldset class="form-group">';
      $output .= '<div class="g-recaptcha" data-sitekey="' . $recaptcha_key . '"></div>';
      $output .= '</fieldset>';
    }
    $output .= '<div>';
    $output .= '<input type="submit" name="appointment_form_submit" class="btn btn-1" value="Submit">';
    $output .= '</div>';
    $output .= '<small>(* required fields)</small>';
  }
  $output .= '</form>';
  $output .= '</div>';

  return $output;
}
add_shortcode('form_appointment', 'tutsu_form_appointment_shortcode');