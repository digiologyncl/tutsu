<?php

require_once TUTSU_PLUGIN_DIR . '/public/partials/enqueue.php';
require_once TUTSU_PLUGIN_DIR . '/public/partials/design.php';
require_once TUTSU_PLUGIN_DIR . '/public/partials/agencies.php';
require_once TUTSU_PLUGIN_DIR . '/public/partials/custom_posts.php';
require_once TUTSU_PLUGIN_DIR . '/public/partials/shortcodes.php';
require_once TUTSU_PLUGIN_DIR . '/public/partials/components.php';
require_once TUTSU_PLUGIN_DIR . '/public/partials/seo.php';
require_once TUTSU_PLUGIN_DIR . '/public/partials/apis.php';
// require_once TUTSU_PLUGIN_DIR . '/public/partials/templates.php';
require_once TUTSU_PLUGIN_DIR . '/public/partials/email_templates.php';


