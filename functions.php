<?php

// check if post/page is a tree
function is_tree($pid) {   // $pid = The ID of the page we're looking for pages underneath
   global $post;           // load details about this page
   if(is_page()&&($post->post_parent==$pid||is_page($pid))) {
      return true;         // we're at the page or at a sub page
   }
   else {
      return false;        // we're elsewhere
   }
}

// check if post/page has children
function has_children() {
    global $post;

    $children = get_pages( array( 'child_of' => $post->ID ) );
    if( count( $children ) == 0 ) {
        return false;
    } else {
        return true;
    }
}


function tutsu_plugin_url( $path = '' ) {
	$url = plugins_url( $path, TUTSU_PLUGIN );
	if ( is_ssl() && 'http:' == substr( $url, 0, 5 ) ) {
		$url = 'https:' . substr( $url, 5 );
	}
	return $url;
}

// convert hex to rgb
function tutsu_hex2rgb($hex) {
   $hex = str_replace("#", "", $hex);

   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   }
   $rgb = array($r, $g, $b);
   return implode(",", $rgb); // returns the rgb values separated by commas
   // return $rgb; // returns an array with the rgb values
}


// delete between to elements
function tutsu_delete_all_between($beginning, $end, $string) {
  $beginningPos = strpos($string, $beginning);
  $endPos = strpos($string, $end);
  if ($beginningPos === false || $endPos === false) {
    return $string;
  }

  $textToDelete = substr($string, $beginningPos, ($endPos + strlen($end)) - $beginningPos);

  return str_replace($textToDelete, '', $string);
}


// Retrieve all files from a directory
function tutsu_get_files( $dir , $ext ) {
  $path = get_template_directory().'/'.$dir;
  $files = array_diff( scandir( $path ) , array( '..' , '.' ) );

  $filenames = array();

  foreach( $files as $file ) {
    if( is_file( $path.'/'.$file ) && substr( $file , -1 - strlen( $ext ) , 1 + strlen( $ext ) ) == '.'.$ext ) {
      $filenames[] = $file;
    }
  }

  return $filenames;
}

// add body_begin
function body_begin() {
  do_action('body_begin');
}

// add excerpts to pages
add_post_type_support( 'page', 'excerpt' );
